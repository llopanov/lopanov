package abstractions;

import java.util.HashMap;
import java.util.Map;

import interfaces.Cacheable;

abstract public class CacheAbstract<K, V> implements Cacheable<K, V> {
	protected int cacheSize = 3;
	protected Map<K,V> dataCache = new HashMap<K, V>();
	
	
	public final V get(K key) {
		// TODO Auto-generated method stub
		synchronized (this) {
			if (dataCache.containsKey(key)){
				return getFromCache(key);
			}
		}
	
		V tmpVal = calculate(key);
		System.out.println("->Calculate complete for "+key);	
		
		
		synchronized (this) {
			// if some process put data in cache with current key
			// while in current process was invoked calculate() function
			if (dataCache.containsKey(key)){
				return getFromCache(key);//return tmpVal;
			}
			updateDataCache(key, tmpVal);
		}
		return tmpVal;
	}
	
	public int getCacheSize(){
		return cacheSize;
	}
	
	abstract protected V getFromCache(K key);
	abstract protected void updateDataCache(K key, V val);
	
	abstract public void setCacheSize(int cacheSize);
	abstract protected V calculate(K key);

}
