package interfaces;

public interface Cacheable <K, V>{
	public V get(K key);
}
