package collections;

import LFU.LfuCacheString;
import LRU.LruCacheString;
import interfaces.Cacheable;

public class TestThread  implements Runnable{ //extends Thread{//
	private Cacheable<String, String> cache;
	
	public TestThread(LfuCacheString cache) {
		super();
		this.cache = cache;
	}

	public TestThread(LruCacheString cache) {
		super();
		this.cache = cache;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("process 2 START");
		try {
			Thread.sleep(30);
			System.out.println(cache.get("ben"));
			Thread.sleep(60);
			System.out.println(cache.get("alex"));
			Thread.sleep(100);
			System.out.println(cache.get("alex"));
			Thread.sleep(500);
			System.out.println(cache.get("alex"));
			Thread.sleep(200);
			System.out.println(cache.get("ben"));
			Thread.sleep(100);
			System.out.println(cache.get("tom"));
			Thread.sleep(500);
			System.out.println(cache.get("alex"));
			Thread.sleep(300);
			System.out.println(cache.get("hanry"));
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("process 2 STOP");
	}

}
