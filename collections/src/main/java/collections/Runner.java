package collections;

import LRU.LruCacheString;
import LFU.LfuCacheString;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		long timeout=System.currentTimeMillis();
		System.out.println("\n===========================");
		System.out.println("=====LRU start testing=====\n");
		LruCacheString cacheLru =  new LruCacheString();

		Thread threadTestCache = new Thread(new TestThread(cacheLru));
		threadTestCache.start();
		try {
			System.out.println(cacheLru.get("alex"));
			Thread.sleep(200);
			System.out.println(cacheLru.get("ben"));
			Thread.sleep(1000);
			System.out.println(cacheLru.get("tom"));
			Thread.sleep(500);
			System.out.println(cacheLru.get("alex"));
			Thread.sleep(300);
			System.out.println(cacheLru.get("hanry"));
			Thread.sleep(30);
			System.out.println(cacheLru.get("tom"));
			Thread.sleep(60);
			System.out.println(cacheLru.get("alex"));
			Thread.sleep(100);
			System.out.println(cacheLru.get("ben"));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n==total time = " + (System.currentTimeMillis() - timeout));
				
		timeout=System.currentTimeMillis();
		System.out.println("\n===========================");
		System.out.println("=====LFU start testing=====\n");
		
		LfuCacheString cacheLfu =  new LfuCacheString();

		threadTestCache = new Thread(new TestThread(cacheLfu));
		threadTestCache.start();
		
		System.out.println(cacheLfu.get("alex"));
		System.out.println(cacheLfu.get("ben"));
		System.out.println(cacheLfu.get("alex"));
		System.out.println(cacheLfu.get("tom"));
		System.out.println(cacheLfu.get("hanry"));
		System.out.println(cacheLfu.get("tom"));
		System.out.println(cacheLfu.get("alex"));
		System.out.println(cacheLfu.get("ben"));
		
		System.out.println("\n==total time = " + (System.currentTimeMillis() - timeout));
	}

}
