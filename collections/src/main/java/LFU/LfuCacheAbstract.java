package LFU;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import abstractions.CacheAbstract;

abstract class LfuCacheAbstract<K, V> extends CacheAbstract<K, V>{

	private double evictionFactor = 0.4;
	private List<List<K>> lfuList= new ArrayList<List<K>>(cacheSize);
		
	LfuCacheAbstract(){
		for (int i=0; i<cacheSize; i++){
			lfuList.add(new LinkedList<K>());
		}
	}
	
	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
		for (int i=0; i<cacheSize; i++){
			lfuList.add(new LinkedList<K>());
		}
	}
	
	public void setEvictionFactor(int evictionFactor) {
		this.evictionFactor = evictionFactor;
	}
	
	private void printKeys(){
		System.out.print("\n[ ");
		for (List<K> subList: lfuList){
			System.out.print(" [ ");
			for (K key : subList){
				System.out.print(key + ", ");
			}
			System.out.print(" ] ,");
		}
		System.out.print(" ]\n");
	}

	protected void updateDataCache(K key, V val){
		if (getCountElementsInCache() == cacheSize){
			evictionCache();
		}
		
		lfuList.get(0).add(key);
		dataCache.put(key, val);
		printKeys();
	}
	
	protected V getFromCache(K key){
		//updateFrequency
		Iterator <List<K>> it = lfuList.iterator();
		while(it.hasNext()){
			List<K> listCur = it.next();
			if (listCur.contains(key)){
				listCur.remove(key);
				if(it.hasNext()){
					it.next().add(key);
				} else {
					listCur.add(key);
				}
				break;
			}
		}
		printKeys();
		System.out.println("get result from cache : " + key);
		return dataCache.get(key);
	}
	
	private int getCountElementsInCache(){
		int countElementsInCache = 0;
		//for (List<K> subList : lfuList){
		for (int i=0; i<lfuList.size(); i++){
			countElementsInCache+=lfuList.get(i).size();
		}
		return countElementsInCache;
	}
	
	private void evictionCache(){
		int eviction = (int)(evictionFactor*cacheSize);
		eviction = (eviction < 1) ? 1 : eviction;
		for (int i=0; i<lfuList.size() && eviction>0; i++){
			List <K> subList= lfuList.get(i);
			while (!subList.isEmpty() && eviction>0){
				K keyTmp = subList.get(0);
				subList.remove(0);
				dataCache.remove(keyTmp);
				eviction--;
			}
		}
	}

}

