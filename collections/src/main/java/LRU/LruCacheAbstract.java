package LRU;

import java.util.LinkedList;
import java.util.List;

import abstractions.CacheAbstract;

abstract class LruCacheAbstract<K, V> extends CacheAbstract<K, V>{

	private List<K> queueRequests = new LinkedList<K>();
	
	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
	}
	
	private void printKeys(){
		System.out.print("[ ");
		for (K key: queueRequests){
		System.out.print(key + ", ");
		}
		System.out.print(" ]\n");
	}
	
	protected V getFromCache(K key){
		queueRequests.remove(key);
		queueRequests.add(key);
		System.out.println("\nget result from cache : "+key);
		printKeys();
		return dataCache.get(key);
	}
	
	protected void updateDataCache(K key, V val){
		if (queueRequests.size() == cacheSize){
			K keyTmp = queueRequests.get(0);
			queueRequests.remove(0);
			dataCache.remove(keyTmp);
			System.out.println("Remove key from cache: "+keyTmp);
		}
		
		queueRequests.add(key);
		dataCache.put(key, val);
		printKeys();
	}
}
