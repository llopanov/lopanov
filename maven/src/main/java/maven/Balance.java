package maven;

import java.util.ArrayList;
import java.util.List;

public class Balance extends AbstractAlgorithm {
	static final int INDEX_BALANCE_STRING = 1;
	static final Character OPEN  = '(';
	static final Character CLOSE = ')';
	private List<Character> string = new ArrayList<Character>();;
	
	private static boolean balance(List<Character> string){
		List<Character> cuttedStr = new ArrayList<Character>();
		int indexOpen = -1;
		int indexClose = -1;
		for (int i=0;i<string.size(); i++){
			if (OPEN.equals(string.get(i))){
				indexOpen = i;
			}
			if (CLOSE.equals(string.get(i))){
				indexClose = i;
				break;
			}
		}

		if (indexOpen < 0 && indexClose < 0){
			return true;
		} else if (indexOpen < 0 || indexClose < 0){
			return false;
		} else {
			for (int i=0;i<string.size(); i++){
				if (i<indexOpen || i>indexClose){
					cuttedStr.add(string.get(i));
				}
			}
			return balance(cuttedStr);
		}
	}
	@Override
	protected void initialization(String[] args) {
		// TODO Auto-generated method stub
		for (char c : args[INDEX_BALANCE_STRING].toCharArray()){ //
			string.add(c);
		}
	}

	@Override
	protected void calculate() {
		// TODO Auto-generated method stub
		System.out.println(balance(string));
	}

}
