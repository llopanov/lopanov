package maven;

public class Runner {
	static final String ERROR_MESSAGE_INPUT_PAR = "Not enougth input parameters";
	static final String ERROR_MESSAGE_NUMBER_FORMAT = "Number format is wrong";
		
	public static void main(String[] args) {
		// TODO Автоматически созданная заглушка метода
		try {
			AbstractAlgorithm alg = AlgorithmFactory.getAlgorithm(args);
			alg.Run(args);
		} catch (ArrayIndexOutOfBoundsException e){
			System.err.println(ERROR_MESSAGE_INPUT_PAR); // or parameters are wrong
		} catch (NumberFormatException e){
			System.err.println(ERROR_MESSAGE_NUMBER_FORMAT);
		} catch (IllegalArgumentException e){
			System.err.println(e.getMessage());
		}
	}
}
