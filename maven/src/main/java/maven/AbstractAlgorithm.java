package maven;

abstract class AbstractAlgorithm {
	public void Run(String[] args){
		initialization(args);
		calculate();
	}
	
	protected abstract void initialization(String[] args);
	protected abstract void calculate();
	
}
