package maven;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Pascal extends AbstractAlgorithm {
	static final int INDEX_PASCAL_COLUMN = 1;
	static final int INDEX_PASCAL_ROW = 2;
	
	static final String ERROR_MESSAGE_WRONG_INDEXES = "Column index cannot be more than row index";
	
	private List <Integer> start = Arrays.asList(1);//new ArrayList<Integer>( );
	private int curRow = 0;
	
	private int column;
	private int row;
	
	private int pascal(int column, int row){

		if ( curRow<row ){
			List <Integer> arr = new ArrayList<Integer>();

			for (int i=0;i<start.size()+1;i++){
				if (i==0 || i == start.size()){
//					arr.add(start.get(i));
					arr.add(1);
				} else {
					arr.add(start.get(i)+start.get(i-1));
				}
			}
			start = arr;
			curRow ++;
			
			return pascal(column,  row);
		}
		
		return start.get(column);
	}

	@Override
	protected void initialization(String[] args) {
		// TODO Auto-generated method stub
		row 	= Integer.parseInt(args[INDEX_PASCAL_ROW]);
		column 	= Integer.parseInt(args[INDEX_PASCAL_COLUMN]);
		if (column > row){
			throw new IllegalArgumentException(ERROR_MESSAGE_WRONG_INDEXES);
		}
	}

	@Override
	protected void calculate() {
		// TODO Auto-generated method stub
		System.out.println(pascal(column, row));
	}
}
