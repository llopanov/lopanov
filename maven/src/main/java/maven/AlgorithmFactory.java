package maven;

public class AlgorithmFactory {
	static final int INDEX_ALGORITM = 0;
	static final String ERROR_MESSAGE_UNSUPPORT_FUNC = "Unsupported function name";
	
	public static AbstractAlgorithm getAlgorithm(String[] args){
		AlgorithmEnum algoritmName = getAlgoritmName(args);
		AbstractAlgorithm algorithm;
		switch (algoritmName){
		case PASCAL:{  //.jar pascal 0 2 
			algorithm = new Pascal();
			break;
		}
		case BALANCE:{ //.jar balance ())( 
			algorithm = new Balance();
			break;
		}
		case COUNTCHANGE:{  // .jar countChange money=4 coins=[1,2] 
			algorithm = new CountChange();
			break;
		}
		default:
			throw new IllegalArgumentException(ERROR_MESSAGE_UNSUPPORT_FUNC);
		}
		return algorithm;
	}
	
	private static AlgorithmEnum getAlgoritmName(String[] args){
		AlgorithmEnum selectedAlgoritm = AlgorithmEnum.valueOf(args[INDEX_ALGORITM].toUpperCase());
		
		return selectedAlgoritm;
		
	
	}
}
