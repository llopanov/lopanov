package maven;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CountChange extends AbstractAlgorithm {
	private static final int INDEX_CHANGE_MONEY = 1;
	private static final int INDEX_CHANGE_COINS = 2;
	
	private static final String CHANGE_DELIMITER_TMPL = "=";
	private static final String CHANGE_COINS_DELIMITER_TMPL = ",";
	
	private int money;
	private  List<Integer> coins;
	@Override
	protected void initialization(String[] args) {
		// TODO Auto-generated method stub
		money = Integer.parseInt(args[INDEX_CHANGE_MONEY].split(CHANGE_DELIMITER_TMPL)[1]);
		
		coins = new LinkedList<Integer>();
		String str = args[INDEX_CHANGE_COINS].split(CHANGE_DELIMITER_TMPL)[1];
		String strListCoins[] = str.substring(1, str.length()-1).split(CHANGE_COINS_DELIMITER_TMPL);
		for (String s : strListCoins){
			coins.add(Integer.parseInt(s.trim()));
		}
	}

	@Override
	protected void calculate() {
		// TODO Auto-generated method stub
		System.out.println(countChange(money,coins));
	}

	private static int countChange(int money, List<Integer> coins){
		Collections.sort(coins);
		if (coins.size() == 0 || money<0){
			return  0;
		} else if (money == 0){
			return 1;
		} else {
			List<Integer> newCoins = new LinkedList<Integer>(coins);
			int coin = newCoins.remove(newCoins.size()-1);
			return countChange(money, newCoins) + countChange(money - coin, coins);
		}
		
	}
}
