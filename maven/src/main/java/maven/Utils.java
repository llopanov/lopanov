package maven;

import java.lang.Integer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Utils {

	static List <Integer> start = Arrays.asList(1);//new ArrayList<Integer>( );
	static int curRow = 0;
	
	public static int pascal(int column, int row){
		//
		
		if ( curRow<row ){
			List <Integer> arr = new ArrayList<Integer>();
			//start.add(0, 0);
			//start.add(start.size(), 0);
			for (int i=0;i<start.size()+1;i++){
				if (i==0){
					System.out.print(1+" ");
//					arr.add(start.get(i));
					arr.add(1);
				} else if (i == start.size()){
					System.out.print(1+" ");
//					arr.add(start.get(i-1));
					arr.add(1);
				} else {
					System.out.print(start.get(i)+start.get(i-1)+" ");
					arr.add(start.get(i)+start.get(i-1));
				}
			}
			System.out.println();
			start = arr;
			curRow ++;
			// calc new start and iteration curRow
			return pascal(column,  row);
		}
		
		return start.get(column);
	}
	
	static final Character OPEN  = '(';
	static final Character CLOSE = ')';
	
	public static boolean balance(List<Character> string){
		List<Character> cuttedStr = new ArrayList<Character>();
		int indexOpen = -1;
		int indexClose = -1;
		for (int i=0;i<string.size(); i++){
			if (OPEN.equals(string.get(i))){
				indexOpen = i;
			}
			if (CLOSE.equals(string.get(i))){
				indexClose = i;
				break;
			}
		}

		if (indexOpen < 0 && indexClose < 0){
			return true;
		} else if (indexOpen < 0 || indexClose < 0){
			return false;
		} else {
			for (int i=0;i<string.size(); i++){
				if (i<indexOpen || i>indexClose){
					cuttedStr.add(string.get(i));
				}
			}
			return balance(cuttedStr);
		}
	}
	
	public static int countChange(int money, List<Integer> coins){
		Collections.sort(coins);
		if (coins.size() == 0 || money<0){
			return  0;
		} else if (money == 0){
			return 1;
		} else {
			List<Integer> newCoins = new LinkedList<Integer>(coins);
			int coin = newCoins.remove(newCoins.size()-1);
			return countChange(money, newCoins) + countChange(money - coin, coins);
			//return countChange(money, coins.size() - 1) + countChange(money - coins.get(coins.size()-1), coins.size());
			//return count_change(money, n − 1) + count_change(money − type(n), n);
		}
		
	}
}
