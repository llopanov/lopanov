package reflection.part_b;

import java.lang.reflect.Field;

public class AnalyzerClass {
	public static boolean alObjects(Object o1, Object o2){
		boolean rez = true;
		Class<?> cl1 = o1.getClass();
		Class<?> cl2 = o2.getClass();
		
		Field fields[] = cl1.getFields(); // getDeclaredFields
		
		for (Field fld1 : fields){
			try{
				Field fld2 = cl2.getField(fld1.getName());
				boolean isAnnotationPresent =  fld1.isAnnotationPresent(Equal.class);
				if (isAnnotationPresent){
					Equal equal = (Equal) fld1.getAnnotation(Equal.class);
					EqualType type = equal.type();
					switch (type){
						case EQUAL_REFERENCE:{
							rez &= (fld1.get(o1) == fld2.get(o2));
							break;
						}
						case EQUAL_VALUE:{
							rez &= fld1.get(o1).equals(fld2.get(o2));
							break;
						}
					}
					if (!rez)
						break;
				}
			} catch (NoSuchFieldException e){
					System.err.println(e.getMessage());
				} catch (IllegalArgumentException e) {
					System.err.println(e.getMessage());
				} catch (IllegalAccessException e) {
					System.err.println(e.getMessage());
				}
			
		}
		return rez;
	}
}
