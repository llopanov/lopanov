package reflection.part_b;

public class TestClass {
	@Equal(type = EqualType.EQUAL_REFERENCE)
	public Integer a;
	
	@Equal(type = EqualType.EQUAL_VALUE)
	public Double b;
	
	@Equal(type = EqualType.EQUAL_VALUE)
	public String str;
	
	public TestClass(Integer a, Double b, String str) {
		super();
		this.a = a;
		this.b = b;
		this.str = str;
	}
}
