package reflection.part_a;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

public @interface Proxy {
	String invocationHandler();
}
