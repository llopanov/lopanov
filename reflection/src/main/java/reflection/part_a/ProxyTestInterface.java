package reflection.part_a;

public interface ProxyTestInterface {
    public String getName(String name);
}
