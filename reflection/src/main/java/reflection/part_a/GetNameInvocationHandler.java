package reflection.part_a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class GetNameInvocationHandler implements InvocationHandler {
	private ProxyTestInterface proxied;
	 
    public GetNameInvocationHandler(Object proxied) {
        this.proxied = (ProxyTestInterface) proxied;
    }
 
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(proxied, new Object[]{"Its a proxy invoke "});
    }
}
