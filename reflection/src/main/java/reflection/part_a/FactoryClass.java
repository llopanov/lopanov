package reflection.part_a;

import reflection.part_a.Proxy;
import java.lang.reflect.*;


public class FactoryClass {
	public static Object getInstanceOf(Class<?> className) throws Throwable {
		
		boolean isAnnotationPresent =  className.isAnnotationPresent(Proxy.class);
		
		if (isAnnotationPresent){
			Proxy proxy = (Proxy) className.getAnnotation(Proxy.class);
			
			Class<?> cl = Class.forName(proxy.invocationHandler());

			Constructor<?> cons = cl.getConstructor(new Class[]{Object.class}); // get constructor with parameter ( Object ) in handler class

			return java.lang.reflect.Proxy.newProxyInstance(className.getClassLoader(), 
													 className.getInterfaces(), 
													 (InvocationHandler) cons.newInstance(new Object[]{className.newInstance()}));

		} else {
			return className.newInstance();
		}
	}
}
