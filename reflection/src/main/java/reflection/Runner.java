package reflection;

import reflection.part_a.FactoryClass;
import reflection.part_a.ProxyTest;
import reflection.part_a.ProxyTestInterface;

import reflection.part_b.AnalyzerClass;
import reflection.part_b.TestClass;

public class Runner {

	public static void main(String[] args) {
		// Part A test
		System.out.print("\n==============\n    PART A\n==============\n");
		try {
			// for proxy invoke just uncomment @Proxy annotation in ProxyTest.java
			ProxyTestInterface test = (ProxyTestInterface) FactoryClass.getInstanceOf(ProxyTest.class);
	        System.out.println(test.getName("not Proxy invoke"));			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		// Part B test
		TestClass test1 = new TestClass(5, 20., "hello");
		TestClass test2 = new TestClass(new Integer(5), new Double(20.), "hello");
		
		System.out.print("\n==============\n    PART B\n==============\n");
		System.out.print("Equals of classes are - " + AnalyzerClass.alObjects(test1, test2) +"\n");
	}

}
