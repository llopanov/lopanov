package epam.enums;

public enum PassengerState {
	NOT_STARTED ,//(эти значением поле инициализируется при создании экземпляра Passenger)
    IN_PROGRESS ,//(когда создается Transportation Task) 
    IN_ELEVATOR ,// когда в лифте
    COMPLETED   ,//(когда завершается Transportation Task) 
    ABORTED     //(когда прерывается Transportation Task) 
}
