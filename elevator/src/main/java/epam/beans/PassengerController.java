package epam.beans;

import static epam.logging.LoggerClass.LOGGER;

import epam.enums.PassengerState;


class PassengerController implements Runnable{
	Passenger passenger;
	Controller controller;
	
	public PassengerController(Passenger passenger,Controller controller) {
		super();
		// TODO Auto-generated constructor stub
		this.passenger = passenger;
		this.controller = controller;
	}

	public void run() {
		//System.out.println("Start task for "+passenger);
		// TODO Auto-generated method stub
		passenger.setTransportationState(PassengerState.IN_PROGRESS);

		try {
			
			// waiting elevator
			synchronized (controller.getStory(passenger.getDepartureStory())){
				controller.decrementNotStartedPassengers();
				controller.getStory(passenger.getDepartureStory()).wait();
			}
			
			// waiting load to elevator, if no waiting more
			while (true){
				boolean isMoveToElevator = controller.tryMoveToElevator(passenger);
				
				if (isMoveToElevator){
						break;
				} else {
					synchronized (controller.getStory(passenger.getDepartureStory())){
						controller.getStory(passenger.getDepartureStory()).wait();
					}
				}
			}
			
			passenger.setTransportationState(PassengerState.IN_ELEVATOR);
			
			synchronized (controller.getElevator()) {
					controller.getElevator().wait();
			}
			
			// waiting arrival story
			while (true){
				boolean isMoveFromElevator = controller.tryMoveFromElevator(passenger);
				
				if (isMoveFromElevator){
						break;
				} else {
					synchronized (controller.getElevator()) {
						controller.getElevator().wait();
					}
				}
			}
			
			passenger.setTransportationState(PassengerState.COMPLETED);
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				passenger.setTransportationState(PassengerState.ABORTED);
				LOGGER.info(passenger +" "+ PassengerState.ABORTED);
				//e.printStackTrace();
			}
		}

}