package epam.beans;

import epam.interfaces.Waitable;

public class Waiter {
	
	private volatile int count ;

	
	public synchronized void decrement(){
		count--;
	}

	
	public void doWait(Waitable ob){
		this.count = ob.getCounter();
		
		synchronized (ob) {
			ob.notifyAll();
		}
		
		while (true){
			if (count == 0)
				break;
		}
		
		
		
	}
}
