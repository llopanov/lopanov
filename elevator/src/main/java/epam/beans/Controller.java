package epam.beans;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import epam.enums.PassengerState;
import epam.interfaces.DrawElevator;
import epam.interfaces.DrawPeople;

import static epam.logging.LoggerClass.*;
import static epam.beans.Constants.*;

public class Controller implements Runnable {
	private int storyCnt;
	private List<Story> stories;
	private Elevator elevator;
	private int passengersNotStarted;
	Waiter waiter = new Waiter();
	
	public Controller(int storyCnt, int elevatorCapacity, int countPass) {
		super();
		this.storyCnt = storyCnt;
		stories = new ArrayList<Story>(storyCnt);
		for (int i=0; i<storyCnt; i++){
			stories.add(new Story(i));
		}
		elevator = new Elevator(elevatorCapacity, storyCnt);
		
		addPassengeres(countPass);
		
	}
	
	public Story getStory(int storyId){
		return stories.get(storyId);
	}
	
	public int getCountStories(){
		return storyCnt;
	}
	
	private void addPassengeres(int countPass){
		passengersNotStarted = countPass;
		Random rand = new Random();
		for (int i=0; i < countPass; i++){
			int destinationStory = rand.nextInt(storyCnt);//2;//
			int departureStory = rand.nextInt(storyCnt);//0;//

			while (departureStory == destinationStory){
				departureStory = rand.nextInt(storyCnt);
			}
			
			stories.get(departureStory).addToDepart(new Passenger(i, destinationStory,departureStory));//,  elevator));
		}
	}
	
	public synchronized void decrementNotStartedPassengers(){
		passengersNotStarted--;
		if (passengersNotStarted == 0){
				notifyAll();
		}
	}

	public void printBuild() {
		// TODO Auto-generated method stub
		for (int i=stories.size()-1; i>=0;i--){
			System.out.println((i) + " : " + "pass: " + stories.get(i)) ;
		}
	}
	
	public boolean isEmptyStories(){
		for (Story story : stories){
			if (!story.isEmptyStory()){
				return false;
			}
		}
		return true;
	}
	
	public void run() {
		LOGGER.info(START_ELEVATOR);
		try{
		//printBuild();
		runPassengers();
		
		while (!isEmptyStories() || !elevator.isEmpty()){
			Story story;
			synchronized (this) {
				
				elevator.moveElevator();
				story = stories.get(elevator.getCurrentStory());
			
			} //while
			
			waiter.doWait(elevator);
			waiter.doWait(story);
			
//			System.out.println("============");
//			printBuild();
//			System.out.println("============");
		}
		
		LOGGER.info(STOP_ELEVATOR);
		verifyTask();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
		LOGGER.info(Constants.RUNNER_ABORTED);
	}
		//System.out.println("STOP ELEVATOR");
	}
	
	public /**/synchronized boolean tryMoveToElevator(Passenger passenger) throws InterruptedException{
		
		waiter.decrement();
		//System.out.println(passenger + " : Try move to elevator");
		boolean returnResult = elevator.addPassengerToElevator(passenger);
			if(returnResult){
				stories.get(elevator.getCurrentStory()).removeFromDepart(passenger);
				//System.out.println("Elevator: " + elevator);
			} 
			
			//stories.get(passenger.getDepartureStory()).decrementPeopleWaitingThreadStory();
			return returnResult;

	}
	
	public /**/synchronized boolean tryMoveFromElevator(Passenger passenger) throws InterruptedException{
		waiter.decrement();
		boolean returnResult = elevator.removePassenger(passenger);
		
		
		if (returnResult){
			stories.get(elevator.getCurrentStory()).addToArrival(passenger);
		}
		
		return returnResult;
	}
	
	private void runPassengers() throws InterruptedException{
		// TODO Auto-generated method stub
		ThreadGroup tg = Thread.currentThread().getThreadGroup();
		for (Story story : stories){
			for (Passenger passenger : story.getDepartList()){
				Thread  transport = new Thread(tg, new PassengerController(passenger,this));
				
				transport.start();
				
			}
		}
		
		synchronized (this) {
			this.wait();
		}
	}
	
	public DrawElevator getElevator(){
		return elevator;
	}
	

	public List<DrawPeople>  getStoriesPassengersCoord(){
		List<DrawPeople> listRet = new ArrayList<DrawPeople>();
		listRet.addAll(elevator.getElevatorContainer());
		for (Story story : stories){
			listRet.addAll(story.getDepartList());
			listRet.addAll(story.getArrivalList());
		}
		
		return listRet;
		
	}
	
	private void verifyTask(){
		int countPassengersToDepart 	= 0;
		int countPassengersArrival 		= 0;
		boolean status = true;
		
		for (Story story : stories){
			countPassengersToDepart += story.getDepartList().size();
			countPassengersArrival  += story.getArrivalList().size();
			
			for (Passenger pas : story.getArrivalList()){
				if (pas.getTransportationState() != PassengerState.COMPLETED ||
						pas.getDestinationStory() != story.getStoryId()){
					status = false;
				}
			}
		}
		
		LOGGER.info(COUNT_PAS_TO_DEPART + countPassengersToDepart);
		LOGGER.info(COUNT_PAS_IN_ELEVATOR + elevator.getElevatorContainer().size());
		LOGGER.info(COUNT_PAS_ARRIVAL + countPassengersArrival);
		LOGGER.info(STATUS_PASSENGERS_AND_STORIES + status);
		
		
	}
}
