package epam.beans;

public final class Constants {
	public final static int START_POSITION = 10;
	public final static int END_POSITION = 80;
	public final static int START_END_POSITION_WIDTH = 20;
	public final static int ELEVATOR_POSITION = 50;
	public final static int ELEVATOR_WIDTH = 24;
	
	public final static int PEOPLE_WIDTH = 8;
	
	public static final double ELEVATOR_SPEED = 0.025;
	public static final double PASSENGER_SPEED = 1;
	public static final int ANIMATION_TIMEOUT = 5;
	
	
	public final static String START_ELEVATOR = "Start elevator";
	public final static String ABORT_ELEVATOR = "ABORT elevator";
	public final static String STOP_ELEVATOR = "Stop elevator";
	public final static String RUNNER_ABORTED = "Runner aborted";
	
	public final static String ELEVATOR_CAPACITY_EXCEPTION = "Synchronization error, capacity of elevator less then passengers count";
	
	public final static String ELEVATOR_MOVE_FROM = "Moving elevator from ";
	public final static String ELEVATOR_MOVE_TO = " to ";
	public final static String PASSENGER_MOVE_TO_ELEVATOR = "Add pass to elevator : ";
	public final static String PASSENGER_MOVE_FROM_ELEVATOR = "Remove pass from elevator : ";
	
	
	
	public final static String COUNT_PAS_TO_DEPART = "Count passengers to depart: ";
	public final static String COUNT_PAS_IN_ELEVATOR = "Count passengers in elevator: ";
	public final static String COUNT_PAS_ARRIVAL = "Count arrival passengers : ";
	public final static String STATUS_PASSENGERS_AND_STORIES = "Distanation stories and statuses passengers is: ";
}
