package epam.beans;

import java.util.List;
import java.util.ArrayList;

import epam.interfaces.Waitable;

public class Story implements Waitable{
	private List<Passenger> departList = new ArrayList<Passenger>();
	private List<Passenger> arrivalList = new ArrayList<Passenger>();
	int storyId;
	
	Story(int storyId){
		super();
		this.storyId = storyId;
	}
	

	public void addToDepart(Passenger pas){
		departList.add(pas);
	}
	
	public void removeFromDepart(Passenger pas){
		departList.remove(pas);
	}
	
	public List<Passenger> getDepartList(){
		return departList;
	}
	
	public synchronized boolean isEmptyStory(){
		return departList.isEmpty();
	}
	
	public List<Passenger> getArrivalList(){
		return arrivalList;
	}
	
	public void addToArrival(Passenger pas){
		arrivalList.add(pas);
	}
	
	public int getStoryId(){
		return storyId;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder str = new StringBuilder();
		for (Passenger pas : departList){
			str.append(pas + " ");
		}
		if (departList.size() == 0){
			str.append("none");
		}
		return str.toString();
	}

	public int getCounter() {
		// TODO Auto-generated method stub
		return departList.size();
	}
}
