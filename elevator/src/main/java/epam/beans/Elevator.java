package epam.beans;

import static epam.logging.LoggerClass.LOGGER;

import java.util.LinkedList;
import java.util.List;
//import java.util.Random;

import epam.interfaces.DrawElevator;
import epam.interfaces.Waitable;

import static epam.beans.Constants.*;

public class Elevator implements Waitable, DrawElevator {

	private final int ELEVATOR_CAPACITY;
	private final int STORY_COUNT;

	private int currentStory = 0;
	private double currentStoryYPos = 0;
	private int direction = 1;

	private List<Passenger> elevatorContainer = new LinkedList<Passenger>();

	// --------------------------

	public Elevator(int capacity, int storyCount) {
		super();
		ELEVATOR_CAPACITY = capacity;
		STORY_COUNT = storyCount;
	}

	public void moveElevator() throws InterruptedException {
		animateElevator();
		LOGGER.info(ELEVATOR_MOVE_FROM + currentStory + ELEVATOR_MOVE_TO
				+ (currentStory + direction));
		// System.out.print("from "+(int)currentStory);

		currentStory += direction;

		if (currentStory == 0 || currentStory == STORY_COUNT - 1) {
			direction *= -1;
		}
		// System.out.print(" to "+(int)currentStory + "\n");
	}

	public int getCurrentStory() {
		return currentStory;
	}

	public double getCurrentStoryYPos() {
		return currentStoryYPos;
	}

	private/**/synchronized boolean isFull() {
		if (elevatorContainer.size() > ELEVATOR_CAPACITY) {
			throw new IllegalStateException(ELEVATOR_CAPACITY_EXCEPTION);
		}
		return (elevatorContainer.size() == ELEVATOR_CAPACITY);
	}

	public/**/synchronized boolean isEmpty() {
		return elevatorContainer.isEmpty();
	}

	public/**/synchronized boolean addPassengerToElevator(Passenger pas)
			throws InterruptedException {

		if (!isFull() && direction == pas.getDirection()) {
			LOGGER.info(PASSENGER_MOVE_TO_ELEVATOR + pas);
			// System.out.println("Add pass to elevator : " + pas);

			// animation move pas in elevator
			//animatePassToElevator(pas);
			pas.animateToElevator();

			elevatorContainer.add(pas);

			return true;
		} else {
			// System.out.println("Elevator is full ...." + pas);
			return false;
		}

	}

	public/**/synchronized boolean removePassenger(Passenger pas)
			throws InterruptedException {
		// decrementPeopleWaitingThreadElevator();

		if (pas.getDestinationStory() == getCurrentStory()) {
			LOGGER.info(PASSENGER_MOVE_FROM_ELEVATOR + pas);
			// System.out.println("Remove pass from elevator : " + pas);
			//animatePassFromElevator(pas);
			pas.animateFromElevator();
			elevatorContainer.remove(pas);
			return true;
		} else {
			// System.out.println("Remove fail not need story : " + pas);
			return false;
		}
	}

	public List<Passenger> getElevatorContainer() {
		return elevatorContainer;
	}

	public int getCountPeopleInElevator() {
		return elevatorContainer.size();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("elevator");
		for (int i = 0; i < elevatorContainer.size(); i++) {
			sb.append(elevatorContainer.get(i));
		}
		return sb.toString();
	}

	public boolean hasPassengersOnExit() {
		for (Passenger pas : elevatorContainer) {
			if (pas.getDestinationStory() == (int) currentStory) {
				return true;
			}
		}
		return false;
	}

	private void animateElevator() throws InterruptedException {
		// TODO Auto-generated method stub
		Config cfg = Config.getInstance();
		double oldStory = currentStoryYPos;
		for (;;) {
			currentStoryYPos += ELEVATOR_SPEED * direction;
			for (Passenger pas : elevatorContainer) {
				pas.setCurrentStoryYPos(currentStoryYPos);
			}
			if (Math.abs(oldStory - currentStoryYPos) > 1) {
				currentStoryYPos = oldStory + direction;
				break;
			}
			Thread.sleep(ANIMATION_TIMEOUT * cfg.getAnimationBoost());
		}

	}

	public int getCounter() {
		// TODO Auto-generated method stub
		return elevatorContainer.size();
	}
	
	
	public double getElevatorYPosition(){
		return (STORY_COUNT-1.0-getCurrentStoryYPos())/STORY_COUNT;
	}
	
	public int getElevatorCurrentStory(){
		return getCurrentStory();
	}

}
