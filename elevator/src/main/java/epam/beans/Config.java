package epam.beans;

public class Config {
	private int storiesNumber	;
	private int elevatorCapacity ;
	private int passengersNumber ;
	private int animationBoost 	;
	
	private static volatile Config instance;
	
	private Config(){
		storiesNumber	 = 3;
		elevatorCapacity = 1;
		passengersNumber = 2;
		animationBoost 	 = 0;
	}
	
	public static Config getInstance() {
		if (instance == null) {
			synchronized (Config.class) {
				if (instance == null) {
					instance = new Config();
				}
			}
		}
		return instance;
	}

	public int getStoriesNumber() {
		return storiesNumber;
	}

	public void setStoriesNumber(int storiesNumber) {
		this.storiesNumber = storiesNumber;
	}

	public int getElevatorCapacity() {
		return elevatorCapacity;
	}

	public void setElevatorCapacity(int elevatorCapacity) {
		this.elevatorCapacity = elevatorCapacity;
	}

	public int getPassengersNumber() {
		return passengersNumber;
	}

	public void setPassengersNumber(int passengersNumber) {
		this.passengersNumber = passengersNumber;
	}

	public int getAnimationBoost() {
		return animationBoost;
	}

	public void setAnimationBoost(int animationBoost) {
		this.animationBoost = animationBoost;
	}
	
	public void setParameters(int storiesNumber, int elevatorCapacity, int passengersNumber,int animationBoost) {
		this.storiesNumber = storiesNumber;
		this.elevatorCapacity = elevatorCapacity;
		this.passengersNumber = passengersNumber;
		this.animationBoost = animationBoost;
	}
	
	
}
