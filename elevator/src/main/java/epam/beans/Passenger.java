package epam.beans;

import java.util.Random;
import static epam.beans.Constants.*;

import epam.enums.PassengerState;
import epam.interfaces.DrawPeople;

public class Passenger implements DrawPeople{
//	public enum Sex{
//		MALE,
//		FEMALE;
//
//	}
	
	private int destinationStory;
	private int departureStory; 
	private int direction;
	
	private double currentStoryYPos; 
	private double xPos;
	
	private final int sex;
	private int passengerID;

	private PassengerState transportationState = PassengerState.NOT_STARTED;
	
	
	public Passenger(int passengerID, int destinationStory, int departureStory){//, Elevator elevator) {
		super();
		this.passengerID = passengerID;
		this.destinationStory = destinationStory;
		this.departureStory = departureStory;
		this.currentStoryYPos = departureStory;
		this.direction = (departureStory>destinationStory)?-1:1;
		Random rand = new Random();
		this.sex = rand.nextInt(2);

		xPos = rand.nextInt(Constants.START_END_POSITION_WIDTH+Constants.START_POSITION);
	}
	
	public int getDirection(){
		return direction;
	}
	
	public int getSex(){
		return sex;
	}
	
	public double getXPos(){
		return xPos;
	}
	
	public void setXPos(double xPos){
		this.xPos = xPos;
	}
	
	public void setCurrentStoryYPos(double currentStoryYPos){
		this.currentStoryYPos = currentStoryYPos;
	}
	
	public double getCurrentStoryYPos(){
		return this.currentStoryYPos ;
	}
	
	@Override
	public String toString() {
		return "[ID=" + passengerID
				+ ", to=" + destinationStory + "]";
	}

	public int getDestinationStory() {
		return destinationStory;
	}

	public PassengerState getTransportationState() {
		return transportationState;
	}

	public void setTransportationState(PassengerState transportationState) {
		this.transportationState = transportationState;
	}

	
	public int getDepartureStory() {
		return departureStory;
	}
	
	public void animateToElevator()
			throws InterruptedException {
		// TODO Auto-generated method stub
		Config cfg = Config.getInstance();

		double oldXPos = this.getXPos();
		Random rand = new Random();
		double newXPos = Constants.ELEVATOR_POSITION
				- Constants.ELEVATOR_WIDTH
				/ 2
				+ rand.nextInt(Constants.ELEVATOR_WIDTH
						- Constants.PEOPLE_WIDTH);

		for (; newXPos - oldXPos > 0;) {
			oldXPos += PASSENGER_SPEED;
			this.setXPos(oldXPos);
			Thread.sleep(ANIMATION_TIMEOUT * cfg.getAnimationBoost());
		}
	}
	
	
	
	public void animateFromElevator()
			throws InterruptedException {
		// TODO Auto-generated method stub
		Config cfg = Config.getInstance();
		double oldXPos = this.getXPos();
		Random rand = new Random();
		double newXPos = Constants.END_POSITION - Constants.PEOPLE_WIDTH
				+ rand.nextInt(Constants.START_END_POSITION_WIDTH);

		for (; newXPos - oldXPos > 0;) {
			oldXPos += PASSENGER_SPEED;
			this.setXPos(oldXPos);
			Thread.sleep(ANIMATION_TIMEOUT * cfg.getAnimationBoost());
		}

	}
	
}
