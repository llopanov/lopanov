package epam.form;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import epam.beans.Config;
import epam.beans.Controller;
import epam.logging.TextAreaAppender;


public class MainWindow extends JFrame  implements Runnable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String BUTTON_START_TEXT = "Start"; 
	private final String BUTTON_STOP_TEXT = "Stop"; 
	private final String LABEL_PASSENGERS_NUMBER = "Passengers count:"; 
	private final String LABEL_ELEVATOR_CAPACITY = "Elevator capacity:";
	private final String LABEL_STORIES_NUMBER = "Stories count:";
	//private final String LABEL_ANIMATION_BOOST= "Animation boost";
	
	private JButton buttonStart = new JButton(BUTTON_START_TEXT);
	private JButton buttonStop = new JButton(BUTTON_STOP_TEXT);
	
	private JSpinner passengersNumber;
	private JSpinner elevatorCapacity;
	private JSpinner storiesNumber;
	private JLabel labelPassengersNumber = new JLabel(LABEL_PASSENGERS_NUMBER);
	private JLabel labelElevatorCapacity = new JLabel(LABEL_ELEVATOR_CAPACITY);
	private JLabel labelStoriesNumber = new JLabel(LABEL_STORIES_NUMBER);
	private JSlider animationBoost;
	private JTextArea console;
	private ElevatorGraphPanel elavatorBackground;
		
	private ThreadGroup threadGroup = new ThreadGroup ("A");
	
	
	public MainWindow(){
		super("Elevator");
		
		Config cfg = Config.getInstance();
		
		passengersNumber = new JSpinner(new SpinnerNumberModel(cfg.getPassengersNumber(),1,1000,1));
		elevatorCapacity = new JSpinner(new SpinnerNumberModel(cfg.getElevatorCapacity(),1,100,1));
		storiesNumber = new JSpinner(new SpinnerNumberModel(cfg.getStoriesNumber(),1,100,1));
		animationBoost = new JSlider(0,5,1);
		console = new JTextArea();
		console.setRows(6);
		JScrollPane sp = new JScrollPane(console);
		//console.setSize(this.getWidth(), 100);
		console.setEditable(false);
		
		this.setSize(600, 400);
		this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLayout(new BorderLayout());
		
		JPanel leftPanel = new JPanel(new GridBagLayout());
	    JPanel rightPanel = new JPanel(new BorderLayout()); 
	    JPanel leftContentPanel = new JPanel(new GridBagLayout());
	    

	    leftContentPanel.add(leftPanel, new GridBagConstraints(0,0,1,1,0.1,0.1, 
		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(20, 20, 20, 20), 0, 0));
	    this.add(leftContentPanel, BorderLayout.WEST);
	    this.add(sp, BorderLayout.SOUTH);
	    this.add(rightPanel, BorderLayout.CENTER);
	    
	    leftPanel.add(labelPassengersNumber, new GridBagConstraints(0,0,1,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(0, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(passengersNumber, new GridBagConstraints(1,0,1,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(0, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(labelElevatorCapacity, new GridBagConstraints(0,1,1,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(elevatorCapacity, new GridBagConstraints(1,1,1,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(labelStoriesNumber, new GridBagConstraints(0,2,1,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(storiesNumber, new GridBagConstraints(1,2,1,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(animationBoost, new GridBagConstraints(0,3,2,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(buttonStart, new GridBagConstraints(0,4,2,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    leftPanel.add(buttonStop, new GridBagConstraints(0,5,2,1,1,1, 
	    		GridBagConstraints.NORTH,GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
	    
	    elavatorBackground = new ElevatorGraphPanel();
	    rightPanel.add(elavatorBackground, BorderLayout.CENTER);

	    buttonStart.addActionListener(new ButtonStartEventListener());
	    buttonStop.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				threadGroup.interrupt();
			}
		});
	    animationBoost.addChangeListener(new SpinnerChangeListener());
	    
	    
	    TextAreaAppender.setTextArea(console);
		
	}
	

	class ButtonStartEventListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			Config cfg = Config.getInstance();
			
			int storyCnt = (Integer)storiesNumber.getValue();
			int elevCapacity = (Integer)elevatorCapacity.getValue();
			int countPass = (Integer)passengersNumber.getValue();
			int animBoost = animationBoost.getValue();
			
			cfg.setParameters(storyCnt, elevCapacity, countPass, animBoost);
				
			Controller controller = new Controller(storyCnt, elevCapacity, countPass);
			elavatorBackground.setController(controller.getStoriesPassengersCoord(), controller.getElevator(), cfg.getStoriesNumber());
			Thread th = new Thread(threadGroup,controller);
			th.start();

		}
	}
	
	class SpinnerChangeListener implements ChangeListener {

		public void stateChanged(ChangeEvent arg0) {
			// TODO Auto-generated method stub
			//System.out.println("Go was pres" + animationBoost.getValue());
			Config cfg = Config.getInstance();
			
			cfg.setAnimationBoost(animationBoost.getValue());
		}
		
	}


	public void run() {
		// TODO Auto-generated method stub
		for(;;){
			repaint();
			try {
				Thread.sleep(33);//30fps
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
}
