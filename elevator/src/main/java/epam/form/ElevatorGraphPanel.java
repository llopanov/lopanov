package epam.form;

import java.awt.*;

import javax.swing.*;

import epam.beans.Controller;
import epam.beans.Constants;
import epam.interfaces.DrawElevator;
import epam.interfaces.DrawPeople;
import java.util.List;

public class ElevatorGraphPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int countStories;

	//private Controller controller;
	private DrawElevator elevator = null;
	private List <DrawPeople> peoples = null;
	
	private int width;
	private int height;
	private int heightStory;
	private int widthElevator;
	private int heightGirl;
	private int heightBoy;
	private int widthGirl ;
	private int widthBoy ;
	
	private final int FONT_SIZE = 12;
	
	
	public ElevatorGraphPanel() {
        setOpaque(true);
    }

	@Override
	protected void paintComponent(Graphics g) {
		
		// TODO Auto-generated method stub
//		//super.paintComponent(g);
		if (elevator == null || peoples == null){
			return;
		}
		
		this.width = getWidth();
		this.height = getHeight(); 
		this.heightStory = getHeight()/countStories;
		this.widthElevator = width*Constants.ELEVATOR_WIDTH/100;//(int)(elevatorWidth*height);
		this.heightGirl = heightStory/2;
		this.heightBoy = heightStory/2;
		this.widthGirl = width*Constants.PEOPLE_WIDTH/100;
		this.widthBoy = width*Constants.PEOPLE_WIDTH/100;
		
		drawStories(g);
		drawElevator(g);
		drawPassengers(g);
		
	}
	
	private void drawPassengers(Graphics g) {
		// TODO Auto-generated method stub
		// people
		for (DrawPeople pass : peoples){
			//System.out.println("xpos = "+pass.getXPos()+"   ypos = "+pass.getCurrentStoryYPos());
			int xPos = (int)(pass.getXPos()*width/100);
			int yPos = (int)((countStories-pass.getCurrentStoryYPos())*heightStory)-heightGirl;
			if (pass.getSex() == 1){
				drawBoy(g, xPos, yPos, widthBoy, heightBoy, pass.getDestinationStory());
			} else {
				drawGirl(g,xPos, yPos, widthGirl,heightGirl,pass.getDestinationStory());
			}
		}

	}

	private void drawElevator(Graphics g) {
		// TODO Auto-generated method stub
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaint(Color.darkGray);
		// elevator
		//DrawElevator elevator = controller.getElevator();
		g2d.drawRect((width/2 -widthElevator/2), (int)(elevator.getElevatorYPosition()*getHeight()), widthElevator, heightStory);
				
		
		g2d.setFont(new Font("Comic Sans MS", Font.PLAIN, FONT_SIZE));
		FontMetrics fm = g.getFontMetrics();
		String story = Integer.toString(elevator.getElevatorCurrentStory());
		int textWidth = (int)fm.getStringBounds(story, g).getWidth();
		g2d.drawArc(width/2 - widthElevator/4, (int)(elevator.getElevatorYPosition()*getHeight())-13, widthElevator/2, 26, 180, 180);
		g2d.setPaint(Color.red);
		g2d.drawString(story,width/2-textWidth/2, (int)(elevator.getElevatorYPosition()*getHeight())+FONT_SIZE);

	}

	public void setController(List <DrawPeople> peoples, DrawElevator elevator, int countStories){
		//this.controller = controller;
		this.peoples = peoples;
		this.elevator = elevator;
        this.countStories = countStories;
        
	}
	
	private void drawStories(Graphics g){

		int width = getWidth();
		Graphics2D g2d = (Graphics2D) g;
		//g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setPaint(Color.darkGray);
				
		//FontMetrics fm = g.getFontMetrics();
		
		
		
		for (int i = 1; i <countStories; i++) {
			g2d.setPaint(Color.darkGray);
			g2d.drawLine(0, i*height/countStories, width/2  - widthElevator/2, i*height/countStories);
			g2d.drawLine(width/2 + widthElevator/2, i*height/countStories, width, i*height/countStories);
			
		}
		g2d.setFont(new Font("Comic Sans MS", Font.BOLD, heightStory*4/5));
		g2d.setPaint(Color.lightGray);
		for (int i = 0; i <countStories; i++) {
			g2d.drawString(Integer.toString(countStories-i-1),20, i*height/countStories+heightStory*4/5);
		}
		

	}
	
	private void drawGirl(Graphics g, int xPos, int yPos, int width, int height, int distanation){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setFont(new Font("Comic Sans MS", Font.PLAIN, FONT_SIZE));
		g2d.setPaint(Color.darkGray);
		FontMetrics fm = g.getFontMetrics();
		double textWidth = fm.getStringBounds(Integer.toString(distanation), g).getWidth();
		g2d.drawString(Integer.toString(distanation), (int) (width+xPos - textWidth/2),
                (int) (yPos + fm.getMaxAscent() / 2));
		
		//girl
		
		//hear
		g2d.setColor(new Color(200,150,40));
		int xArr0[]={(int)(1./3*width), (int)(1./6*width), (int)(5./6*width), (int)(2./3*width), (int)(0.5*width)};
		int yArr0[]={0, (int)(0.25*height), (int)(0.25*height), 0,0};
		recalcToPos(xPos, yPos, xArr0, yArr0, xArr0.length);
		g2d.fillPolygon(xArr0, yArr0, xArr0.length);
				
		//head
		g2d.setColor(new Color(255,254,182));
		g2d.fillOval((int)(2./6*width)+xPos, 0+yPos,   (int)(2./6*width), (int)(1./4*height));
		
		//dress
		g2d.setColor(new Color(230,30,130));
		int xArr[]={(int)(width/6.), (int)(2*width/6.), (int)(4*width/6.), (int)(5./6*width), (int)(width/6.)};
		int yArr[]={(int)(0.75*height), (int)(0.25*height), (int)(0.25*height), (int)(0.75*height), (int)(0.75*height)};
		recalcToPos(xPos, yPos, xArr, yArr, xArr.length);
		g2d.fillPolygon(xArr, yArr, xArr.length);
		
		// right arm
		int xArr1[]={(int)(2./3*width), width, (int)(1./3*width), (int)(2./3*width)};
		int yArr1[]={(int)(1./4*height), (int)(1./2*height), (int)(5./16*height), (int)(1./4*height)};
		recalcToPos(xPos, yPos, xArr1, yArr1, xArr1.length);
		g2d.fillPolygon(xArr1, yArr1, xArr1.length);
					
		//left arm
		int xArr2[]={(int)(1./3*width), 0, (int)(2./3*width), (int)(1./3*width)};
		int yArr2[]={(int)(1./4*height), (int)(1./2*height), (int)(5./16*height), (int)(1./4*height)};
		recalcToPos(xPos, yPos, xArr2, yArr2, xArr2.length);
		g2d.fillPolygon(xArr2, yArr2, xArr2.length);
						
		//legs
		g2d.setColor(new Color(60,40,2));
		g2d.fillRect((int)(1./3*width)+xPos, (int)(3./4*height)+yPos, (int)(1./12*width), (int)(1./4*height));
		g2d.fillRect((int)(7./12*width)+xPos, (int)(3./4*height)+yPos, (int)(1./12*width), (int)(1./4*height));

	}
	
	private void recalcToPos(int xPos, int yPos, int xArr[], int yArr[], int length ){
		for (int i=0; i<length; i++){
			xArr[i] += xPos;
			yArr[i] += yPos;
		}
	}
	
	private void drawBoy(Graphics g, int xPos, int yPos, int width, int height, int distanation){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setFont(new Font("Comic Sans MS", Font.PLAIN, FONT_SIZE));
		g2d.setPaint(Color.darkGray);
		FontMetrics fm = g.getFontMetrics();
		double textWidth = fm.getStringBounds(Integer.toString(distanation), g).getWidth();
		g2d.drawString(Integer.toString(distanation), (int) (width+xPos - textWidth/2),
                (int) (yPos + fm.getMaxAscent() / 2));
		
		//boy
		
		//head
		g2d.setColor(new Color(255,254,182));
		g2d.fillOval((int)(4./12*width)+xPos, 0+yPos,   (int)(4./12*width), (int)(0.2*height));
		
		//coat
		g2d.setColor(new Color(22,113,143));
		int xArr[]={(int)(width/3.), (int)(1*width/6.), (int)(5*width/6.), (int)(2./3*width), (int)(width/3.)};
		int yArr[]={(int)(0.7*height), (int)(0.2*height), (int)(0.2*height), (int)(0.7*height), (int)(0.7*height)};
		recalcToPos(xPos, yPos, xArr, yArr, xArr.length);
		g2d.fillPolygon(xArr, yArr, xArr.length);
		
		// right arm
		g2d.setColor(new Color(22,113,143));
		int xArr1[]={(int)(5./6*width), width, (int)(2*width/3.), (int)(5./6*width)};
		int yArr1[]={(int)(0.2*height), (int)(1./2*height), (int)(0.3*height), (int)(0.2*height)};
		recalcToPos(xPos, yPos, xArr1, yArr1, xArr1.length);
		g2d.fillPolygon(xArr1, yArr1, xArr1.length);
				
		//left arm
		g2d.setColor(new Color(22,113,143));
		int xArr2[]={(int)(1./6*width), 0, (int)(1./3*width), (int)(1./6*width)};
		int yArr2[]={(int)(0.2*height), (int)(0.5*height), (int)(0.3*height), (int)(0.2*height)};
		recalcToPos(xPos, yPos, xArr2, yArr2, xArr2.length);
		g2d.fillPolygon(xArr2, yArr2, xArr2.length);
	
			
		//legs
		g2d.setColor(new Color(80,60,70));
		g2d.fillRect((int)(1./3*width)+xPos, (int)(0.7*height)+yPos, (int)(1./12*width), (int)(0.3*height));
		g2d.fillRect((int)(7./12*width)+xPos, (int)(0.7*height)+yPos, (int)(1./12*width), (int)(0.3*height));

	}
//	private long t = System.nanoTime();


}
