package epam.interfaces;

public interface DrawPeople {
	double getXPos();
	double getCurrentStoryYPos();
	int getDestinationStory();
	int getSex();
}
