package epam.interfaces;

public interface Waitable {
	int getCounter();
}
