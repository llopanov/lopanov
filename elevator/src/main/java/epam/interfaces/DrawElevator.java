package epam.interfaces;

public interface DrawElevator {
	double getElevatorYPosition();
	int getElevatorCurrentStory();
}
