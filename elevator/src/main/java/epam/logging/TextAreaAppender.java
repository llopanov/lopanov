package epam.logging;

import java.io.Serializable;

import javax.swing.JTextArea;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name = "TextAreaAppender", category = "Core", elementType = "appender", printObject = true)

public final class TextAreaAppender extends AbstractAppender {
	private static JTextArea textArea = null;
	
	protected TextAreaAppender(String name, Filter filter, Layout<? extends Serializable> layout,
			final boolean ignoreExceptions) {
		super(name, filter, layout, ignoreExceptions);
	}

	public synchronized void append(LogEvent event) {

		String message = new String(getLayout().toByteArray(event));
        textArea.append(message);
        int length = textArea.getText().length();
        textArea.setCaretPosition(length);
		
	}

	@PluginFactory
	public static TextAreaAppender createAppender(@PluginAttribute("name") String name,
			@PluginElement("Layout") Layout<? extends Serializable> layout,
			@PluginElement("Filter") final Filter filter) {
		if (name == null) {
			LOGGER.error("No name provided for TextAreaAppender");
			return null;
		}
		if (layout == null) {
			layout = PatternLayout.createDefaultLayout();
		}
		return new TextAreaAppender(name, filter, layout, true);
	}

	public static void setTextArea(JTextArea textArea) {
		
		TextAreaAppender.textArea = textArea;
		//System.out.println("3" +(this));
	}
}

