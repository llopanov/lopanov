//console.log('Hello');

var myModule = myModule || {};

 myModule = (function(module){

	function Character(health, name){
		this.health = health;
		this.name  = name;
		this.position = 0;
		this.isLeave = true;
	}

	Character.prototype.orr = function(){
		console.log("I am "+this.name);
	};

	var Temp = function(){};
	Temp.prototype = Character.prototype;


	module.Dragon = function Dragon (health, name){
		Character.apply(this, arguments);
		this.damage = 10;
	};

	module.Dragon.prototype = new Temp();

	module.Dragon.prototype.attack = function(character){
		if(character.isLeave === true){
			character.health -= this.damage;
			if (character.health <= 0 ){
				character.isLeave = false;
			}
		}
		console.log(this.orr);
		return character.isLeave;
	};

	

	module.Peasant = function Peasant (health, name){
		Character.apply(this, arguments);
		this.speed = 20;
	};

	module.Peasant.prototype = Object.create(Character.prototype);
	//Peasant.prototype.constructor = Peasant;

	module.Peasant.prototype.run = function(){
		this.position += this.speed;
		console.log("I run");
	};

	return module;
}(myModule ));



var peasant = new myModule.Peasant(20, "Erik");
var dragon = new myModule.Dragon(200, "Smoug");

peasant.orr();
dragon.orr();

console.log("Erick was attacked dragon, and is he leave? - " +dragon.attack(peasant));
console.log("Erick run");
peasant.run();

console.dir(peasant);
console.dir(dragon);

