 document.addEventListener("DOMContentLoaded", function(event) { 
 	function Exception(mes){
		this._message = mes;
	}

	Exception.prototype._message = "";

	Exception.prototype.getMessage = function(){
		return this._message;
	}

	function MyButton(){
		this._pressed = false;
		this._handler = this.press.bind(this);
	} 

	MyButton.prototype._element;
	
	MyButton.prototype.press = function(){
		this._pressed = !this._pressed;
		this._element.classList.toggle("activate");
	}

	MyButton.prototype.attach = function(el){
		
		if (!el){
			throw new Exception("empty element");
		}

		this._element = el;
		var self = this;
		this._element.addEventListener("click", this._handler);
	}

	MyButton.prototype.detach = function(){
		this._element.removeEventListener("click", this._handler);
		this._element = null;
		this._pressed = false;
	}

	var list = document.querySelectorAll(".button");
	
	for(i=0; i<list.length; i++){
		try{
			var button = new MyButton();
			button.attach(list[i]);
		} catch (ex){
			console.log(ex.getMessage());
		}

	}

});