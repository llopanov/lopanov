 document.addEventListener("DOMContentLoaded", function(event) { 
 	
 	//////////////////////////////////////
 	/*
		Exception class
 	*/
 	function Exception(mes){
		this._message = mes;
	}

	Exception.prototype._message = "";

	Exception.prototype.getMessage = function(){
		return this._message;
	}

	//////////////////////////////////////
 	/*
		Box with items of strings
 	*/
	function ContainerItems(elementInput){
		this._parentInput = elementInput;
		this._elemContainer = document.createElement('div');
		this._elemContainer.className = 'boxData';
		//elementInput.parentNode.insertBefore(this._elemContainer, elementInput.nextSibling);
		this._beginWidth = elementInput.offsetWidth;
		elementInput.parentNode.insertBefore(this._elemContainer,elementInput);

	}
	ContainerItems.prototype._elemContainer = null;
	ContainerItems.prototype._arrayItems = [];
	ContainerItems.prototype._beginWidth = 0;
	ContainerItems.prototype._minInputWidthSpace = 10;
	ContainerItems.prototype._parentInput = null;
	ContainerItems.prototype._notShowed = 0;

	ContainerItems.prototype.addItem = function (string){
		var item = new Item (string); //this.constructItem(string);
		//console.log(item.getCloseElement());
		var self = this;
		item.getCloseElement().addEventListener("click",	function(){
				self.removeItem.call(self, item.getCloseElement());
			});
			//this.removeItem);

		this._arrayItems.push(item);

		//this.putItemsToParent();

		this.appendToView(item);
		//this.putItemsToParent();
	}

ContainerItems.prototype.appendToView = function (item){
	this._elemContainer.appendChild(item.getItemElement(), this._elemContainer);
	var newInputWidth = this._parentInput.offsetWidth - item.getItemElement().offsetWidth;
		// console.log("w " +newInputWidth);
		// console.log("hidden " +this._notShowed);
		// console.log("arrlen " + this._arrayItems.length);
		
	while ((newInputWidth < ContainerItems.prototype._minInputWidthSpace) && (this._notShowed < this._arrayItems.length)){
		newInputWidth += this._arrayItems[this._notShowed].getItemElement().offsetWidth;
		//this._arrayItems[this._notShowed].getItemElement().remove();
		this._arrayItems[this._notShowed].getItemElement().style.display = "none";
		this._notShowed++;
	}

	this._parentInput.style.width = newInputWidth +"px";
}


	ContainerItems.prototype.removeItem = function(el){
		for(var i=0; i<this._arrayItems.length; i++){
			
		 	if (this._arrayItems[i].getCloseElement() === el){
				this._arrayItems[i].getCloseElement().removeEventListener("click", el);

				this.removeFromView(this._arrayItems[i].getItemElement());

				this._arrayItems.splice(i,1);


				//this.putItemsToParent();
		 	}
		}
	}

ContainerItems.prototype.removeFromView = function (item){
	var newInputWidth = this._parentInput.offsetWidth + item.offsetWidth;

	// console.log("w " +newInputWidth);
	// console.log("hidden " +this._notShowed);
	// console.log("arrlen " + this._arrayItems.length);
	
	
	while (this._notShowed > 0){
		var tempWidth = newInputWidth - this._arrayItems[this._notShowed-1].getItemElement().offsetWidth;
		if (tempWidth < ContainerItems.prototype._minInputWidthSpace) {
			break;
		}
		newInputWidth = tempWidth;
		//this._arrayItems[this._notShowed].getItemElement().remove();
		this._arrayItems[this._notShowed-1].getItemElement().style.display = "inline-block";
		//item.parentNode.insertBefore(this._arrayItems[this._notShowed-1].getItemElement(),item.parentNode.firstChild);
		this._notShowed--;
	}

	this._parentInput.style.width = newInputWidth +"px";
	item.remove();	
}

// 	ContainerItems.prototype.putItemsToParent = function(){
// var i ;


// for(i=0; i<this._arrayItems.length; i++){
// 	this._elemContainer.appendChild(this._arrayItems[i].getItemElement(), this._elemContainer);

// }
// this._width = this._elemContainer.offsetWidth;
// console.log(this._width);
// }

	//////////////////////////////////////
 	/*
		Box with items of strings
 	*/
 	function Item(string){

		this._elem =  document.createElement('span');
		this._elem.innerHTML = string;
		this._elem.className = 'blockText';

		this._closeBtn =  document.createElement('span');
		this._closeBtn.innerHTML = 'x';
		this._closeBtn.className = 'closeBtn';
		this._elem.appendChild(this._closeBtn, this._elem);
	}
	Item.prototype._elem;
	Item.prototype._closeBtn;
	Item.prototype.getCloseElement = function(){
		return this._closeBtn;
	}
	Item.prototype.getItemElement = function(){
		return this._elem;
	}

	//////////////////////////////////////
 	/*
		input class
 	*/
	function MyInput(){
		this._handlerPressedKey = this.pressKey.bind(this);
		this._handlerBlur       = this.inputBlur.bind(this);
	} 

	MyInput.prototype._elementInput;
	MyInput.prototype.itemsArray;
	
	MyInput.prototype.SEPARATOR = " ";
	MyInput.prototype.pressKey = function(key){
		
		if (this.SEPARATOR === String.fromCharCode(key.charCode)){

			this.appendItem();
		}
	}

	MyInput.prototype.inputBlur = function(){
		if (this._elementInput.value.trim()){
			this.appendItem();
		}
		
	}

	MyInput.prototype.appendItem = function(){
		var string = this._elementInput.value.trim();
		if (string.length === 0){
			return;
		}
		this._elementInput.value="";

		this._containerItems.addItem(string);
	}

	MyInput.prototype.attach = function(el){
		
		if (!el){
			throw new Exception("empty element");
		}

		this._elementInput = el;
		this._elementInput.addEventListener("keypress", this._handlerPressedKey);
		this._elementInput.addEventListener("blur", this._handlerBlur);

		this._containerItems = new ContainerItems(this._elementInput);
	}

	MyInput.prototype.detach = function(){
		this._elementInput.removeEventListener("keypress", this._handlerPressedKey);
		this._elementInput.removeEventListener("keypress", this._handlerBlur);
		this._elementInput = null;
	}

	// var list = document.querySelectorAll(".specialInput");

	// for(i=0; i<list.length; i++){
	// 	try{
	// 		var button = new MyInput();
	// 		//console.log(button);
	// 		button.attach(list[i]);
	// 	} catch (ex){
	// 		console.log(ex.getMessage());
	// 	}

	// }	


	function attachAll(selector, constructor){
		var list = document.querySelectorAll(selector);	

		for(i=0; i<list.length; i++){
			try{
				var button = new constructor();
				//console.log(button);
				button.attach(list[i]);
			} catch (ex){
				console.log(ex.getMessage());
			}

		}
	}

	attachAll(".specialInput", MyInput);
});