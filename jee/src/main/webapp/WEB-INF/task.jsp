<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">
<div class="col-sm-11" style="text-align:right" >
				<button id="addReport"  class="btn btn-success" >Add report</button>
				<a href="task/export?taskId=${task.id}" id="exportTask" class="btn btn-default">export to xml</a>
				<input type="hidden" value="${task.id}" name="taskId" id="taskId">
				
			</div>
			<div class="col-sm-1"></div>
</div>
<div class="row">

	<div class="col-sm-1"></div>
	<div class="col-sm-5">
		 <div id="taskActivityList">
		 	<%@ include  file="list_activities.jsp" %>
		 </div>
	</div>
	
	<div class="col-sm-1"></div>
	
	<div class="col-sm-4">
		<h4>Description</h4>
	
		<p>${task.description}</p>
		
		<div>
			<label>Assigned to:</label>
			<select id="taskAssigneeState">
				
				<c:forEach items="${employeeList}" var="employee">
				
					<c:choose>
					<c:when test="${task.assignedEmployee.id == employee.id}">
						<option value="${employee.id}" selected>${employee.firstName} ${employee.lastName}</option>
					</c:when>
					<c:otherwise>
						<option value="${employee.id}">${employee.firstName} ${employee.lastName}</option>
					</c:otherwise>
					</c:choose>
					
			</c:forEach>
			</select>
		</div>
		
		<div>
				<label for="taskStatusState">Task status :</label>
				<select id="taskStatusState">
					<c:forEach items="${statusList}" var="status">
					<c:choose>
						<c:when test="${task.status.id == status.id}">
							<option value="${status.id}" selected>${status.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${status.id}">${status.name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				</select>
			</div>
		
		<c:if test="${not empty task.attachments}">
			<h4>Attachments</h4>
			<table class="table">
			<c:forEach items="${task.attachments}" var="attach" varStatus="loop">
			<tr><td>${attach.name}</td><td><a href="task/downloadfile?taskId=${task.id}&fileId=${attach.id}"><span class="glyphicon glyphicon-download-alt"></span></a></td>
			</c:forEach>
			</table>
		</c:if>
	</div>
	
	<div class="col-sm-1"></div>
</div>