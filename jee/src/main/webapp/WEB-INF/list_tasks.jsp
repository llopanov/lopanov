


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty taskList}">
    <div class="taskListBox">

	<c:forEach items="${taskList}" var="task" varStatus="loop">
	
		<div class="row taskItem">
		  <div class="col-sm-1"><div class="counter">${loop.count+(paginator.currentPage-1)*paginator.countElementsOnPage}</div></div>
		  <div class="description col-sm-7"><a href="task?taskId=${task.id}"> ${task.description} </a></div>
		  <div class="status col-sm-2">${task.status.name}</div>
		  <div class="psd col-sm-2">${task.psdView}</div>
		</div>
	</c:forEach>
	</div>	
		
		<c:if test="${paginator.showPaginator}">
	<nav class="paginator">
		<ul class="pagination pagination-sm">
			<c:forEach var="pageNum" begin="1" end="${paginator.countPages}">
				<c:choose>
					<c:when test="${pageNum != paginator.currentPage}"><li class="pageNum" pageNum="${pageNum}"><a href="#">${pageNum}</a></li></c:when>
					<c:otherwise><li class="pageNum active" pageNum="${pageNum}"><a href="#">${pageNum}</a></li></c:otherwise>
				
				</c:choose>
			</c:forEach>
		</ul>
	</nav>
	</c:if>
</c:if>

<c:if test="${empty taskList}">
no tasks
</c:if>


