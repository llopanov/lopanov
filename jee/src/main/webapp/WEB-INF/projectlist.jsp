<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
<div class="col-sm-12">
	projects

	<c:if test="${not empty projectList}">
		<c:forEach items="${projectList}" var="project" varStatus="loop">
				<div>${loop.count}. ${project.name} </div>
				<div>Members ${project.members}<br/>
				<c:forEach items="${project.members}" var="entry">
				    Key = ${entry.key}, value = ${entry.value}<br>
				</c:forEach>
				</div>
		</c:forEach>
	</c:if>


	<c:if test="${empty projectList}">
	no projects
	</c:if>
	</div>
</div>
