


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:if test="${not empty activityList}">
    <c:forEach items="${activityList}" var="activity">
        <div class="activityBox">
            <div class="activityHeader">
	            <div class="activityEmployee">${activity.employee.firstName} ${activity.employee.lastName} </div>
		        <div class="activityDate">${activity.dateView}</div> 
	        </div>
	        <div class="activityComment">${activity.comment}</div>
        </div>
    </c:forEach>
    <c:if test="${not empty paginatorActivity && paginatorActivity.countPages != paginatorActivity.currentPage}">
        <div class="loadMoreActivitiesBtn btn btn-primary" pageNum="${paginatorActivity.currentPage}">More activities</div>
    </c:if>
</c:if>

<c:if test="${empty activityList}">
no activities
</c:if>


