<%@ include  file="header.jsp" %>

<c:if test="${not empty pagename}">
  <jsp:include page='${pagename}' flush="true"/>
 </c:if>

<%@ include  file="footer.jsp" %>