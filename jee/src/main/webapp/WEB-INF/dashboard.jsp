<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
<div class="col-sm-1"></div>
<div class="col-sm-5">
	<h3>Activity stream</h3>
	<div id="activityList">
	   <%@ include  file="list_activities.jsp" %>
	</div>
	
</div>

<div class="col-sm-5">
	<h3>Assigned tasks</h3>
	<div id="taskList">
	<%@ include  file="list_tasks.jsp" %>
	</div>
</div>
<div class="col-sm-1"></div>
</div>
