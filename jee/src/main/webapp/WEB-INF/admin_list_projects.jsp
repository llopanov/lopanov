


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:if test="${not empty listProjects}">
<table class="table">
<c:forEach items="${listProjects}" var="project" varStatus="loop">
        <tr>
            <td>${loop.count}</td>
            <td>${project.name}</td>
            <td>${project.description}</td>
            
            <td><span class="editProject" rowId="${project.id}" >Edit <span class="glyphicon glyphicon-edit"></span></span></td>
            <td><span class="deleteProject" rowId="${project.id}" >Delete <span class="glyphicon glyphicon-trash"></span></span></td>
        </tr>
    </c:forEach>
</div>
</table>
</c:if>

<c:if test="${empty listProjects}">
no projects
</c:if>


