    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    

    <div class="alert alert-danger errorFormMsg" ></div>
    <form action="#" method="post"  class="form-horizontal" id="reportForm">
        <div  class="form-group">
            <label  for="Comment" class="col-sm-3 control-label">Comment</label>
            <div class="col-sm-9">
            <input type="text" class="form-control" id="comment" required="required" name="Comment" placeholder="Comment here"/>
            </div>
        </div>
        
        <div  class="form-group">
            <label  for="Duration" class="col-sm-3 control-label">Duration</label>
            <div class="col-sm-9">
            <input type="text" class="form-control" id="duration" required="required" name="Duration" placeholder="Duration here"/>
            </div>
        </div>
        
        <input type="hidden" required="required" name="taskId" value="${taskId}"/>
        

        <button type="button" id="execReport" class="btn btn-primary">Ok</button>

    </form>
