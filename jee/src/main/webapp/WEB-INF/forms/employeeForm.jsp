    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
    <div class="alert alert-danger errorFormMsg" ></div>
    <form action="#" method="post"  class="form-horizontal" id="employeeForm">
        <div  class="form-group">
            <label  for="firstName" class="col-sm-2 control-label">first name</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="firstName" required="required" name="FirstName" placeholder="first name here" value="${employee.firstName}"/>
            </div>
        </div>
        
        <div  class="form-group">
            <label  for="lastName" class="col-sm-2 control-label">last name</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="lastName" required="required" name="LastName" placeholder="last name here" value="${employee.lastName}"/>
            </div>
        </div>
        
        <div  class="form-group">
            <label  for="login" class="col-sm-2 control-label">login</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="login" required="required" name="Login" placeholder="login" value="${employee.login}"/>
            </div>
        </div>
        
        <div  class="form-group">
            <label  for="password" class="col-sm-2 control-label">password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" id="taskname" required="required" name="Password" placeholder="password" value=""/>
            </div>
        </div>
        
        <div  class="form-group">
            <label  for="rePassword" class="col-sm-2 control-label">confirm password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" id="confirmPassword" required="required" name="rePassword" placeholder="confirm password" value=""/>
            </div>
        </div>
     
        <div  class="form-group">
            <label  for="position" class="col-sm-2 control-label">position</label>
            <div class="col-sm-10">
            <select class="form-control"  id="position" name="PositionId">
            
            <c:forEach items="${listPositions}" var="position">
	              <c:if test="${employee.position.id == position.id}">
                    <option value="${position.id}" selected>${position.name}</option>
                </c:if>
                 <c:if test="${employee.position.id != position.id}">
                    <option value="${position.id}">${position.name}</option>
                </c:if> 
	            
		    </c:forEach>

			</select>
            </div>
        </div>
        
        <input type="hidden" value="${employee.id}" name="Id"/>




        <button type="button" id="execEmployee" class="btn btn-primary">Ok</button>
        <!--<button type="submit" class="btn btn-primary" name="submit">Add task</button>
        
         <input type="hidden" required="required" name="taskid" value="${param.taskid}"/> -->
    </form>