    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    

    <div class="alert alert-danger errorFormMsg" ></div>
    <form action="#" method="post"  class="form-horizontal" id="taskForm" enctype="multimultipart/form-datapart">
        <div  class="form-group">
            <label  for="project" class="col-sm-3 control-label">Select project</label>
            <div class="col-sm-9">
            <select class="form-control"  id="project" name="ProjectId">
            
            <c:forEach items="${listProjects}" var="project">
	              <c:if test="${selectedProjectId == project.id}">
                    <option value="${project.id}" selected>${project.name}</option>
                </c:if>
                 <c:if test="${selectedProjectId != project.id}">
                    <option value="${project.id}">${project.name}</option>
                </c:if> 
	            
		    </c:forEach>

			</select>
            </div>
        </div>
        
        
        
        <div  class="form-group">
            <label  for="description" class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
            <input type="text" class="form-control" id="description" required="required" name="Description" placeholder="Description here" value="${task.description}"/>
            </div>
        </div>
        

        <div class="form-inline">
        <div class="form-group col-sm-6">
        	<label  for="psd" class="control-label">Plan start date</label>
        
	        <div class="input-group date" >
	        
			    <input type="text" data-date-format="dd-mm-yyyy"  class="form-control" id="psd" required="required" name="Psd"  />
			    <div class="input-group-addon">
			        <span class="glyphicon glyphicon-th"></span>
			    </div>
			    <!--  -->
			</div>
        </div>
        <div class="col-sm-1"></div>

		<div class="form-group col-sm-6">
			<label  for="pdd" class="control-label">Plan end date</label>
	        <div class="input-group date " >
	        
			    <input type="text" data-date-format="dd-mm-yyyy"   class="form-control" id="pdd" required="required" name="Pdd"/>
	            <div class="input-group-addon ">
			        <span class="glyphicon glyphicon-th"></span>
			    </div>
			    <!--  -->
			</div>
     	</div>   
     </div>


<div class="form-group"></div>
        <div class="form-group">
            <label  for="attachment" class="col-sm-3 control-label">Attachments</label>
            <div class="col-sm-9">
            
            <div class="fileList">
            <div><input type="file" name="Attachment" value=""/></div>
            </div>
            <span id="addMoreFile">add more file </span>
            </div>
        </div>
        

     
        <div  class="form-group">
            <label  for="position" class="col-sm-3 control-label">Assignee</label>
            <div class="col-sm-9">
            <select class="form-control"  id="assignee" name="assigneeId">
            
            <c:forEach items="${listEmployees}" var="employee">
                    <option value="${employee.id}">${employee.firstName} ${employee.lastName}</option>
		    </c:forEach>

			</select>
            </div>
        </div>
        

        <button type="button" id="execTask" class="btn btn-primary">Ok</button>

    </form>
