    <form action="login" method="post">
    <div  class="form-group">
        <label class="sr-only" for="login">login</label>
        <input type="text" class="form-control" id="login" required="required" name="login" placeholder="your login" value="${param.login}"/>
    </div>
    <div  class="form-group">
        <label class="sr-only" for="password">password</label>
        <input type="password" class="form-control" id="password" required="required" name="password" placeholder="your password"/>
    </div>
    <button type="submit" class="btn btn-primary">Login</button>
    </form>
    
