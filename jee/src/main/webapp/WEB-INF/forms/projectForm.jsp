    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
    <div class="alert alert-danger errorFormMsg" ></div>
    <form action="#" method="post"  class="form-horizontal" id="projectForm">
        <div  class="form-group">
            <label  for="firstName" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="name" required="required" name="Name" placeholder="first name here" value="${project.name}"/>
            </div>
        </div>
        
        <div  class="form-group">
            <label  for="lastName" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="description" required="required" name="Description" placeholder="description" value="${project.description}"/>
            </div>
        </div>
    <div class="row">
		<div class="employeesListForProject col-sm-4 ">
			
		    <c:forEach items="${employeesList}" var="employee">

		    
		    <c:if test="${empty project.members[employee]}">
		    	<div class="member" empId="${employee.id}">
					<label>
					${employee.firstName} ${employee.lastName}
				 	
				    
					</label>
					
					<div class="add"><span class="glyphicon glyphicon-plus"></span></div>
				    <div class="del"><span class="glyphicon glyphicon-minus"></span></div>
				    <div class="rolesList">
					    <select>
					    <c:forEach items="${rolesList}" var="role">
					    	<option value="${role.id}">${role.name}</option>
					    </c:forEach>
					    </select>
				    </div>
				</div>
		    </c:if>
				
			
		    	
		    	
		    </c:forEach>
		    
	    </div>
	    <div class="col-sm-1"></div>
	    <div class="membersProject col-sm-7">
	    	<c:forEach items="${employeesList}" var="employee">
	   		<c:if test="${not empty project.members[employee]}">
		    	<div class="member" empId="${employee.id}">
					<label>
					${employee.firstName} ${employee.lastName}
				 	
				    
					</label>
					
					<div class="add"><span class="glyphicon glyphicon-plus"></span></div>
				    <div class="del"><span class="glyphicon glyphicon-minus"></span></div>
				    <div class="rolesList">
					    <select>
					    <c:forEach items="${rolesList}" var="role">
					    	<c:choose>
						    	<c:when test="${role.id == project.members[employee].id}">
						    		<option value="${role.id}" selected>${role.name}</option>
						    	</c:when>
						    	<c:otherwise>
						    		<option value="${role.id}" >${role.name}</option>
						    	</c:otherwise>
					    	</c:choose>
					    </c:forEach>
					    </select>
				    </div>
				</div>
		    </c:if>
		    </c:forEach>
	    </div>
    </div>
        
   <!--     <div id="addEmployeeToProject">add employee</div> -->
        
        <input type="hidden" value="${project.id}" name="Id"/>
<!-- 
<div emplId="3" roleId="5" class="emp">name</div>
<div emplId="1" roleId="10" class="emp">name2</div>
<label>Wayne
<select name="2">
<option value="3">qa</option>
<option value="4">teamlead</option>
</select>
</label>

<label>kent
<select name="4">
<option value="3">qa</option>
<option value="4">teamlead</option>
</select>
</label>

-->
	
    

        <button type="button" id="execProject" class="btn btn-primary">Ok</button>
        
    </form>
    
    
    
    