


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:if test="${not empty listEmployees}">
<table class="table">
<c:forEach items="${listEmployees}" var="employee" varStatus="loop">
        <tr>
            <td>${loop.count}</td>
            <td>${employee.firstName}</td>
            <td>${employee.lastName}</td>
            <td>${employee.login}</td>
            <td>${employee.position.name}</td>
            <td><span class="editEmployee" rowId="${employee.id}" >Edit <span class="glyphicon glyphicon-edit"></span></span></td>
            <td><span class="deleteEmployee" rowId="${employee.id}" >Delete <span class="glyphicon glyphicon-trash"></span></span></td>
        </tr>
    </c:forEach>
</div>
</table>
</c:if>

<c:if test="${empty listEmployees}">
no employees
</c:if>

