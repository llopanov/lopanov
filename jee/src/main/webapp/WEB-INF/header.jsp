<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!doctype html>
<html lang="en">
<head>
    <title>jee</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content=''>
    <meta name="description" content=''>
    <link href="<c:url value="/resources/css/normalize.css" />" rel="stylesheet" type="text/css" />
    <!-- <link href="./css/styles.css" rel="stylesheet" type="text/css" /> -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- --><link href="<c:url value="/resources/css/styles.less" />" rel="stylesheet/less" type="text/css" />
    <script src="<c:url value="/resources/js/less/less.min.js"/>" type="text/javascript"></script>
    
    <script src="<c:url value="/resources/js/jquery/jquery-1.7.1.min.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/myscript.js"/>" type="text/javascript"></script>
    
    <link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"  />
    <script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>" type="text/javascript" ></script>
    
    <link href="<c:url value="/resources/datepicker/css/datepicker.css"/>" rel="stylesheet" type="text/css"  />
    <script src="<c:url value="/resources/datepicker/js/bootstrap-datepicker.js"/>" type="text/javascript" ></script>
    
</head>


</head>
<body>
<div class="main">

        <div class="header">
        
        
        
        <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      
      <a class="navbar-brand" href="<c:url value="/"/>">JEE task</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse">
        
      <ul class="nav navbar-nav">
        <c:if test="${sessionScope.Role.showDashboardProject}">
        
                
                <li><a href="<c:url value="/dashboard"/>">Dashboard</a></li>
                <li class="dropdown"><a id="myProjectsBtn" href="#"  class="dropdown-toggle">Projects<span class="caret"></span></a>
                     <ul class="dropdown-menu"></ul> 
                </li>
                <c:if test="${sessionScope.Role.allowTaskCreate}">
                    <li><a href="#" id="taskNewBtn">task add</a></li>
                </c:if>
        </c:if>
        
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <c:if test="${not empty sessionScope.Employee}">
            <li><a href="<c:url value="/logout"/>" id="logoutBtn">Logout ${sessionScope.Employee.login}</a></li>
        </c:if>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
        
        
        
        </div>
        <div class="cont">
        <div class="alert alert-danger" id="errorMsg">
		<c:if test="${not empty errorMessage}">
			<c:out value="${errorMessage}"/>
			</c:if>
		</div>