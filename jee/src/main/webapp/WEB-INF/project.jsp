<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
<div class="col-sm-2"></div>
<div class="col-sm-8">
	
	
<h2>${project.name}</h2>
<p>${project.description}</p>

<h3>Members</h3>
<table class="table table-bordered">
<c:forEach var="member" items="${project.members}" varStatus="status">
<tr>
<td>${member.key.firstName} ${member.key.lastName}</td>
<td> ${member.value.name}</td>
</tr>    
</c:forEach>
</table>
		</div>
		<div class="col-sm-2"></div>
</div>
