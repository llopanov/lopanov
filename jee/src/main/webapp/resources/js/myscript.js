$(document).ready(function () {
	
	$('#addEmployee').click(function (e) {
		$.ajax({
            type: 'GET',
            url: 'admin/employee',
            data:  {
                "Id" : 0
            },
            success:  function(response){
            	$('#popUp').css("display", "block");
            	$('#popUp .content').html(response);
            },
            error: function(response){
            	$('#errorMsg').html("Error on server");
            	$('#errorMsg').show();
            }
        });
	});
	
	$('body').on('click', '#execEmployee', function(){

        var data = $("#employeeForm").serialize();
        $.ajax({
            type: 'POST',
            url: 'admin/employee',
            data: data,
            success:  function(response){
                switch (response.status){
                case 'ok':
                    // refresh list and close window
                    //reloadListTasks();
                    $('#popUp').hide();
                    loadEmployees();
                    break;
                case 'error':
                    $("#popUp .errorFormMsg").html(response.message);
                    $("#popUp .errorFormMsg").show();
                    break;
                }
                
                
            }
        });
    });
	
	
	$('body').on('click', '.editEmployee', function(){
            $.ajax({
                type: 'GET',
                url: 'admin/employee',
                data:  {
                    "Id" : $(this).attr("rowId")
                },
                success:  function(response){
                	$('#popUp').css("display", "block");
                	$('#popUp .content').html(response);
                	return;
                    switch (response.status){
                    case 'ok':
                        // refresh list and close window
                    	$('#popUp').css("display", "block");
                    	$('#popUp .content').html(response.html);
                        //showPopUp();
                        break;
                    case 'error':

                        break;
                    }
                }
            });
    });
	
	$('body').on('click', '.deleteEmployee', function(){
        if (confirm("Are you sure that you want delete employee?")) {
            $.ajax({
                type: 'POST',
                url: 'admin/employee/delete',
                data:  {
                	"Id" : $(this).attr("rowId")
                },
                success:  function(response){
                    switch (response.status){
                    case 'ok':
                        // refresh list and close window
                    	loadEmployees();
                        break;
                    case 'error':

                        break;
                    }
                }
            });
        } 
    });
	
	
	$('#addProject').click(function (e) {
		$.ajax({
            type: 'GET',
            url: 'admin/project',
            data:  {
                "Id" : 0
            },
            success:  function(response){
            	$('#popUp').css("display", "block");
            	$('#popUp .content').html(response);
            	
            },
            error: function(response){
            	$('#popUp').css("display", "block");
            	$('#popUp .content').html(response);
            	
            }, 
        });
	});
	
	
	$('body').on('click', '.employeesListForProject .member .add', function(){

		$(this).parent().detach().prependTo('.membersProject');//.remove();
		
	});
	
	$('body').on('click', '.membersProject .member .del', function(){
		$(this).parent().detach().prependTo('.employeesListForProject');//.remove();
		
	});
	
	$('body').on('click', '#execProject', function(){
        var data = $("#projectForm").serialize();
        var emp = new Object();
        var count = 0;

        $('.membersProject .member').each(function(index, el){
        	emp['employee['+index+'][empid]']= $(this).attr('empId');
        	emp['employee['+index+'][roleid]']= $(this).find('select').val();//role id
        	count++;
        	
        });

        $.ajax({
            type: 'POST',
            url: 'admin/project',
            data: data+'&count='+count+'&'+$.param(emp),//send,//

            success:  function(response){
                switch (response.status){
                case 'ok':
                    // refresh list and close window
                    //reloadListTasks();
                    $('#popUp').hide();
                    loadProjects();
                    break;
                case 'error':
                	//alert(1111);
                    $("#popUp .errorFormMsg").html(response.message);
                    $("#popUp .errorFormMsg").show();
                    break;
                }
                
                
            },
            error: function(response){
            	$("#popUp .errorFormMsg").html(response.message);
                $("#popUp .errorFormMsg").show();
            }
        });
    });
	
	
	$('body').on('click', '.editProject', function(){

            $.ajax({
                type: 'GET',
                url: 'admin/project',
                data:  {
                    "Id" : $(this).attr("rowId")
                },
                success:  function(response){
                	$('#popUp').css("display", "block");
                	$('#popUp .content').html(response);
                	return;
                    switch (response.status){
                    case 'ok':
                        // refresh list and close window
                    	$('#popUp').css("display", "block");
                    	$('#popUp .content').html(response.html);
                        //showPopUp();
                        break;
                    case 'error':

                        break;
                    }
                }
            });
    });
	
	$('body').on('click', '.deleteProject', function(){
 
        if (confirm("Are you sure that you want delete project?")) {
            $.ajax({
                type: 'POST',
                url: 'admin/project/delete',
                data:  {
                	"Id" : $(this).attr("rowId")
                },
                success:  function(response){
                    switch (response.status){
                    case 'ok':
                        // refresh list and close window
                    	loadProjects();
                        break;
                    case 'error':
                    	//errorMessageShow(response.message);
                        break;
                    }
                }
            });
        } 
    });
	
	//=====================================
	
	$('#taskNewBtn').click(function (e) {
		$.ajax({
            type: 'GET',
            url: 'project/newtask',
//            data:  {
//                "Id" : 0
//            },
            success:  function(response){
            	$('#popUp').css("display", "block");
            	$('#popUp .content').html(response);
            }
        });
	});
	
	$('body').on('change', '#project ', function(){
		$.ajax({
            type: 'GET',
            url: 'project/employees',
            data:  {
                "projId" : $(this).val()
            },
            success:  function(response){
            	$('#assignee').empty();
            	var i = 0;
            	while(response[i] !== undefined){
            		$('#assignee').append($('<option value="'+response[i].id+'">'+response[i].firstName + " "+response[i].lastName+'</option>'));//$("<option></option>"))
            		i++;
            	}
            	
            }
        });
	});
	
	$('body').on('focus', '#psd , #pdd ', function(){
		$(this).datepicker({
			minDate:0,
			autoclose: true,

			defaultDate: new Date(),
		    
		});//.datepicker("setDate", new Date());
		
	});
	
	
	$('body').on('click', '#execTask', function(){

        var formData = new FormData($('#taskForm')[0]);//new FormData($('#taskForm')[0]);

        $.ajax({
            type: 'POST',
            url: 'project/newtask',
            //cache: false,
            contentType: false,
            processData: false,
            data: new FormData($('#taskForm')[0]),
            success:  function(response){
                switch (response.status){
                case 'ok':
                    // refresh list and close window
                    loadTasks();
                    $('#popUp').hide();
                    if (response.redirect){
                    	window.location.replace(response.redirect);
                    }
                    break;
                case 'error':
                    $("#popUp .errorFormMsg").html(response.message);
                    $("#popUp .errorFormMsg").show();
                    break;
                }
                
                
            }
        });
    });
	
	
	//===========================================
	$('#taskAssigneeState').change(function(){
		$.ajax({
            type: 'POST',
            url: 'task/setassignee',
            data: {
            	"taskId" : $("#taskId").val(),
                "empId" : $(this).val()
            },
            success:  function(response){
                switch (response.status){
                case 'ok':
                	
                	//window.location.replace(response.redirectURI);
                    // refresh list and close window
                    //reloadListTasks();
                    //$('#popUp').hide();
                    //loadEmployees();
                    break;
                case 'error':
                	$("#errorMsg ").html(response.message);
                    $('#errorMsg').show();
                    break;
                }
                
                
            }
        });
	});
	
	$('#taskStatusState').change(function(){
		$.ajax({
            type: 'POST',
            url: 'task/setstatus',
            data: {
            	"taskId" : $("#taskId").val(),
                "statusId" : $(this).val()
            },
            success:  function(response){
                switch (response.status){
                case 'ok':
                    break;
                case 'error':
                    $("#errorMsg ").html(response.message);
                    $('#errorMsg').show();
                    break;
                }
                
                
            }
        });
	});
	
	$('#addReport').click(function(){
		$.ajax({
            type: 'GET',
            url: 'task/addreport',
            data:  {
                "taskId" : $("#taskId").val()
            },
            success:  function(response){
            	$('#popUp').css("display", "block");
            	$('#popUp .content').html(response);
            }
        });
	});
	
	$('body').on('click', '#execReport', function(){
        var data = $("#reportForm").serialize();
        $.ajax({
            type: 'POST',
            url: 'task/addreport',
            data: data,

            success:  function(response){
                switch (response.status){
                case 'ok':
                    // refresh list and close window
                    loadActivities($("#taskId").val());
                    $('#popUp').hide();
                    break;
                case 'error':
                    $("#popUp .errorFormMsg").html(response.message);
                    $("#popUp .errorFormMsg").show();
                    break;
                }
                
                
            }
        });
    });
	
	
	$('#errorMsg').click(function(){
		$(this).hide();
	});
	
	$('body').on('click', '#addMoreFile', function(){
		$("#taskForm .fileList").append('<div><input type="file" name="Attachment" value=""/> <span id="removeAttachment">remove</span></div>');
	});
	
	$('body').on('click', '#removeAttachment', function(){
		$(this).parent().remove();
	});
	
	
	$('body').on('click', '.paginator .pageNum', function(){
		$.ajax({
	        type: 'POST',
	        url: 'dashboard/tasklist',
	        data:  {
	        	pageNum : $(this).attr("pageNum") 
	        },
	        success:  function(response){
	        	$('#taskList').html(response);
	        	return;
	        }
	    });
		
		
	});
	
	$('body').on('click', '.loadMoreActivitiesBtn', function(){
		$.ajax({
	        type: 'POST',
	        url: 'dashboard/activitylist',
	        data:  {
	        	pageNum : $(this).attr("pageNum") 
	        },
	        success:  function(response){
	        	$('#activityList').html(response);
	        	return;
	        }
	    });
		
		
	});
	
	
	$('#myProjectsBtn').click(function (e) {
		$.ajax({
            type: 'POST',
            url: 'project/list',
            data:  {
               // "Id" : 0
            },
            success:  function(response){

            	if (response.length){
            		var str = '';
            		for (i=0; i<response.length; i++){
	            		
	            		str+='<li><a href="project?projId='+response[i].id+'">'+response[i].name+'</a></li>';
	            	}

            		$('#myProjectsBtn + UL').html(str);
            		$('#myProjectsBtn + UL').show();
	            	
            	}
            	
            },
            error: function(response){
            	$('#popUp').css("display", "block");
            	$('#popUp .content').html(response);
            	
            }, 
        });
	});
	
	$('body').on('mouseleave', '#myProjectsBtn + UL', function(){

		$(this).hide();
	});

});


$(document).mouseup(function (e){ // событие клика по веб-документу
	var div = $(".content"); // тут указываем ID элемента
	var dpBlock = $(".datepicker");
	if (!div.is(e.target) && !dpBlock.is(e.target) && dpBlock.has(e.target).length === 0// если клик был не по нашему блоку
	    && div.has(e.target).length === 0) { // и не по его дочерним элементам
		$("#popUp").hide(); // скрываем его
	}
});	

function loadEmployees(){
	$.ajax({
        type: 'GET',
        url: 'admin/employee/list',
        data:  {
        },
        success:  function(response){
        	$('.adminUsersList').html(response);
        	return;
        }
    });
}

function loadProjects(){
	$.ajax({
        type: 'GET',
        url: 'admin/project/list',
        data:  {
        },
        success:  function(response){
        	$('.adminProjectsList').html(response);
        	return;
        }
    });
}

function loadTasks(){
	$.ajax({
        type: 'POST',
        url: 'task/list',
        data:  {
        	
        },
        success:  function(response){
        	$('#taskList').html(response);
        	return;
        }
    });
}

function loadActivities(id){
	$.ajax({
        type: 'POST',
        url: 'task/activitylist',
        data:  {
        	taskId : id
        },
        success:  function(response){
        	$('#taskActivityList').html(response);
        	return;
        }
    });
}


