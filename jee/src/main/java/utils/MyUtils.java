package utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import bean.Config;
import bean.Constants;
import com.google.gson.Gson;

public class MyUtils {
	private final static SimpleDateFormat FORMATTER = new SimpleDateFormat(Config.DATE_VIEW_STRING_FORMAT);
	private final static SimpleDateFormat FORMATTER_DB = new SimpleDateFormat(Config.DATE_FORMAT_BASE);
	private final static SimpleDateFormat FORMATTER_DB_DATE_HMS = new SimpleDateFormat(Config.DATE_FORMAT_BASE_WITH_TIME);
	
	public static <T> void sendWithJSON (Map<String, T > options, HttpServletResponse response) throws IOException{
		String json = new Gson().toJson(options);

		response.setContentType(Constants.CONTENT_TYPE_JSON);
	    response.setCharacterEncoding(Constants.ENCODING_UTF8);
	    response.getWriter().write(json);
	}
	
	public static String generateFileName(int taskId, String fileName){
		return Integer.toString(taskId)+fileName;
	}
	
	public static  String getDateString(Date date) {
		return FORMATTER.format(date);
	}
	
	public static  String getDateDBString(Date date) {
		return FORMATTER_DB.format(date);
	}
	
	public static  String getDateDBStringHMS(Date date) {
		return FORMATTER_DB_DATE_HMS.format(date);
	}
	
	public static  Date getDateFromDBString(String date) {
		try {
			return new Date(FORMATTER.parse(date).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
		//e.printStackTrace();
			throw new IllegalArgumentException(Constants.ERROR_PARSE_DATE);
		}
		//return null;
	}
	
}
