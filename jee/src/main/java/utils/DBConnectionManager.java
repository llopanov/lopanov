package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bean.Config;
import exceptions.ConnectionException;

public class DBConnectionManager {

	static {
		try {
			Class.forName(Config.DRIVER_NAME);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
     
    public Connection getConnection(){
    	try{
        return DriverManager.getConnection(Config.DB_URL, Config.DB_USER, Config.DB_PASS);
    	} catch (SQLException e) {
			throw new ConnectionException(e.getMessage());
		}
    }
    
    public void closeConnection(Connection connection, Statement statement, ResultSet resultSet) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
            System.err.println("Can't close connection." + e);
		}
	}
    
    public void closeConnections(Connection... connections){
		try {
			for (Connection connection : connections) {
				if (connection != null) {
					connection.close();
				}
			}
		} catch (SQLException e) {
			throw new ConnectionException(e.getMessage());
		}

	}

	public void closeStatements(Statement... statements){
		try {
			for (Statement statement : statements) {
				if (statement != null) {
					statement.close();
				}
			}
		} catch (Exception e) {
			throw new ConnectionException(e.getMessage());
		}

	}
	
	public void closePreparedStatements(PreparedStatement... ps){
		try {
			for (PreparedStatement statement : ps) {
				if (statement != null) {
					statement.close();
				}
			}
		} catch (Exception e) {
			throw new ConnectionException(e.getMessage());
		}

	}

	public void closeResultSets(ResultSet... resultSets){
		try {
			for (ResultSet resultSet : resultSets) {
				if (resultSet != null) {
					resultSet.close();
				}
			}
		} catch (SQLException e) {
			throw new ConnectionException(e.getMessage());
		}

	}

}
