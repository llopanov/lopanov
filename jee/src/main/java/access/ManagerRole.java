package access;

import java.util.ArrayList;
import java.util.List;

public class ManagerRole extends AbstractRole {//implements IRole{//

	private final static List<String > ALLOW_PAGES = new ArrayList<String>();
	private final static String HOME_PAGE ;
	
	static {
		HOME_PAGE = "/dashboard";
		ALLOW_PAGES.add("/task");
		ALLOW_PAGES.add("/logout");
		ALLOW_PAGES.add("/project");
		ALLOW_PAGES.add("/dashboard");
	}
	
	public ManagerRole(){
		super(ALLOW_PAGES,HOME_PAGE);
//		super.homePage = HOME_PAGE;
//		super.allowPages = ALLOW_PAGES;
	}
	
	
	public boolean getAllowTaskCreate() {
		// TODO Auto-generated method stub
		return true;
	}
	
	public boolean getShowDashboardProject(){
		return true;
	}
}
