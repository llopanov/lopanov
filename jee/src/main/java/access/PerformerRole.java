package access;

import java.util.ArrayList;
import java.util.List;

public class PerformerRole extends AbstractRole {//implements IRole{//
	private final static List<String > ALLOW_PAGES = new ArrayList<String>();
	private final static String HOME_PAGE ;
	
	static {
		HOME_PAGE = "/dashboard";
		ALLOW_PAGES.add("/task");
		ALLOW_PAGES.add("/logout");
		ALLOW_PAGES.add("/dashboard");
		ALLOW_PAGES.add("/project/list");
		ALLOW_PAGES.add("/project");
	}
	
	public PerformerRole(){
		super(ALLOW_PAGES,HOME_PAGE);
//		super.homePage = HOME_PAGE;
//		super.allowPages = ALLOW_PAGES;
	}
	

	public boolean getShowDashboardProject(){
		return true;
	}
	
}
