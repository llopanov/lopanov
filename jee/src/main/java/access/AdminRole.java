package access;

import java.util.ArrayList;
import java.util.List;

public class AdminRole extends AbstractRole { //implements IRole{//

	private final static List<String > ALLOW_PAGES = new ArrayList<String>();
	private final static String HOME_PAGE ;
	
	static {
		HOME_PAGE = "/admin";
		ALLOW_PAGES.add("/admin");
		ALLOW_PAGES.add("/logout");
	}
	
	public AdminRole(){
		super(ALLOW_PAGES,HOME_PAGE);
//		super.homePage = HOME_PAGE;
//		super.allowPages = ALLOW_PAGES;
	}
	
	
}
