package access;

public interface IRole {
	boolean hasAllow(String pageName);
	String getHomePage();
	boolean getAllowTaskCreate();
	boolean getShowDashboardProject();
}
