package access;

import bean.Position;

public class FactoryRoles {
	public static IRole getRole(Integer roleId){
		if (roleId == null){
			return new GuestRole();
		}
		
		if (roleId == Position.Positions.ADMIN.getId()){
			return new AdminRole();
		}
		
		if (roleId == Position.Positions.TEAM_LEAD.getId() ||
			roleId == Position.Positions.PROJECT_MANAGER.getId()	){
			return new ManagerRole();
		}
		
		if (roleId == Position.Positions.JUNIOR_SOFTWARE_ENGINEER.getId() ||
			roleId == Position.Positions.QA_ENGINEER.getId() ||
			roleId == Position.Positions.SENIOR_SOFTWARE_ENGINEER.getId()	||
			roleId == Position.Positions.SOFTWARE_ENGINEER.getId()	){
			return new PerformerRole();
			}
		
		throw new IllegalArgumentException();
	}
}
