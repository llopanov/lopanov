package access;

import java.util.List;

public abstract class AbstractRole implements IRole{
	protected List<String > allowPages;
	protected String homePage ;
	
	protected AbstractRole(List<String > allowPages, String homePage){
		this.allowPages = allowPages;
		this.homePage = homePage;
	}

	public boolean hasAllow(String pageName) {
		// TODO Auto-generated method stub
		for (String page : allowPages){
			

			if (pageName.startsWith(page)){
				return true;
			}
		}
		return false;//ALLOW_PAGES.contains(pageName);
	}

	public String getHomePage() {
		// TODO Auto-generated method stub
		return homePage;
	}
	
	public boolean getAllowTaskCreate() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean getShowDashboardProject(){
		return false;
	}

}
