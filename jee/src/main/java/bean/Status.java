package bean;


public class Status {
	public enum DbNames{
		ID			("Id"),
		NAME		("Name");
		
		private final String colName;
		DbNames(String name){
			colName = name;
		}
		public String getColName(){
			return colName;
		}
	}
	
	public enum Statuses{
		OPEN(1)			,
		IN_PROGRESS(2)		,
		RESOLVE(3) ,
		CLOSED(4)		;
		
		private final int id;
		Statuses(int id){
			this.id = id;
		}
		public int getId(){
			return id;
		}
		

	};

	private int id;
	private String name;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	
	
}
