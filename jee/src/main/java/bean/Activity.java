package bean;

import java.util.Date;

import utils.MyUtils;

public class Activity {

	
	public enum DbNames{
		ID				("Id", 0),
		DATE			("Date", 1),
		DURATION		("Duration" , 2),
		COMMENT			("Comment" , 3),
		MEMBER_ID		("MemberId" , 4),
		ASSIGMENT_ID	("AssigmentId" , 5);
		
		
		private final String colName;
		private final int colNum;
		DbNames(String name, int num){
			colName = name;
			colNum = num;
		}
		public String getColName(){
			return colName;
		}
		
		public int getColNum(){
			return colNum;
		}
	}

	private int id;
	private Date date;
	private int duration;
	private String comment;
	private int memberId;
	private int assigmentId;
	
	private int taskId;
	private Employee employee;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public String getDateView() {
		return MyUtils.getDateString(date);
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public int getAssigmentId() {
		return assigmentId;
	}
	public void setAssigmentId(int assigmentId) {
		this.assigmentId = assigmentId;
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
}
