package bean;

public class Role {
	public enum DbNames{
		ID			("Id"),
		NAME		("Name");
		
		private final String colName;
		DbNames(String name){
			colName = name;
		}
		public String getColName(){
			return colName;
		}
	}
	public enum Roles{
		DEVELOPER(1)			,
		KEY_DEVELOPER(2)		,
		TEAM_LEAD(3) ,
		PROJECT_MANAGER(4)		,
		QA(5);
		
		private final int id;
		Roles(int id){
			this.id = id;
		}
		public int getId(){
			return id;
		}
		

	};
	
	
	private int id;
	private String name;
	
	public Role() {
		super();
	}
	
	public Role(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	
	static public String getManagerRoles(){
		//List<Integer> managerRoles = new ArrayList<Integer>();
		//managerRoles.add(Roles.TEAM_LEAD.getId());
		//managerRoles.add(Roles.PROJECT_MANAGER.getId());
		//return managerRoles;
		
		return Integer.toString(Roles.TEAM_LEAD.getId())+","+Integer.toString(Roles.PROJECT_MANAGER.getId());
	}
}
