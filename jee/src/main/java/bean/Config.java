package bean;

public class Config {
	public final static String TEMPLATE_PATH = "/WEB-INF/";
	public final static String TEMPLATE_ERROR_AJAX = TEMPLATE_PATH + "error.jsp";
	public final static String TEMPLATE_MAIN = TEMPLATE_PATH + "main_template.jsp";

	public final static String PATH_TO_TMP_UPLOAD_FILES = "/tmp";
	public final static String PATH_TO_UPLOAD_FILES = "/home/antar/jeeUpload";
	
	public final static int COUNT_ACTIVITIES_PER_PAGE = 3;
	public final static int COUNT_TASK_PER_PAGE = 3;
	
	public final static String DATE_FORMAT_BASE = "yyyy-MM-dd";
	public final static String DATE_FORMAT_BASE_WITH_TIME = "yyyy-MM-dd HH:mm:ss";
	public final static String DATE_VIEW_STRING_FORMAT = "dd-MM-yyyy";
	
	public static final int MIN_LENGTH_LOGIN_PASS = 3;
	public static final int MIN_LENGTH_PASS = 3;
	public static final int MIN_LENGTH_NAME = 2;
	public static final int MIN_LENGTH_DESCRIPTION = 3;
	
	
	
	public final static String DRIVER_NAME = "org.gjt.mm.mysql.Driver";
	public final static String DB_URL = "jdbc:mysql://localhost/bts";
	public final static String DB_USER = "root";
	public final static String DB_PASS = "1234";
}
