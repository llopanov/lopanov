package bean;


public class Employee {

	
	public enum DbNames{
		ID			("Id", 0),
		FIRST_NAME	("FirstName", 1),
		LAST_NAME	("LastName" , 2),
		LOGIN		("Login" , 3),
		PASSWORD	("Password" , 4),
		POSITION_ID	("PositionId" , 5);
		
		private final String colName;
		private final int colNum;
		DbNames(String name, int num){
			colName = name;
			colNum = num;
		}
		public String getColName(){
			return colName;
		}
		
		public int getColNum(){
			return colNum;
		}
	}

	private int id;
	private String firstName;
	private String lastName;
	private String login;
	private String password;
	//private int positionId;
	private Position position;

	public Employee() {
		super();
	}
	
	public Employee(int id, String firstName, String lastName, String login, String password, Position position) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.position = position;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
