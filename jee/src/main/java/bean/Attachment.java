package bean;


public class Attachment {

	
	public enum DbNames{
		ID			("Id", 0),
		NAME		("Name", 1),
		SIZE		("Size" , 2),
		DESCRIPTION	("Description" , 3),
		TASK_ID		("TaskId" , 4);
		
		
		private final String colName;
		private final int colNum;
		DbNames(String name, int num){
			colName = name;
			colNum = num;
		}
		public String getColName(){
			return colName;
		}
		
		public int getColNum(){
			return colNum;
		}
	}

	private int id;
	private String name;
	private long size;
	private String description;
	private int taskId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}


	
	
}
