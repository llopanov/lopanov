package bean;


public class Position {
	public enum DbNames{
		ID			("Id"),
		NAME		("Name");
		
		private final String colName;
		DbNames(String name){
			colName = name;
		}
		public String getColName(){
			return colName;
		}
	};

	public enum Positions{
		ADMIN(1)			,
		QA_ENGINEER(2)		,
		PROJECT_MANAGER(3) ,
		TEAM_LEAD(4)		,
		SENIOR_SOFTWARE_ENGINEER(5),
		SOFTWARE_ENGINEER(6),
		JUNIOR_SOFTWARE_ENGINEER(7);
		
		private final int id;
		Positions(int id){
			this.id = id;
		}
		public int getId(){
			return id;
		}
	};
	
	private int id;
	private String name;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	
	
}
