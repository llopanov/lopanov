package bean;

import java.util.HashMap;
import java.util.Map;

public class Project {
	public enum DbNames{
		ID			("Id", 0),
		NAME		("Name", 1),
		DESCRIPTION	("Description" , 2);
		
		private final String colName;
		private final int colNum;
		DbNames(String name, int num){
			colName = name;
			colNum = num;
		}
		public String getColName(){
			return colName;
		}
		
		public int getColNum(){
			return colNum;
		}
	}
	
	
	
	private int id;
	
	
	private String name;
	private String description;
	
	private Map <Employee, Role>  members = new HashMap<Employee, Role>();  
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void addMember(Employee emp, Role role){
		members.put(emp, role);
	}
	public Map<Employee, Role> getMembers(){
		return members;
	}
		
}
