package bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import utils.MyUtils;

public class Task {
	public enum DbNames{
		ID			("Id", 0),
		PROJECT_ID	("ProjectId", 1),
		DESCRIPTION	("Description" , 2),
		PSD			("Psd" , 3),
		PDD			("Pdd" , 4),
		ASD			("Asd" , 5),
		ADD			("Add" , 6),
		STATUS_ID	("StatusId" , 7);
		
		private final String colName;
		private final int colNum;
		DbNames(String name, int num){
			colName = name;
			colNum = num;
		}
		public String getColName(){
			return colName;
		}
		
		public int getColNum(){
			return colNum;
		}
	}
	
	private int id;
	private int projectId;
	private String description;
	private Date psd;
	private Date pdd;
	private Date asd;
	private Date add;
	private Status status;
	
	Employee assignedEmployee;
	List <Attachment> attachments = new ArrayList<Attachment>();
	List <FileItem> files = new ArrayList<FileItem>();;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getPsd() {
		return psd;
	}
	public String getPsdView() {
		return MyUtils.getDateString(psd);
	}
	public void setPsd(Date psd) {
		this.psd = psd;
	}
	public Date getPdd() {
		return pdd;
	}
	public String getPddView() {
		return MyUtils.getDateString(pdd);
	}
	public void setPdd(Date pdd) {
		this.pdd = pdd;
	}
	public Date getAsd() {
		return asd;
	}
	public String getAsdView() {
		return MyUtils.getDateString(asd);
	}
	public void setAsd(Date asd) {
		this.asd = asd;
	}
	public Date getAdd() {
		return add;
	}
	public String getAddView() {
		return MyUtils.getDateString(add);
	}
	public void setAdd(Date add) {
		this.add = add;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Employee getAssignedEmployee() {
		return assignedEmployee;
	}
	public void setAssignedEmployee(Employee assignedEmployee) {
		this.assignedEmployee = assignedEmployee;
	}
	
	public void addAttachment(Attachment attachment) {
		attachments.add(attachment);
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}
	
	public void addFiles(List<FileItem> files) {
		this.files = files;
	}
	
	public List<FileItem> getFiles() {
		return files;
	}
}
