package bean;

public class Constants {
	
	
	public static final String ERROR_USER_WITH_PASS_ARE_WRONG 	= "user with current password are wrong, or user doesn't exist";
	public static final String ERROR_EMPLOYEE_NOT_EXIST = "Employee not exist";
	public static final String ERROR_PROJECT_NOT_EXIST = "Project not exist";
	public static final String ERROR_PSITION_NOT_EXIST = "Position not exist";
	public static final String ERROR_EMPLOYEE_ALLREADY_EXIST = "Employee with current login allready exist";
	public static final String ERROR_EMPLOYEE_NOT_MEMBER_PROJECT = "Employee not member project";
	public static final String TASK_DOESNT_EXIST_OR_YOU_NOT_ASSIGNED_TO_IT= "Task doesn't exist or you not assigned to it";
	public static final String ERROR_ASSIGNEE_ON_TASK = "task with assigment not exist";
	
	public static final String ERROR_PARSE_DATE = "error parse date";
	
	public static final String PAR_NAME_STATUS   	= "status";
	public static final String PAR_NAME_REDIRECT   	= "redirect";
	public static final String PAR_NAME_STATUS_OK 	= "ok";
	public static final String PAR_NAME_STATUS_ERROR= "error";
	public static final String PAR_NAME_PAGENAME    = "pagename";
	public static final String PAR_NAME_MESSAGE   	= "message";
	public static final String PAR_NAME_EMPLOYEES  	= "employees";
	
	
	
	public static final String CONTENT_TYPE_JSON   	= "application/json";
	public static final String ENCODING_UTF8   	= "UTF-8";
	
	
	public static final String ERROR_MSG = "errorMessage";
	
	public static final String SESSTION_ATTR_NAME_EMPLOYEE = "Employee";
	public static final String SESSTION_ATTR_NAME_ROLE = "Role";
	/*
	// pages
	public static final String INDEX_CONTROLLER 		= "index";
	public static final String LOGIN_CONTROLLER 		= "login";
	public static final String LOGOUT_CONTROLLER 		= "logout";
	public static final String REGISTRATION_CONTROLLER	= "registration";
	public static final String TASK_LIST_CONTROLLER 	= "tasklist";
	public static final String ADD_TASK_CONTROLLER 	    = "addtask";
	
	public static final String TODAY_TAB 	    	= "today";
	public static final String TOMORROW_TAB 	    = "tomorrow";
	public static final String SOMEDAY_TAB 	    	= "someday";
	// errors
	public static final String ERROR_VALIDATE_LOGIN_FORM_NOT_EMPTY 	= "login and password must be not empty";
	
	public static final String ERROR_VALIDATE_LOGIN_FORM_PASS_NOT_MATCH 	= "password not match";
	public static final String ERROR_VALIDATE_TASK_NAME 	= "error in task name, may be very short or empty?";
	public static final String ERROR_VALIDATE_EMAIL 	= "error in email";
	public static final String ERROR_VALIDATE_DATE 	= "error in date format, must be like 31.12.2002";
	public static final String ERROR_VALIDATE_VERY_SHORT_TASK_NAME 	= "Very short task name, must be longer than 2 charachters";
	public static final String ERROR_VALIDATE_TASKID 	= "error in task id";
	public static final String ERROR_VALIDATE_CHECKEDID 	= "error in checked id";
	public static final String ERROR_VALIDATE_OPERATION 	= "error in operation value";
	public static final String ERROR_CHECKEDLIST_EMPTY 	= "no checked items";
	public static final String ERROR_CONTENT_TYPE_NOT_MULTIPART_FORM_DATA 	= "Content type is not multipart/form-data";
	public static final String ERROR_FILE_NOT_EXIST_ON_SERVER 	= "File doesn't exists on server.";
	
	public static final String ERROR_USER_WITH_PASS_ARE_WRONG 	= "user with current password are wrong, or user doesn't exist";
	public static final String ERROR_USER_ALLREADY_EXIST 		= "user with same login allready exist, try other login";
	
	
	public static final String ERROR_NOT_OWNER_TASK = "Its not your task";
	public static final String ERROR_TASK_NOT_EXIST = "Task not exist";
	
	
	public static final String ATTR_NAME_TASKS 	    	= "tasks";
	public static final String ATTR_NAME_SHOW_FILE_OPERATIONS = "showfileoperations";
	public static final String ATTR_NAME_DATE_TODAY 	= "datetoday";
	public static final String ATTR_NAME_DATE_TOMORROW  = "datetomorrow";

	public static final String PAR_NAME_LOGIN    	= "login";
	public static final String PAR_NAME_PASSWORD   	= "password";
	public static final String PAR_NAME_REPASSWORD  = "repassword";
	public static final String PAR_NAME_EMAIL   	= "email";
	public static final String PAR_NAME_TASKNAME   	= "taskname";
	public static final String PAR_NAME_TASKDATE   	= "taskdate";
	public static final String PAR_NAME_CHECKED_TASKS 	= "checkedTasks";
	public static final String PAR_NAME_TASKID 		= "taskId";
	public static final String PAR_NAME_OPERATION 		= "operation";
	

	
	
	public static final String PAR_NAME_TABNAME   	= "tabname";
	
	

	
	*/
	
}
