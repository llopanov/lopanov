package DAOInterfaces;

import java.util.List;

import bean.Activity;

public interface IActivityDAO {
	
	public List<Activity> getActivitiesForTask(int taskId);
	public List<Activity> getAllActivities(int limit, int offset);
	public int getCountAllRows();
	

}

