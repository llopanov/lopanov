package DAOInterfaces;

import java.util.List;

import bean.Position;



public interface IPositionDAO {
	public Position getPositionById(int id);
	public List<Position> getPositions();
}
