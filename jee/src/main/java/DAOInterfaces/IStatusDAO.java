package DAOInterfaces;

import java.util.List;

import bean.Status;



public interface IStatusDAO {
	public Status getStatusById(int id);
	public List<Status> getStatuses();
}
