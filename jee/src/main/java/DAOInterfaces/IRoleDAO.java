package DAOInterfaces;

import java.util.List;

import bean.Role;



public interface IRoleDAO {
	public Role getRoleById(int id);
	public List<Role> getRoles();
}
