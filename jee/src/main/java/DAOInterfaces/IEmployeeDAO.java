package DAOInterfaces;

import java.util.List;

import bean.Employee;



public interface IEmployeeDAO {
	public Employee getEmployee(String login, String password);
	public Employee getEmployeeById(int id);
	public void addEmployee(Employee emp);
	public void updateEmployee(Employee emp);
	public void deleteEmployee(int id);
	public List<Employee> getEmployees();
	public List<Employee> getEmployeesFromProject(int projectId);
}
