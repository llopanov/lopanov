package DAOInterfaces;

import java.util.List;
import bean.Project;



public interface IProjectsDAO {
	public List<Project> getProjects();
	public Project getProjectById(int id);
	public void addProject(Project project);
	public void updateProject(Project project);
	public void deleteProject(int id);
	public List<Project> getProjectsWhereMemberManager(int id);
	public List<Project> getProjectsWhereMember(int id);
	
}
