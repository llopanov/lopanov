package DAOInterfaces;

import java.util.List;

import bean.Activity;
import bean.Task;



public interface ITaskDAO{
	//public Employee getEmployee(String login, String password);
	public Task getTaskById(int id);
	public int addTask(Task task);
	//public void updateEmployee(Employee emp);
	//public void deleteEmployee(int id);
	
	public List<Task> getTasks(int employeeId, int pageId, int tasksCount);
	//public List<Employee> getEmployeesFromProject(int projectId);
	public void addActivity(Activity activity);
	
	//public Attachment getTaskAttachment(int taskId, int fileId); 
	
	public void setAssigneeOnTask(int taskId, int employeeId);
	public void setStatusOnTask(int taskId, int statusId);
	
	public boolean isEmployeeInTaskPojectTeam(int taskId, int employeeId);

	
	public int getCountAllRows();
}

