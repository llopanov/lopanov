package exceptions;

import static logger.LoggerClass.LOGGER;

public class DaoException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DaoException(String mes){
		super(mes);
		LOGGER.error(mes);
	}
}
