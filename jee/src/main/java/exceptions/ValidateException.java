package exceptions;

public class ValidateException extends RuntimeException{ // extends Exception{ //
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidateException(String mes){
		super(mes);
	}
	
	//Map<String, String> errors;
//	public ValidateException(Map<String, String> errors){
//		this.errors = errors;
//	}
}
