package factories;

import DAOImpl.DbActivityImpl;
import DAOImpl.DbEmployeeImpl;
import DAOImpl.DbPositionImpl;
import DAOImpl.DbProjectImpl;
import DAOImpl.DbRoleImpl;
import DAOImpl.DbStatusImpl;
import DAOImpl.DbTaskImpl;
import DAOInterfaces.IActivityDAO;
import DAOInterfaces.IEmployeeDAO;
import DAOInterfaces.IPositionDAO;
import DAOInterfaces.IProjectsDAO;
import DAOInterfaces.IRoleDAO;
import DAOInterfaces.IStatusDAO;
import DAOInterfaces.ITaskDAO;

public class modelFactory {
	
		public static IEmployeeDAO getEmployeeModel() {
			return new DbEmployeeImpl();
		}
		
		public static IPositionDAO getPositionModel() {
			return new DbPositionImpl();
		}
		
		public static IRoleDAO getRoleModel() {
			return new DbRoleImpl();
		}

		public static IProjectsDAO getProjectModel() {
			return new DbProjectImpl();
		}
		
		public static IStatusDAO getStasusModel() {
			return new DbStatusImpl();
		}
		
		public static ITaskDAO getTaskModel() {
			return new DbTaskImpl();
		}
		
		public static IActivityDAO getActivityModel() {
			return new DbActivityImpl();
		}
}
