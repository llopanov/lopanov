package DAOImpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import exceptions.ConnectionException;
import exceptions.DaoException;

import utils.DBConnectionManager;

import bean.Constants;
import bean.Employee;
import bean.Position;
import DAOInterfaces.IEmployeeDAO;

public class DbEmployeeImpl implements IEmployeeDAO { // extends AbstractBaseDB

	public Employee getEmployee(String login, String password) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		Employee emp = null;
		String query = String.format("SELECT Employee.*, Position.%s FROM Employee LEFT JOIN Position ON Position.Id = Employee.PositionId WHERE login=\"%s\" AND password=\"%s\" LIMIT 1",Position.DbNames.NAME.getColName(), login,
				password);

		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			if (resultSet.next()) {

				emp = new Employee();
				emp.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.ID.getColName())));
				emp.setFirstName(resultSet.getString(Employee.DbNames.FIRST_NAME.getColName()));
				emp.setLastName(resultSet.getString(Employee.DbNames.LAST_NAME.getColName()));
				emp.setLogin(resultSet.getString(Employee.DbNames.LOGIN.getColName()));
				Position position = new Position();
				position.setName(resultSet.getString(Position.DbNames.NAME.getColName()));
				position.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.POSITION_ID.getColName())));
				
				emp.setPosition(position);

				return emp;
			} else {
				// no users
				throw new DaoException(Constants.ERROR_USER_WITH_PASS_ARE_WRONG);
			}

		} catch (ConnectionException e) {
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
			throw new DaoException("Query: " + query + "\n" + e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}

	}

	public void addEmployee(Employee employee) {
		// TODO Auto-generated method stub

		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		PreparedStatement psInsertUser = null;

		String query = String.format("SELECT * FROM Employee WHERE Login=\"%s\" LIMIT 1;", employee.getLogin());

		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();

			psInsertUser = connection.prepareStatement(
					"INSERT INTO Employee (`Id`, `FirstName`, `LastName`, `Login`,`Password`, `PositionId`) VALUES (NULL, ?, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);

			psInsertUser.setString(Employee.DbNames.FIRST_NAME.getColNum(), employee.getFirstName());
			psInsertUser.setString(Employee.DbNames.LAST_NAME.getColNum(), employee.getLastName());
			psInsertUser.setString(Employee.DbNames.LOGIN.getColNum(), employee.getLogin());
			psInsertUser.setString(Employee.DbNames.PASSWORD.getColNum(), employee.getPassword());
			psInsertUser.setInt(Employee.DbNames.POSITION_ID.getColNum(), employee.getPosition().getId());

			synchronized (DbEmployeeImpl.class) {
				resultSet = stmt.executeQuery(query);

				if (resultSet.next()) {
					throw new DaoException(Constants.ERROR_EMPLOYEE_ALLREADY_EXIST);
				}

				// try {
				// Thread.sleep(5000);
				// } catch (InterruptedException ex) {
				// System.err.println("main::Interrupted: " + ex.getMessage());
				// }

				psInsertUser.executeUpdate();

			}
			psInsertUser.close();
		} catch (SQLException e) {

			throw new DaoException("Query: " + e.getMessage());
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}

	}

	public List<Employee> getEmployees() {
		// TODO Auto-generated method stub
		List<Employee> employeeList = new ArrayList<Employee>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Employee.*, Position.%s FROM Employee LEFT JOIN Position ON Position.Id = Employee.PositionId", Position.DbNames.NAME.getColName());
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()) {
				Employee employee = new Employee();
				employee.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.ID.getColName())));
				employee.setFirstName(resultSet.getString(Employee.DbNames.FIRST_NAME.getColName()));
				employee.setLastName(resultSet.getString(Employee.DbNames.LAST_NAME.getColName()));
				employee.setLogin(resultSet.getString(Employee.DbNames.LOGIN.getColName()));
				
				Position position = new Position();
				position.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.POSITION_ID.getColName())));
				position.setName(resultSet.getString(Position.DbNames.NAME.getColName()));
				employee.setPosition(position);

				employeeList.add(employee);
			}
			return employeeList;

		} catch (ConnectionException e) {
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
			throw new DaoException("Query: " + query + "\n" + e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public Employee getEmployeeById(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Employee.*, Position.%s FROM Employee LEFT JOIN Position ON Position.Id = Employee.PositionId WHERE Employee.Id=\"%d\" LIMIT 1", Position.DbNames.NAME.getColName(), id);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);

			if (!resultSet.next()) {
				throw new DaoException(Constants.ERROR_EMPLOYEE_NOT_EXIST);
			}

			Employee employee = new Employee();
			employee.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.ID.getColName())));
			employee.setFirstName(resultSet.getString(Employee.DbNames.FIRST_NAME.getColName()));
			employee.setLastName(resultSet.getString(Employee.DbNames.LAST_NAME.getColName()));
			employee.setLogin(resultSet.getString(Employee.DbNames.LOGIN.getColName()));
			
			Position position = new Position();
			position.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.POSITION_ID.getColName())));
			position.setName(resultSet.getString(Position.DbNames.NAME.getColName()));
			employee.setPosition(position);

			// }
			return employee;

		} catch (ConnectionException e) {
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
			throw new DaoException("Query: " + query + "\n" + e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		PreparedStatement psUdpateEmployee = null;

		String query = String.format("SELECT * FROM Employee WHERE Login=\"%s\" AND Id != \"%d\" LIMIT 1;",
				employee.getLogin(), employee.getId());

		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			boolean isNeedChangePassword = !employee.getPassword().isEmpty();

			if (!isNeedChangePassword) {
				psUdpateEmployee = connection.prepareStatement(
						"UPDATE Employee SET `FirstName` = ?, `LastName` = ?, `Login` = ?, `PositionId` = ? WHERE Id = ?",
						Statement.RETURN_GENERATED_KEYS);
			} else {
				psUdpateEmployee = connection.prepareStatement(
						"UPDATE Employee SET `FirstName` = ?, `LastName` = ?, `Login` = ?, `Password` = ?, `PositionId` = ? WHERE Id = ?",
						Statement.RETURN_GENERATED_KEYS);
			}
			int index = 1;
			
			psUdpateEmployee.setString(index++, employee.getFirstName());
			psUdpateEmployee.setString(index++, employee.getLastName());
			psUdpateEmployee.setString(index++, employee.getLogin());
			if (isNeedChangePassword) {
				psUdpateEmployee.setString(index++, employee.getPassword());
			}
			psUdpateEmployee.setInt(index++, employee.getPosition().getId());
			psUdpateEmployee.setInt(index++, employee.getId());

			synchronized (DbEmployeeImpl.class) {
				resultSet = stmt.executeQuery(query);

				if (resultSet.next()) {
					throw new DaoException(Constants.ERROR_EMPLOYEE_ALLREADY_EXIST);
				}

				// try {
				// Thread.sleep(5000);
				// } catch (InterruptedException ex) {
				// System.err.println("main::Interrupted: " + ex.getMessage());
				// }

				psUdpateEmployee.executeUpdate();

			}
			psUdpateEmployee.close();
		} catch (SQLException e) {

			throw new DaoException("Query: " + e.getMessage());
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public void deleteEmployee(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("DELETE FROM Employee WHERE Id=\"%s\" LIMIT 1;", id);

		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();

			stmt.executeUpdate(query);
			
		} catch (SQLException e) {

			throw new DaoException("Query: " + e.getMessage());
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public List<Employee> getEmployeesFromProject(int projectId) {
		// TODO Auto-generated method stub
		List<Employee> employeeList = new ArrayList<Employee>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Employee.*, Position.%s FROM Employee LEFT JOIN Member ON Employee.Id = Member.EmployeeId LEFT JOIN Position ON Position.Id = Employee.PositionId WHERE Member.ProjectId = %d",Position.DbNames.NAME.getColName(), projectId);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()) {
				Employee employee = new Employee();
				employee.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.ID.getColName())));
				employee.setFirstName(resultSet.getString(Employee.DbNames.FIRST_NAME.getColName()));
				employee.setLastName(resultSet.getString(Employee.DbNames.LAST_NAME.getColName()));
				employee.setLogin(resultSet.getString(Employee.DbNames.LOGIN.getColName()));
				
				Position position = new Position();
				position.setId(Integer.parseInt(resultSet.getString(Employee.DbNames.POSITION_ID.getColName())));
				position.setName(resultSet.getString(Position.DbNames.NAME.getColName()));
				employee.setPosition(position);
				

				employeeList.add(employee);
			}
			return employeeList;

		} catch (ConnectionException e) {
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
			throw new DaoException("Query: " + query + "\n" + e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

}
