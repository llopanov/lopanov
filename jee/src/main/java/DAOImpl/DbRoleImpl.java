package DAOImpl;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import exceptions.ConnectionException;
import exceptions.DaoException;

import utils.DBConnectionManager;

import bean.Constants;
import bean.Role;
import DAOInterfaces.IRoleDAO;


public class DbRoleImpl implements IRoleDAO { //extends AbstractBaseDB 

	public Role getRoleById(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT * FROM Role WHERE Id=\"%d\" LIMIT 1", id);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);

			if (!resultSet.next()){
				throw new DaoException(Constants.ERROR_PSITION_NOT_EXIST);
			}
			
			Role position = new Role();
			position.setId(	Integer.parseInt(resultSet.getString(Role.DbNames.ID.getColName())));
			position.setName(resultSet.getString(Role.DbNames.NAME.getColName()));

			return position;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}


	public List<Role> getRoles() {
		// TODO Auto-generated method stub
		List<Role> positionsList = new ArrayList<Role>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT * FROM Role");
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Role position = new Role();
				position.setId(	Integer.parseInt(resultSet.getString(Role.DbNames.ID.getColName())));
				position.setName(resultSet.getString(Role.DbNames.NAME.getColName()));
				
				
				positionsList.add(position);
			} 
			return positionsList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

}
