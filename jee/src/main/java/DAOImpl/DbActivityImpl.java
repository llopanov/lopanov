package DAOImpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DAOInterfaces.IActivityDAO;
import bean.Activity;
import bean.Employee;
import exceptions.ConnectionException;
import exceptions.DaoException;
import utils.DBConnectionManager;

public class DbActivityImpl implements IActivityDAO {
	private int countAllRecords = 0;
	
	
	public List<Activity> getActivitiesForTask(int taskId) {
		List<Activity> activityList = new ArrayList<Activity>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		
//		StringBuilder sb = new StringBuilder();
//		sb.append("SELECT Activity.* FROM Activity ");
//		sb.append("LEFT JOIN Assigment ON Activity.AssigmentId = Assigment.Id ");
//		sb.append(String.format("WHERE Assigment.TaskId = %d ORDER BY Date DESC",taskId));
//		String query = sb.toString();
		
		String query = String.format("SELECT Activity.* FROM Activity LEFT JOIN Assigment ON Activity.AssigmentId = Assigment.Id WHERE Assigment.TaskId = %d ORDER BY Date DESC",taskId);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Activity activity = new Activity();
				activity.setId(resultSet.getInt(Activity.DbNames.ID.getColName()));
				activity.setDate(resultSet.getDate(Activity.DbNames.DATE.getColName()));
				activity.setComment(resultSet.getString(Activity.DbNames.COMMENT.getColName()));
				activity.setDuration(resultSet.getInt(Activity.DbNames.DURATION.getColName()));
				activity.setAssigmentId(resultSet.getInt(Activity.DbNames.ASSIGMENT_ID.getColName()));
				activity.setMemberId(resultSet.getInt(Activity.DbNames.MEMBER_ID.getColName()));
				activity.setTaskId(taskId);
				
				activityList.add(activity);
			} 
			return activityList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public List<Activity> getAllActivities(int limit, int offset) {
		// TODO Auto-generated method stub
		List<Activity> activityList = new ArrayList<Activity>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		
		final String EMPLOYEE_ID_NAME = Employee.DbNames.ID.getColName();  
		final String EMPLOYEE_FIRST_NAME = Employee.DbNames.FIRST_NAME.getColName();
		final String EMPLOYEE_LAST_NAME = Employee.DbNames.LAST_NAME.getColName();

		String query = String.format("SELECT SQL_CALC_FOUND_ROWS Activity.*, Employee.Id AS %s, Employee.FirstName AS %s, Employee.LastName AS %s FROM Activity LEFT JOIN Member ON Member.Id = Activity.MemberId LEFT JOIN Employee ON Employee.Id = Member.EmployeeId ORDER BY Date DESC LIMIT %d, %d", EMPLOYEE_ID_NAME, EMPLOYEE_FIRST_NAME, EMPLOYEE_LAST_NAME, offset, limit);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Activity activity = new Activity();
				activity.setId(resultSet.getInt(Activity.DbNames.ID.getColName()));
				activity.setDate(resultSet.getDate(Activity.DbNames.DATE.getColName()));
				activity.setComment(resultSet.getString(Activity.DbNames.COMMENT.getColName()));
				activity.setDuration(resultSet.getInt(Activity.DbNames.DURATION.getColName()));
				activity.setAssigmentId(resultSet.getInt(Activity.DbNames.ASSIGMENT_ID.getColName()));
				activity.setMemberId(resultSet.getInt(Activity.DbNames.MEMBER_ID.getColName()));
				
				Employee employee = new Employee();
				employee.setId(resultSet.getInt(EMPLOYEE_ID_NAME));
				employee.setFirstName(resultSet.getString(EMPLOYEE_FIRST_NAME));
				employee.setLastName(resultSet.getString(EMPLOYEE_LAST_NAME));

				activity.setEmployee(employee);
				activityList.add(activity);
			} 
			
			resultSet.close();
			
			resultSet = stmt.executeQuery("SELECT FOUND_ROWS()");
			if(resultSet.next()){
                this.countAllRecords = resultSet.getInt(1);
			}
			else {
				throw new DaoException("error sql query, not return count rows: " + query);
			}
			
			return activityList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}
	
	public int getCountAllRows(){
		return countAllRecords;
	}

}
