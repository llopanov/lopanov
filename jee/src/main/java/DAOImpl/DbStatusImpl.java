package DAOImpl;


import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import exceptions.ConnectionException;
import exceptions.DaoException;

import utils.DBConnectionManager;

import bean.Constants;
import bean.Status;
import DAOInterfaces.IStatusDAO;


public class DbStatusImpl implements IStatusDAO { //extends AbstractBaseDB 

	public Status getStatusById(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT * FROM Status WHERE Id=\"%d\" LIMIT 1", id);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);

			if (!resultSet.next()){
				throw new DaoException(Constants.ERROR_PSITION_NOT_EXIST);
			}
			
			Status position = new Status();
			position.setId(	Integer.parseInt(resultSet.getString(Status.DbNames.ID.getColName())));
			position.setName(resultSet.getString(Status.DbNames.NAME.getColName()));
 
			return position;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}


	public List<Status> getStatuses() {
		// TODO Auto-generated method stub
		List<Status> positionsList = new ArrayList<Status>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT * FROM Status");
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Status position = new Status();
				position.setId(	Integer.parseInt(resultSet.getString(Status.DbNames.ID.getColName())));
				position.setName(resultSet.getString(Status.DbNames.NAME.getColName()));
				
				
				positionsList.add(position);
			} 
			return positionsList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

}
