package DAOImpl;



import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import exceptions.ConnectionException;
import exceptions.DaoException;

import utils.DBConnectionManager;

import bean.Constants;
import bean.Employee;
import bean.Project;
import bean.Role;

import DAOInterfaces.IProjectsDAO;


public class DbProjectImpl implements IProjectsDAO {

	public List<Project> getProjects() {
		// TODO Auto-generated method stub
		List<Project> projectList = new ArrayList<Project>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT * FROM Project");
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Project project = new Project();
				project.setId(Integer.parseInt(resultSet.getString(Project.DbNames.ID.getColName())));
				project.setName(resultSet.getString(Project.DbNames.NAME.getColName()));
				project.setDescription(resultSet.getString(Project.DbNames.DESCRIPTION.getColName()));
				
				projectList.add(project);
			} 
			return projectList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public Project getProjectById(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		
		final String EMPLOYEE_ID_STR = "emp_"+Employee.DbNames.ID.getColName();
		final String ROLE_ID_STR = "role_"+Role.DbNames.ID.getColName();
		final String ROLE_NAME_STR = "role_"+Role.DbNames.NAME.getColName();
		
		//String query = String.format("SELECT * FROM Project	 WHERE Id=\"%d\" LIMIT 1", id);
		String query = String.format("SELECT p.*, emp.FirstName, emp.LastName, emp.Id AS %s, Role.Name AS %s, Role.Id AS %s FROM `Project` AS p LEFT JOIN `Member` AS m ON p.Id = m.ProjectId LEFT JOIN `Employee` AS emp ON m.EmployeeId = emp.Id LEFT JOIN `Role` ON Role.Id = m.RoleId WHERE p.Id=%d ", EMPLOYEE_ID_STR, ROLE_NAME_STR, ROLE_ID_STR, id);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			if (!resultSet.next()){
				throw new DaoException(Constants.ERROR_PROJECT_NOT_EXIST);
			}
			
			Project project = new Project();
			project.setId(	Integer.parseInt(resultSet.getString(Project.DbNames.ID.getColName())));
			project.setName(resultSet.getString(Project.DbNames.NAME.getColName()));
			project.setDescription(resultSet.getString(Project.DbNames.DESCRIPTION.getColName()));
			
			
			do{
				Employee emp = new Employee();
				emp.setId(resultSet.getInt(EMPLOYEE_ID_STR));
				emp.setFirstName(resultSet.getString(Employee.DbNames.FIRST_NAME.getColName()));
				emp.setLastName(resultSet.getString(Employee.DbNames.LAST_NAME.getColName()));
				
				Role role = new Role();
				role.setId(resultSet.getInt(ROLE_ID_STR));
				role.setName(resultSet.getString(ROLE_NAME_STR));
				project.addMember(emp, role);	
			}while(resultSet.next());
				
				
			return project;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public void addProject(Project project) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		PreparedStatement psInsertProject = null;
		PreparedStatement psInsertMember = null;
		
		
		DBConnectionManager connectionManager = new DBConnectionManager();
		
		try {
			connection = connectionManager.getConnection();
			connection.setAutoCommit(false);
			
			psInsertProject = connection.prepareStatement("INSERT INTO Project (`Id`, `Name`, `Description`) VALUES (NULL, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			psInsertProject.setString(Project.DbNames.NAME.getColNum(), project.getName());
			psInsertProject.setString(Project.DbNames.DESCRIPTION.getColNum(), project.getDescription());
			
			psInsertProject.executeUpdate();
			
			
			ResultSet keys = psInsertProject.getGeneratedKeys();
				
			int projectId;
			if (keys.next()){
				projectId = keys.getInt(1);
			} else {
                throw new SQLException("Creating project failed, no ID obtained.");
            }
			
			psInsertMember = connection.prepareStatement("INSERT INTO Member (`Id`, `ProjectId`, `EmployeeId`, `RoleId`) VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			
			Set <Employee> setKeys = project.getMembers().keySet();
			for (Employee employee :  setKeys){
				int index=1;
				Role role = project.getMembers().get(employee);
				psInsertMember.setInt(index++, projectId);
				psInsertMember.setInt(index++, employee.getId());
				psInsertMember.setInt(index++, role.getId());
				
				psInsertMember.executeUpdate();
			}
			
			connection.commit();
		} catch (SQLException e) {
			try{
			connection.rollback();
			} catch (SQLException ex){
				throw new DaoException("Query: " + ex.getMessage());
			} 
			
			throw new DaoException("Query: " + e.getMessage());
			
		} finally {
			try{
				if (psInsertProject != null){
					psInsertProject.close();
				}
				if (psInsertMember != null){
					psInsertMember.close();
				}
			}catch (SQLException e){
				throw new DaoException("Query: " + e.getMessage());
			}
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public void updateProject(Project project) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		PreparedStatement psUpdateProject = null;
		PreparedStatement psInsertMember = null;
		
		
		DBConnectionManager connectionManager = new DBConnectionManager();
		String query = String.format("DELETE FROM Member WHERE ProjectId=\"%d\" ", project.getId());
		
		try {
			connection = connectionManager.getConnection();
			connection.setAutoCommit(false);
			stmt = connection.createStatement();
			
			stmt.executeUpdate(query);
			
			int index = 1;
			psUpdateProject = connection.prepareStatement("UPDATE Project SET `Name` = ?, `Description` = ? WHERE Id = ?", Statement.RETURN_GENERATED_KEYS);
			psUpdateProject.setString(index++, project.getName());
			psUpdateProject.setString(index++, project.getDescription());
			psUpdateProject.setInt(index++, project.getId());
			psUpdateProject.executeUpdate();
			
			
			psInsertMember = connection.prepareStatement("INSERT INTO Member (`Id`, `ProjectId`, `EmployeeId`, `RoleId`) VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			
			Set <Employee> setKeys = project.getMembers().keySet();
			for (Employee employee :  setKeys){
				index=1;
				Role role = project.getMembers().get(employee);
				psInsertMember.setInt(index++, project.getId());
				psInsertMember.setInt(index++, employee.getId());
				psInsertMember.setInt(index++, role.getId());
				
				psInsertMember.executeUpdate();
			}
			
			connection.commit();
		} catch (SQLException e) {
			try{
			connection.rollback();
			} catch (SQLException ex){
				throw new DaoException("Query: " + ex.getMessage());
			} 
			
			throw new DaoException("Query: " + e.getMessage());
			
		} finally {
			try{
				if (psInsertMember != null){
					psInsertMember.close();
				}
			}catch (SQLException e){
				throw new DaoException("Query: " + e.getMessage());
			}
			connectionManager.closeConnection(connection, stmt, resultSet);
		}

	}

	public void deleteProject(int id) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		
		String query = String.format("DELETE p, m FROM Project p JOIN Member m ON m.ProjectId = p.Id WHERE p.Id = \"%d\";", id);
		
		DBConnectionManager connectionManager = new DBConnectionManager();
		
		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
						
			int rez = stmt.executeUpdate(query);
			if (rez<=0){
				throw new SQLException("Nothing to delete");
			}

		} catch (SQLException e) {
            throw new DaoException("Query: " + e.getMessage());
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}  
	
	public List<Project> getProjectsWhereMemberManager(int id){
		List<Project> projectList = new ArrayList<Project>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Project.* FROM Project LEFT JOIN Member ON Project.Id = Member.ProjectId WHERE EmployeeId = %d AND RoleId IN (%s) ",id,Role.getManagerRoles());
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Project project = new Project();
				project.setId(Integer.parseInt(resultSet.getString(Project.DbNames.ID.getColName())));
				project.setName(resultSet.getString(Project.DbNames.NAME.getColName()));
				project.setDescription(resultSet.getString(Project.DbNames.DESCRIPTION.getColName()));
				
				projectList.add(project);
			} 
			return projectList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public List<Project> getProjectsWhereMember(int id){
		List<Project> projectList = new ArrayList<Project>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Project.* FROM Project LEFT JOIN Member ON Project.Id = Member.ProjectId WHERE EmployeeId = %d ",id);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Project project = new Project();
				project.setId(Integer.parseInt(resultSet.getString(Project.DbNames.ID.getColName())));
				project.setName(resultSet.getString(Project.DbNames.NAME.getColName()));
				project.setDescription(resultSet.getString(Project.DbNames.DESCRIPTION.getColName()));
				
				projectList.add(project);
			} 
			return projectList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

}
