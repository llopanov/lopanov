package DAOImpl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import exceptions.ConnectionException;
import exceptions.DaoException;

import utils.DBConnectionManager;
import utils.MyUtils;
import bean.Activity;
import bean.Attachment;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Role;
import bean.Status;
import bean.Task;
import DAOInterfaces.ITaskDAO;

public class DbTaskImpl implements ITaskDAO {

	private int countAllRecords;
	
	public Task getTaskById(int id) {
		final String STATUS_NAME_STR = "stat_"+Status.DbNames.NAME.getColName();
		final String ROLE_NAME_STR = "role_"+Role.DbNames.NAME.getColName();
		final String EMP_NAME_STR = "emp_"+Employee.DbNames.ID.getColName();
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Task.*, Status.Name AS %s, Role.Name AS %s, Employee.Id AS %s, Employee.FirstName, Employee.LastName FROM Task "
				+ "LEFT JOIN Status ON Task.StatusId = Status.Id "
				+ "LEFT JOIN Assigment ON Task.Id = Assigment.TaskId "
				+ "LEFT JOIN Member ON Assigment.MemberId = Member.Id "
				+ "LEFT JOIN Employee ON Employee.Id = Member.EmployeeId "
				+ "LEFT JOIN Role ON Role.Id = Member.RoleId "
				+ "WHERE Task.Id = %d  LIMIT 1", STATUS_NAME_STR, ROLE_NAME_STR, EMP_NAME_STR, id);
		
		String queryAttachments = String.format("SELECT * FROM Attachment WHERE Attachment.TaskId = %d", id);

		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			if (resultSet.next()) {

				Status status = new Status();
				status.setId(resultSet.getInt(Task.DbNames.STATUS_ID.getColName()));
				status.setName(resultSet.getString(STATUS_NAME_STR));
				
				Employee employee = new Employee();
				employee.setFirstName(resultSet.getString(Employee.DbNames.FIRST_NAME.getColName()));
				employee.setLastName(resultSet.getString(Employee.DbNames.LAST_NAME.getColName()));
				employee.setId(resultSet.getInt(EMP_NAME_STR));
				
				Task task = new Task();
				
				task.setStatus(status);
				task.setAssignedEmployee(employee);
				
				task.setId(resultSet.getInt(Task.DbNames.ID.getColName()));
				task.setProjectId(resultSet.getInt(Task.DbNames.PROJECT_ID.getColName()));
				task.setDescription(resultSet.getString(Task.DbNames.DESCRIPTION.getColName()));
				task.setPsd(resultSet.getDate(Task.DbNames.PSD.getColName()));
				task.setPdd(resultSet.getDate(Task.DbNames.PDD.getColName()));
				task.setAsd(resultSet.getDate(Task.DbNames.ASD.getColName()));
				task.setAdd(resultSet.getDate(Task.DbNames.ADD.getColName()));
				
				resultSet.close();
				resultSet = stmt.executeQuery(queryAttachments);
				
				while (resultSet.next()){

					Attachment attach = new Attachment();
					attach.setDescription(resultSet.getString(Attachment.DbNames.DESCRIPTION.getColName()));
					attach.setId(resultSet.getInt(Attachment.DbNames.ID.getColName()));
					attach.setName(resultSet.getString(Attachment.DbNames.NAME.getColName()));
					attach.setSize(resultSet.getLong(Attachment.DbNames.SIZE.getColName()));
					attach.setTaskId(id);
					
					task.addAttachment(attach);
				}
				
				return task;
			} else {
				// no users
				throw new DaoException(Constants.TASK_DOESNT_EXIST_OR_YOU_NOT_ASSIGNED_TO_IT);
			}

		} catch (ConnectionException e) {
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
			throw new DaoException("Query: " + query + "\n" + e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public int addTask(Task task) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		PreparedStatement psInsertTask = null;
		PreparedStatement psInsertAttachment = null;
		PreparedStatement psInsertAssignee = null;

		String query = String.format("SELECT Member.Id FROM Member WHERE ProjectId=\"%d\" AND EmployeeId=\"%d\" LIMIT 1;", task.getProjectId(), task.getAssignedEmployee().getId());

		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();

			
			int memberId;
			resultSet = stmt.executeQuery(query);
			if (!resultSet.next()) {
				throw new DaoException(Constants.ERROR_EMPLOYEE_NOT_MEMBER_PROJECT);
				
			}
			memberId = resultSet.getInt("Id");
			
			connection.setAutoCommit(false);
			
			psInsertTask = connection.prepareStatement(
					"INSERT INTO Task (`Id`, `ProjectId`, `Description`, `Psd`,`Pdd`,  `StatusId`) VALUES (NULL, ?, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			psInsertTask.setInt(index++, task.getProjectId());
			psInsertTask.setString(index++, task.getDescription());
			psInsertTask.setString(index++, MyUtils.getDateDBString(task.getPsd()));
			psInsertTask.setString(index++, MyUtils.getDateDBString(task.getPdd()));
			psInsertTask.setInt(index++, task.getStatus().getId());
			
			psInsertTask.executeUpdate();
			ResultSet keys = psInsertTask.getGeneratedKeys();
			
			int taskId;
			if (keys.next()){
				taskId = keys.getInt(1);
			} else {
                throw new SQLException("Creating task failed, no ID obtained.");
            }
			
			psInsertAssignee = connection.prepareStatement(
					"INSERT INTO Assigment (`Id`, `MemberId`, `TaskId`) VALUES (NULL, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			index = 1;
			psInsertAssignee.setInt(index++, memberId);
			psInsertAssignee.setInt(index++, taskId);
			
			psInsertAssignee.executeUpdate();
			
			
			psInsertAttachment = connection.prepareStatement(
					"INSERT INTO Attachment (`Id`, `Name`, `Size`, `Description`, `TaskId`) VALUES (NULL, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			
			for (FileItem file: task.getFiles()){
				File uploadedDir = new File(Config.PATH_TO_UPLOAD_FILES);
	            if(!uploadedDir.exists()) {
	          	  uploadedDir.mkdirs();
	            }
        		File uploadedFile = new File(uploadedDir,MyUtils.generateFileName(taskId, file.getName()));

        		try {
					file.write(uploadedFile);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					throw new DaoException("cant remove write file");
				}
        		
        		Attachment attachment = new Attachment();
        		attachment.setName(file.getName());
        		attachment.setDescription(file.getContentType());
        		attachment.setSize(file.getSize());
        		attachment.setTaskId(taskId);
				
				
				index = 1;
				psInsertAttachment.setString(index++, attachment.getName());
				psInsertAttachment.setLong(index++, attachment.getSize());
				psInsertAttachment.setString(index++, attachment.getDescription());
				psInsertAttachment.setInt(index++, taskId);
				
				psInsertAttachment.executeUpdate();
			}
			
			
			connection.commit();
			return taskId;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				throw new DaoException(e1.getMessage());
			}
			throw new DaoException("Query: " + e.getMessage());
		} 

		finally {
			try {
				if (psInsertTask != null){
					psInsertTask.close();
				}
				if (psInsertAssignee != null){
					psInsertAssignee.close();
				}
				if (psInsertAttachment != null){
					psInsertAttachment.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				throw new DaoException(e.getMessage());
			}
			
			
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public List<Task> getTasks(int employeeId, int offset, int taskCount) {
		// TODO Auto-generated method stub
		List<Task> taskList = new ArrayList<Task>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		final String STATUS_NAME_STR = "stat_"+Status.DbNames.NAME.getColName();

		String query = String.format("SELECT SQL_CALC_FOUND_ROWS Task.* , Status.Name AS %s FROM Task LEFT JOIN Assigment ON Task.Id = Assigment.TaskId LEFT JOIN Member ON Assigment.MemberId = Member.Id LEFT JOIN Employee ON Employee.Id = Member.EmployeeId LEFT JOIN Status ON Task.StatusId = Status.Id WHERE Employee.Id = %d ORDER BY Task.PSD DESC LIMIT %d, %d  ",STATUS_NAME_STR, employeeId, offset, taskCount);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			while (resultSet.next()){
				Task task = new Task();
				task.setId(Integer.parseInt(resultSet.getString(Task.DbNames.ID.getColName())));
				task.setProjectId(Integer.parseInt(resultSet.getString(Task.DbNames.PROJECT_ID.getColName())));
				task.setDescription(resultSet.getString(Task.DbNames.DESCRIPTION.getColName()));
				task.setPsd(resultSet.getDate(Task.DbNames.PSD.getColName()));
				task.setPdd(resultSet.getDate(Task.DbNames.PDD.getColName()));
				task.setAsd(resultSet.getDate(Task.DbNames.ASD.getColName()));
				
				Status status = new Status();
				status.setId(resultSet.getInt(Task.DbNames.STATUS_ID.getColName()));
				status.setName(resultSet.getString(STATUS_NAME_STR));
				task.setStatus(status);
				
				taskList.add(task);
			} 
			resultSet.close();
			
			resultSet = stmt.executeQuery("SELECT FOUND_ROWS()");
			if(resultSet.next()){
                this.countAllRecords = resultSet.getInt(1);
			}
			else {
				throw new DaoException("error sql query, not return count rows: " + query);
			}
			return taskList;
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	} // extends AbstractBaseDB


	public void setAssigneeOnTask(int taskId, int employeeId){
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT Member.Id FROM Member LEFT JOIN Task ON Task.ProjectId = Member.ProjectId WHERE Task.Id = %d AND Member.EmployeeId = %d",taskId, employeeId);
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			if (!resultSet.next()){
				throw new DaoException(Constants.ERROR_EMPLOYEE_NOT_MEMBER_PROJECT);
			} 
			int memberId = resultSet.getInt("Id");
			
			query = String.format("UPDATE Assigment SET `MemberId` = %d WHERE `TaskId` = %d",memberId, taskId);
			stmt.executeUpdate(query);
			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public void setStatusOnTask(int taskId, int statusId){
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		DBConnectionManager connectionManager = new DBConnectionManager();
		StringBuilder query = new StringBuilder();
		query.append(String.format("UPDATE Task SET `StatusId` = %d ",statusId));
		String curDate = MyUtils.getDateDBString(new java.util.Date());
		
		if (statusId == Status.Statuses.IN_PROGRESS.getId()){
			query.append(String.format(" ,`Asd` = '%s' ",curDate));
		} 
		if (statusId == Status.Statuses.RESOLVE.getId()){
			query.append(String.format(" ,`Add` = '%s' ",curDate));
		}
		if (statusId == Status.Statuses.OPEN.getId()){
			query.append(String.format(",`Asd` = NULL,`Add` = NULL "));
		}
		
		query.append(String.format(" WHERE `Id` = %d ",taskId));

		
		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			
			stmt.executeUpdate(query.toString());

			
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query.toString() + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public void addActivity(Activity activity) {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;
		PreparedStatement psInsertActivity = null;

		DBConnectionManager connectionManager = new DBConnectionManager();
		String query = String.format("SELECT * FROM Assigment WHERE TaskId = %d", activity.getTaskId());
		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			
			if (!resultSet.next()){
				throw new DaoException(Constants.ERROR_ASSIGNEE_ON_TASK);
			} 
			int memberId = resultSet.getInt("MemberId");
			int assigneeId = resultSet.getInt("Id");
			
			
			psInsertActivity = connection.prepareStatement(
					"INSERT INTO Activity (`Id`, `Date`, `Duration`, `Comment`,`MemberId`, `AssigmentId`) VALUES (NULL, ?, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);

			psInsertActivity.setString(Activity.DbNames.DATE.getColNum(), MyUtils.getDateDBStringHMS(new java.util.Date()));
			psInsertActivity.setInt(Activity.DbNames.DURATION.getColNum(), activity.getDuration());
			psInsertActivity.setString(Activity.DbNames.COMMENT.getColNum(), activity.getComment());
			psInsertActivity.setInt(Activity.DbNames.MEMBER_ID.getColNum(), memberId);
			psInsertActivity.setInt(Activity.DbNames.ASSIGMENT_ID.getColNum(), assigneeId);
			
			psInsertActivity.executeUpdate();
			
			
			psInsertActivity.close();
		} catch (ConnectionException e){
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
            throw new DaoException("Query: " + query + "\n"+e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
			try {
				if (psInsertActivity != null){
					psInsertActivity.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				throw new DaoException(e.getMessage());
			}
		}
		
	}


	public boolean isEmployeeInTaskPojectTeam(int taskId, int employeeId) {
		// TODO Auto-generated method stub
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet resultSet = null;

		String query = String.format("SELECT * FROM Task LEFT JOIN Member ON Task.ProjectId = Member.ProjectId WHERE Task.Id = %d AND Member.EmployeeId = %d", taskId,  employeeId);
		
		DBConnectionManager connectionManager = new DBConnectionManager();

		try {
			connection = connectionManager.getConnection();
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(query);
			
			return resultSet.next();
			

		} catch (ConnectionException e) {
			throw new DaoException("Can't connect to DB: +" + e);
		} catch (SQLException e) {
			throw new DaoException("Query: " + query + "\n" + e);
		} finally {
			connectionManager.closeConnection(connection, stmt, resultSet);
		}
	}

	public int getCountAllRows(){
		return countAllRecords;
	}
	

}
