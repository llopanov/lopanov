package validators;



import javax.servlet.http.HttpServletRequest;
import exceptions.ValidateException;

public class IdValidator extends AbstractValidator {
	private static final String ERROR_VALIDATE_ID = "wrong Id format";
	private String idName;
	
	public IdValidator(String idName){
		super();
		this.idName = idName;
	}

	public Object validate(HttpServletRequest request) throws ValidateException {
		// TODO Auto-generated method stub
		
		String strId = request.getParameter(idName);
		
				
		if (!validateId(strId)){
			throw new ValidateException (ERROR_VALIDATE_ID);
		}
				
		return new Integer(strId);
	}
	
	public void setIdName(String idName){
		this.idName = idName;
	}

}
