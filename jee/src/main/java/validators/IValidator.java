package validators;

import javax.servlet.http.HttpServletRequest;

import exceptions.ValidateException;

public interface IValidator {
	Object validate(HttpServletRequest request) throws ValidateException;
}
