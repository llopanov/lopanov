package validators;

import static bean.Config.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


import bean.Config;
import bean.Employee;
import bean.Status;
import bean.Task;
import exceptions.ValidateException;



public class TaskFormValidator extends AbstractValidator {
	private static final String ERROR_VALIDATE_DESCRIPTION_FORM_MORE_CHARS 	= "description is very short";
	private static final String ERROR_PDD 	= "wrong date pdd";
	private static final String ERROR_ID 	= "id is wrong format";
	private static final String ERROR_PSD 	= "wrong date psd";
	private static final String ERROR_PSD_AFTER_PDD 	= "psd later then pdd";
	
	//private static final String ERROR_FILE_UPLOAD 	= "error file upload";
	

	
	private static final String ERROR_CONTENT_TYPE_NOT_MULTIPART_FORM_DATA 	= "content type not multipart";
	@Override
	public Object validate(HttpServletRequest request) throws ValidateException {
		
		List <String> errors = new ArrayList<String>();
		// TODO Auto-generated method stub

		if(!ServletFileUpload.isMultipartContent(request)){
			throw new ValidateException(ERROR_CONTENT_TYPE_NOT_MULTIPART_FORM_DATA);
        }
		
		DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        fileFactory.setRepository(new File(Config.PATH_TO_TMP_UPLOAD_FILES));//Config.PATH_TO_UPLOAD_FILES));
        ServletFileUpload uploader = new ServletFileUpload(fileFactory);
        
		String projectId = null;// = request.getParameter(Task.DbNames.PROJECT_ID.getColName());
		String description = null;// = request.getParameter(Task.DbNames.DESCRIPTION.getColName());
		String psd = null;// = request.getParameter(Task.DbNames.PSD.getColName());
		String pdd = null;// = request.getParameter(Task.DbNames.PDD.getColName());
		String assigneeId = null;
        
		List<FileItem> fileList = new ArrayList<FileItem>();
        
		try {
			List<FileItem> fileItemsList = uploader.parseRequest(request);
			Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
			
            while(fileItemsIterator.hasNext()){
                FileItem fileItem = (FileItem) fileItemsIterator.next();        
            	if (fileItem.isFormField()) {
            		//System.out.println("field" + fileItem.getFieldName() +"  "+fileItem.getString());
            		//fileIt = fileItem;
            		String field = fileItem.getFieldName(); 
            		String value = fileItem.getString();
            		if (field.compareTo(Task.DbNames.PROJECT_ID.getColName()) == 0){
            			projectId = value; 
            		}else if(field.compareTo(Task.DbNames.DESCRIPTION.getColName()) == 0){
            			description = value; 
            		}else if(field.compareTo(Task.DbNames.PSD.getColName()) == 0){
            			psd = value;
                	}else if(field.compareTo(Task.DbNames.PDD.getColName()) == 0){
                		pdd = value;
                	}else if("assigneeId".compareTo(field) == 0){
                		assigneeId = value;
                	}else{
                		throw new ValidateException("dont know what the field");
                	}
            		
            	} else {
            		if (!fileItem.getName().isEmpty()){
            			fileList.add(fileItem);
            		}
 
            	}                
            }  // end while
            
		
            if (!validateId(projectId) || Integer.parseInt(projectId) == 0){
    			errors.add(ERROR_ID);
    		}
    		
    		if (description == null || description.trim().length() < MIN_LENGTH_DESCRIPTION ){
    			errors.add(ERROR_VALIDATE_DESCRIPTION_FORM_MORE_CHARS);
    			//throw new ValidateException ();
    		}
    		
    		if ( psd == null || !validateDate(psd)){
    			errors.add(ERROR_PSD);
    		}
    		
    		if ( pdd == null || !validateDate(pdd)){
    			errors.add(ERROR_PDD);
    		}
    		
    		if(!validateDates(psd, pdd)){
    			errors.add(ERROR_PSD_AFTER_PDD);
    		}
    		
    		
    		if (!errors.isEmpty()){
    			throw new ValidateException (errors.toString());
    		}
    		
    		Status status = new Status();
    		status.setId(Status.Statuses.OPEN.getId());
    		
    		Employee assignedEmployee = new Employee();
    		assignedEmployee.setId(Integer.parseInt(assigneeId));
    		
    		Task task = new Task();
    		task.setProjectId(Integer.parseInt(projectId));
    		task.setDescription(description);
    		task.setPsd(parseDate(psd));
    		task.setPdd(parseDate(pdd));
    		task.setStatus(status);
    		task.setAssignedEmployee(assignedEmployee);
    		
    		task.addFiles(fileList);
    		
    		return task;

		} catch (FileUploadException e1) {
			// TODO Auto-generated catch block
			throw new ValidateException(e1.getMessage());
		}
	}

}
