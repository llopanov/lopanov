package validators;

import static bean.Config.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import bean.Activity;

import exceptions.ValidateException;


public class ActivityFormValidator extends AbstractValidator {

	private static final String ERROR_VALIDATE_COMMENT_TO_SHORT 	= "comment to short";
	private static final String ERROR_VALIDATE_DURATION_MUST_BE_POSITIVE_INT 	= "duration must be positive int";
	private static final String ERROR_VALIDATE_TASK_MUST_BE_POSITIVE_INT 	= "task id is wrong";
	@Override
	public Object validate(HttpServletRequest request) throws ValidateException {
		
		List <String> errors = new ArrayList<String>();
		// TODO Auto-generated method stub
		
		String comment = request.getParameter(Activity.DbNames.COMMENT.getColName());
		String duration = request.getParameter(Activity.DbNames.DURATION.getColName());
		String taskId = request.getParameter("taskId");
		
		if (comment == null || comment.trim().length() < MIN_LENGTH_NAME)
		{
			errors.add(ERROR_VALIDATE_COMMENT_TO_SHORT);
		}
		
		
		if ( duration == null || !validatePositiveInt(duration)){
			errors.add(ERROR_VALIDATE_DURATION_MUST_BE_POSITIVE_INT);
		}
		
		if ( taskId == null || !validatePositiveInt(taskId)){
			errors.add(ERROR_VALIDATE_TASK_MUST_BE_POSITIVE_INT);
		}
		
		if (!errors.isEmpty()){
				throw new ValidateException (errors.toString());
		}
		Activity activity = new Activity();
		activity.setComment(comment);
		activity.setDuration(Integer.parseInt(duration));
		activity.setTaskId(Integer.parseInt(taskId));
		
		
		return activity;
	}
	
	

}
