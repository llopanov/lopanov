package validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.servlet.http.HttpServletRequest;

import exceptions.ValidateException;

import bean.Config;


public abstract class AbstractValidator implements IValidator{
	 private static final String EMAIL_PATTERN =
             "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
             "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	 private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);
	 private Matcher matcher;
	 
	 private static final SimpleDateFormat formatter = new SimpleDateFormat(Config.DATE_VIEW_STRING_FORMAT);
	 
	 public enum Required {
	     YES, NO
	 }
	 
	abstract public Object validate(HttpServletRequest request) throws ValidateException;
	
	protected boolean validateEmail(String email, Required req){
		if (email.length() == 0 && req == Required.NO){
			return true;
		} 
		matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	protected boolean validateDate(String date){

		try {
			new Date(formatter.parse(date).getTime()); 
			return true;
		} catch (ParseException e) {
			return false;
		}
	}
	
	protected boolean validateDates(String earlerDate, String laterDate){

		try {
			Date earler = new Date(formatter.parse(earlerDate).getTime()); 
			Date later = new Date(formatter.parse(laterDate).getTime());
			if (earler.compareTo(later) > 0){
				return false;
			}
			
			return true;
			
		} catch (ParseException e) {
			return false;
		}
	}
	
	protected boolean validatePositiveInt(String intVal){

		try {
			int val = Integer.parseInt(intVal);
			if (val>0){
				return true;
			}
			return false;
			
		} catch (NumberFormatException e) {
			return false;
		}
	}

	
	protected boolean validateId(String strId){

		if (strId == null){
			return false;
		}
		
		try{
			Integer id = Integer.parseInt(strId);
			
			if (id < 0){
				return false;
			}
			return true;
		} catch (NumberFormatException e){
			return false;
		}
	}
	
	protected Date parseDate(String date){
		try {
			return new Date(formatter.parse(date).getTime()); 
		} catch (ParseException e) {
			return null;
		}
	}
}
