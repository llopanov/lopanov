package validators;

import static bean.Config.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import bean.Employee;
import bean.Project;
import bean.Role;
import exceptions.ValidateException;


public class ProjectFormValidator extends AbstractValidator {
	private static final String FORMAT_MEMBERS_EMPID = "employee[%d][empid]";
	private static final String FORMAT_MEMBERS_ROLEID = "employee[%d][roleid]";
	private static final String ERROR_VALIDATE_NAME 	= "Very short name, must be longer than 2 charachters";
	private static final String ERROR_VALIDATE_DESCRIPTION 	= "Very short description, must be longer than 2 charachters";
	private static final String ERROR_ID 	= "id is wrong format";
	private static final String ERROR_COUNT_MEMBERS 	= "You must select some employee to project";
	private static final String ERROR_INVALID_COUNT_MEMBERS = "Why you so smart?";
	@Override
	public Object validate(HttpServletRequest request) throws ValidateException {
		
		List <String> errors = new ArrayList<String>();
		// TODO Auto-generated method stub
//		System.out.println(request.getParameter(Project.DbNames.ID.getColName()));
//		System.out.println(request.getParameter(Project.DbNames.NAME.getColName()));
//		System.out.println(request.getParameter(Project.DbNames.DESCRIPTION.getColName()));
		
		String id = request.getParameter(Project.DbNames.ID.getColName());
		String name = request.getParameter(Project.DbNames.NAME.getColName());
		String description = request.getParameter(Project.DbNames.DESCRIPTION.getColName());
		String count = request.getParameter("count");
		
		
		
		if (!validateId(id) ){		
			errors.add(ERROR_ID);
		}
		
		if (name == null || name.trim().length() < MIN_LENGTH_NAME)
		{
			errors.add(ERROR_VALIDATE_NAME);
		}
		
		if (description == null || description.trim().length() < MIN_LENGTH_NAME)
		{
			errors.add(ERROR_VALIDATE_DESCRIPTION);
		}
		
		if (validateId(count) ){
			if( Integer.parseInt(count) == 0 ){
				errors.add(ERROR_COUNT_MEMBERS);
			}
		}else {
			errors.add(ERROR_INVALID_COUNT_MEMBERS);
		}
		
		
		if (!errors.isEmpty()){
			throw new ValidateException (errors.toString());
		}
		
		Project project = new Project();
		project.setId(Integer.parseInt(id));
		project.setName(name);
		project.setDescription(description);
		
		for (int i=0; i<Integer.parseInt(count); i++){
			String keyEmpId = request.getParameter(String.format(FORMAT_MEMBERS_EMPID, i));
			String keyRoleId = request.getParameter(String.format(FORMAT_MEMBERS_ROLEID, i));
			
			if (!validateId(keyEmpId) ||
				!validateId(keyRoleId) ){
				errors.add(ERROR_INVALID_COUNT_MEMBERS);
				break;
			}
			Employee emp = new Employee();
			Role role = new Role();
			emp.setId(Integer.parseInt(keyEmpId));
			role.setId(Integer.parseInt(keyRoleId));
			project.addMember(emp, role);
			
			
		}
		
		return project;
	}
	
	

}
