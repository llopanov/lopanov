package validators;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import bean.Employee;
import exceptions.ValidateException;

public class LoginFormValidator extends AbstractValidator {
	private static final String ERROR_VALIDATE_LOGIN_PASSWORD_EMPTY = "you should fill fields login and password ";
	private static final String ERROR_VALIDATE_LOGIN_FORM_MORE_CHARS = "login and password must be contains more characters";
	private static final int MIN_LENGTH_LOGIN_PASS = 2;
	

	public Object validate(HttpServletRequest request) throws ValidateException {
		// TODO Auto-generated method stub
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		//System.out.println(login+" <--> "+password);
		List <String> errors = new ArrayList<String>();
		
		
		if (login == null || password == null)
		{
			errors.add(ERROR_VALIDATE_LOGIN_PASSWORD_EMPTY);
		}
		
		login = login.trim();
		
		if (login.length() < MIN_LENGTH_LOGIN_PASS || password.length() < MIN_LENGTH_LOGIN_PASS){
			errors.add(ERROR_VALIDATE_LOGIN_FORM_MORE_CHARS);
		}
		
		if (!errors.isEmpty()){
			throw new ValidateException (errors.toString());
		}
		
		Employee employee = new Employee();
		employee.setLogin(login);
		employee.setPassword(password);
		return employee;
	}

}
