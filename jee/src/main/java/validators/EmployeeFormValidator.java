package validators;

import static bean.Config.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import bean.Employee;
import bean.Position;
import exceptions.ValidateException;


public class EmployeeFormValidator extends AbstractValidator {
	private static final String ERROR_VALIDATE_FIRST_NAME 	= "Very short first name, must be longer than 2 charachters";
	private static final String ERROR_VALIDATE_LAST_NAME 	= "Very short last name, must be longer than 2 charachters";
	private static final String ERROR_VALIDATE_LOGIN_FORM_MORE_CHARS 	= "login is very short";
	private static final String ERROR_VALIDATE_PASSWORD_FORM_MORE_CHARS 	= "password is very short";
	private static final String ERROR_VALIDATE_PASSWORDS_NOT_MATCH 	= "passwords not match";
	private static final String ERROR_ID 	= "id is wrong format";
	private static final String ERROR_POSITION_ID 	= "position id is wrong format";
	@Override
	public Object validate(HttpServletRequest request) throws ValidateException {
		
		List <String> errors = new ArrayList<String>();
		// TODO Auto-generated method stub
//		System.out.println("valid");
//		System.out.println(request.getParameter(Employee.DbNames.ID.getColName()));
//		System.out.println(request.getParameter(Employee.DbNames.FIRST_NAME.getColName()));
//		System.out.println(request.getParameter(Employee.DbNames.LAST_NAME.getColName()));
//		System.out.println(request.getParameter(Employee.DbNames.LOGIN.getColName()));
//		System.out.println(request.getParameter(Employee.DbNames.PASSWORD.getColName()));
//		System.out.println(request.getParameter("rePassword"));
//		System.out.println(request.getParameter(Employee.DbNames.POSITION_ID.getColName()));
		
		
		String id = request.getParameter(Employee.DbNames.ID.getColName());
		String firstName = request.getParameter(Employee.DbNames.FIRST_NAME.getColName());
		String lastName = request.getParameter(Employee.DbNames.LAST_NAME.getColName());
		String login = request.getParameter(Employee.DbNames.LOGIN.getColName());
		String password = request.getParameter(Employee.DbNames.PASSWORD.getColName());
		String rePassword = request.getParameter("rePassword");
		String positionId = request.getParameter(Employee.DbNames.POSITION_ID.getColName());
		
	
		
		if (firstName == null || firstName.trim().length() < MIN_LENGTH_NAME)
		{
			errors.add(ERROR_VALIDATE_FIRST_NAME);
		}
		
		if (lastName == null || lastName.trim().length() < MIN_LENGTH_NAME)
		{
			errors.add(ERROR_VALIDATE_LAST_NAME);
		}
		
		
		
//		if (login == null || password == null) {
//			throw new ValidateException (""); // no errors for first entry on the page, but error for login action
//		}
		
//		login = login.trim();
		
		if (login == null  || login.trim().length() < MIN_LENGTH_LOGIN_PASS ){
			errors.add(ERROR_VALIDATE_LOGIN_FORM_MORE_CHARS);
			//throw new ValidateException ();
		}
		

		
		if (validateId(id) ){
			
			if (Integer.parseInt(id) == 0){ // new employee
				if ( password == null || password.length() < MIN_LENGTH_LOGIN_PASS){
					errors.add(ERROR_VALIDATE_PASSWORD_FORM_MORE_CHARS);
				} 
				 if (!password.equals(rePassword)){
					 errors.add(ERROR_VALIDATE_PASSWORDS_NOT_MATCH);
				 }
			}
			else { 		// edit employee
					if (!password.equals(rePassword)){
						errors.add(ERROR_VALIDATE_PASSWORDS_NOT_MATCH);
					}
					if (!password.isEmpty() && password.length() < MIN_LENGTH_LOGIN_PASS){
						errors.add(ERROR_VALIDATE_PASSWORD_FORM_MORE_CHARS);
					}

			}
		} else {
			errors.add(ERROR_ID);
		}
//			if (!checkPassword( password, rePassword)){
//				errors.add(ERROR_VALIDATE_PASSWORD_FORM_MORE_CHARS);
//			}
		
		if ( positionId == null || !validateId(positionId)){
			errors.add(ERROR_POSITION_ID);
		}
		
		if (!errors.isEmpty()){
				throw new ValidateException (errors.toString());
		}
		Employee employee = new Employee();
		employee.setId(Integer.parseInt(id));
		employee.setFirstName(firstName);
		employee.setLastName(lastName);
		employee.setLogin(login);
		employee.setPassword(password);
		Position position = new Position();
		position.setId(Integer.parseInt(positionId));
		employee.setPosition(position);
		
		
		return employee;
	}
	
	

}
