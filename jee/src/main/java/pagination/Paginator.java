package pagination;


public class Paginator {
	private int countElements;
	private int countElementsOnPage;
	private int currentPage;
	
//	IPaginationDAO paginator;
	
	//List<Object> items = new ArrayList<>();
//	private int countPages;
	
//	public Paginator(IPaginationDAO paginatorDAO){
//		this.paginator = paginator;
//	}
	
	public Paginator(int countElements, int countElementsOnPage, int currentPage){
		this.countElements = countElements;
		this.countElementsOnPage = countElementsOnPage;
		this.currentPage = currentPage;
	}
	
	public int getCurrentPage(){
		return currentPage;
	}
	
	public int getCountElementsOnPage(){
		return countElementsOnPage;
	}
	
	public int getCountPages(){
		return (int) Math.ceil(Math.ceil((double)countElements / countElementsOnPage));
	}
	
	public boolean getShowPaginator(){
		return (getCountPages() > 1);
	}
	
//	public List<Object> getItems(int page, ){
//		paginatorDAO.getItems(employeeId, page, totalCount)
//		return items;
//	}
}
