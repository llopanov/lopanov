package pagination;

import java.util.List;

public interface IPaginationDAO{
	public List<Object> getItems(int employeeId, int page, int totalCount);
	public int getCountAllRows();
}

