package filters;

import static logger.LoggerClass.LOGGER;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOInterfaces.ITaskDAO;
import bean.Constants;
import bean.Employee;
import exceptions.ValidateException;
import factories.modelFactory;
import validators.IValidator;
import validators.IdValidator;

/**
 * Servlet Filter implementation class AccessFilter
 */
public class AccessTaskFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AccessTaskFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		Employee emp = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE);
		
		if (emp != null && req.getParameter("taskId")!=null){ // if authorised and not concrete task
			try{
				IValidator validator = new IdValidator("taskId");
				int taskId = (Integer) validator.validate(req);
			
				ITaskDAO taskDAO = modelFactory.getTaskModel();
				boolean isUserInProject = taskDAO.isEmployeeInTaskPojectTeam(taskId,emp.getId());
				
				if (!isUserInProject){
					Employee employee = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE);
					LOGGER.warn("Access to '"+req.getServletPath()+"' for user "+employee.getLogin()+" forbidden, user not on a project");

					
					resp.sendError(HttpServletResponse.SC_FORBIDDEN);
					
					return;
				}
			}catch (ValidateException e){
				Employee employee = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE);
				LOGGER.warn("Access to '"+req.getServletPath()+"' for user "+employee.getLogin()+" forbidden, error taskIdField");

				
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
				
								
				return;
			}
		
		}
		chain.doFilter(request, response);
		
		// pass the request along the filter chain
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
