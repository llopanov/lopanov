package filters;

import static logger.LoggerClass.LOGGER;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Constants;
import bean.Employee;

/**
 * Servlet Filter implementation class AccessFilter
 */
public class AccessAJAXFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AccessAJAXFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		boolean ajax = "XMLHttpRequest".equals(req.getHeader("x-requested-with"));
		if (!ajax){
			HttpSession session = req.getSession();
			Employee employee = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE);
			if (employee != null){
				LOGGER.warn("Access to '"+req.getServletPath()+"' for user "+employee.getLogin()+" forbidden, not AJAX request");
			}
			
			
			resp.sendError(HttpServletResponse.SC_FORBIDDEN);
			//return;
		}
		chain.doFilter(request, response);
		
		// pass the request along the filter chain
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
