package filters;

import static logger.LoggerClass.LOGGER;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import access.FactoryRoles;
import access.IRole;
import bean.Constants;
import bean.Employee;

/**
 * Servlet Filter implementation class AccessFilter
 */
public class AccessFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AccessFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		
		if (!req.getRequestURI().startsWith(req.getContextPath()+"/resources/")) {
		
			
			
			IRole role = (IRole) session.getAttribute(Constants.SESSTION_ATTR_NAME_ROLE);

			if (role == null){
				
				role = FactoryRoles.getRole(null);
				session.setAttribute(Constants.SESSTION_ATTR_NAME_ROLE, role);
			}
			
			//System.out.println(req.getServletPath() + " <-path   "+role.hasAllow(req.getServletPath()));

			if (!role.hasAllow(req.getServletPath())){
				Employee employee = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE);
				if (employee != null){
					LOGGER.warn("Access to '"+req.getServletPath()+"' for user "+employee.getLogin()+" forbidden ");
				}
				
				//resp.sendRedirect(req.getContextPath()+role.getHomePage());
				resp.sendError(HttpServletResponse.SC_FORBIDDEN);
				
				return;
			}
		
		}
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
