package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAOInterfaces.IEmployeeDAO;
import bean.Constants;
import bean.Employee;
import exceptions.DaoException;
import factories.modelFactory;



/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class EmployeeOnProjectController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public EmployeeOnProjectController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Map<String, String> options = new HashMap<String, String>();
			
		IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
		try{
			int idProj = Integer.parseInt(request.getParameter("projId"));
			List<Employee> listEmployee = employeeDAO.getEmployeesFromProject(idProj);
			StringBuilder str = new StringBuilder();
			
			for (Employee e : listEmployee){
				str.append(new Gson().toJson(e));
			}
			
			response.setContentType(Constants.CONTENT_TYPE_JSON);
		    response.setCharacterEncoding(Constants.ENCODING_UTF8);
		    response.getWriter().write(new Gson().toJson(listEmployee));

		} catch (NumberFormatException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		} catch(DaoException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
