package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOInterfaces.IActivityDAO;
import bean.Activity;
import bean.Config;
import bean.Constants;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;
import validators.IdValidator;


/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class ActivityListController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ActivityListController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		IActivityDAO activityDAO = modelFactory.getActivityModel();

		try{
			IdValidator validator = new IdValidator("taskId");
			int taskId = (Integer) validator.validate(request);

			List <Activity> activityList = activityDAO.getActivitiesForTask(taskId);
			
			request.setAttribute("activityList", activityList);
			
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_PATH +"list_activities.jsp");
		    rd.forward(request, response);
		} catch (ValidateException e){
			//System.out.println(e);
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} catch (DaoException e){
			//System.out.println(e);
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		}
	}

}
