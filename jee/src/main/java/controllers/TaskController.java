package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import validators.IValidator;
import validators.IdValidator;

import DAOInterfaces.IActivityDAO;
import DAOInterfaces.IEmployeeDAO;
import DAOInterfaces.IStatusDAO;
import DAOInterfaces.ITaskDAO;
import bean.Activity;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Status;
import bean.Task;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;

/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class TaskController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TaskController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			IValidator validator = new IdValidator("taskId");
			Integer taskId = (Integer) validator.validate(request);
			
			ITaskDAO taskDAO = modelFactory.getTaskModel();
			IStatusDAO statusDAO = modelFactory.getStasusModel();
			IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
			IActivityDAO activityDAO = modelFactory.getActivityModel();
			
			Task task = taskDAO.getTaskById(taskId);
			
			List<Status> statusList = statusDAO.getStatuses();
			List<Employee> employeeList = employeeDAO.getEmployeesFromProject(task.getProjectId());
			
			List <Activity> activityList = activityDAO.getActivitiesForTask(taskId);
			
			request.setAttribute("activityList", activityList);
			request.setAttribute("statusList", statusList);
			request.setAttribute("employeeList", employeeList);
			request.setAttribute("task", task);
			
			
		}catch (ValidateException e){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}catch (DaoException e){
			request.setAttribute(Constants.ERROR_MSG, e.getMessage());
			//doGet(request, response);
			
		}
		
		request.setAttribute(Constants.PAR_NAME_PAGENAME, "task.jsp");	
		RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
