package controllers;

import static logger.LoggerClass.LOGGER;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOInterfaces.ITaskDAO;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Task;
import exceptions.DaoException;
import factories.modelFactory;
import pagination.Paginator;
import validators.IValidator;
import validators.IdValidator;


/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class TaskListController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TaskListController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ITaskDAO taskDAO = modelFactory.getTaskModel();
		HttpSession session = request.getSession();
		Employee employee = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE);
		try{
			IValidator validator = new IdValidator("pageNum");
			int pageNum = (Integer) validator.validate(request);
			int taskCount  = Config.COUNT_TASK_PER_PAGE;
			List <Task> taskList = taskDAO.getTasks(employee.getId(), (pageNum-1)*taskCount, taskCount);
			int taskAllCount  = taskDAO.getCountAllRows();

			Paginator paginator = new Paginator(taskAllCount, taskCount, pageNum);
			request.setAttribute("paginator", paginator);
			request.setAttribute("taskList", taskList);
			
			
			LOGGER.info("Task list for user"+employee.getLogin());
			
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_PATH +"list_tasks.jsp");
		    rd.forward(request, response);
		} catch (DaoException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		}
	}

}
