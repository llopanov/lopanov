package controllers.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.DaoException;
import factories.modelFactory;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Project;
import DAOInterfaces.IEmployeeDAO;
import DAOInterfaces.IProjectsDAO;

/**
 * Servlet implementation class AdminController
 */
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("1234");
		IEmployeeDAO empDAO = modelFactory.getEmployeeModel();
		IProjectsDAO projDAO = modelFactory.getProjectModel();
		
		try{
			
			List <Employee> empList = empDAO.getEmployees();
			request.setAttribute("listEmployees", empList);
			
			
			List <Project> projList = projDAO.getProjects();
			request.setAttribute("listProjects", projList);
			
			
		} catch (DaoException e){
			request.setAttribute(Constants.ERROR_MSG, e.getMessage());
			

		}
		request.setAttribute(Constants.PAR_NAME_PAGENAME, "admin.jsp");	
		RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
	    rd.forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
