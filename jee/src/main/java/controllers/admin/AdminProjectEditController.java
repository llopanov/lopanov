package controllers.admin;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.MyUtils;
import validators.IValidator;
import validators.IdValidator;
import validators.ProjectFormValidator;
import DAOInterfaces.IEmployeeDAO;
import DAOInterfaces.IProjectsDAO;
import DAOInterfaces.IRoleDAO;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Project;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;


/**
 * Servlet implementation class AdminEmployeeController
 */
public class AdminProjectEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminProjectEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		IProjectsDAO projectDAO = modelFactory.getProjectModel();
		IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
		IRoleDAO roleDAO = modelFactory.getRoleModel();

		try{
			IValidator validator = new IdValidator(Employee.DbNames.ID.getColName());
			Integer id = (Integer) validator.validate(request);
			
			Project project;
			if (id == 0 ) { // new employee
				project = new Project();
			} else {		// edit employee
				project = projectDAO.getProjectById(id);
			}
			
			request.setAttribute("project", project);
			request.setAttribute("employeesList", employeeDAO.getEmployees());
			request.setAttribute("rolesList", roleDAO.getRoles());
				
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_PATH + "forms/projectForm.jsp");
			rd.forward(request, response);
		} catch (DaoException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} catch (ValidateException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, "wrong format");
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		Map<String, String> options = new HashMap<String, String>();
		
		IProjectsDAO projectDAO = modelFactory.getProjectModel();
		try{
			IValidator validateForm = new ProjectFormValidator();
			Project project = (Project)validateForm.validate(request);
			
			if (project.getId()==0){ //add
				projectDAO.addProject(project);
			} else { // edit
				projectDAO.updateProject(project);
			}

			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);

		} catch (ValidateException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		} catch(DaoException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		}
		/* */
		MyUtils.sendWithJSON(options, response);
	}
	
	
}
