package controllers.admin;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import utils.MyUtils;
import validators.EmployeeFormValidator;
import validators.IValidator;
import validators.IdValidator;
import DAOInterfaces.IEmployeeDAO;
import DAOInterfaces.IPositionDAO;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Position;

import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;


/**
 * Servlet implementation class AdminEmployeeController
 */
public class AdminEmployeeEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminEmployeeEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
		IPositionDAO positionDAO = modelFactory.getPositionModel();

		try{
			IValidator validator = new IdValidator(Employee.DbNames.ID.getColName());
			int id = (Integer) validator.validate(request);
			
			Employee employee;
			if (id == 0 ) { // new employee
				employee = new Employee();
			} else {		// edit employee
				employee = employeeDAO.getEmployeeById(id);
			}
			request.setAttribute("employee", employee);
			
			List <Position> positionList = positionDAO.getPositions();
			
			request.setAttribute("listPositions", positionList);
			
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_PATH + "forms/employeeForm.jsp");
			rd.forward(request, response);
		} catch (DaoException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} catch (ValidateException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, "wrong format");
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Map<String, String> options = new HashMap<String, String>();
		IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
		try{
			
			IValidator validateForm = new EmployeeFormValidator();
			Employee employee = (Employee)validateForm.validate(request);
			
			if (employee.getId()==0){ //add
				employeeDAO.addEmployee(employee);
			} else { // edit
				employeeDAO.updateEmployee(employee);
			}

			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);

		} catch (ValidateException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		} catch(DaoException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		}
		
		MyUtils.sendWithJSON(options, response);
	}
	
//	protected void  doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException , IOException{
//		
//		//Employee employee;
//		IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
//		Map<String, String> options = new HashMap<String, String>();
//		try{
//			System.out.println(request.getParameter(Employee.DbNames.ID.getColName()));
//			Integer id = Integer.parseInt(request.getParameter(Employee.DbNames.ID.getColName()));
//		
//			if (id > 0 ) { // new employee
//				employeeDAO.deleteEmployee(id);
//			} else {		// edit employee
//				throw new NumberFormatException();
//			}
//			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);
//		} catch (NumberFormatException e){
//			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
//			options.put(Constants.PAR_NAME_MESSAGE, "wrong format");
//		}
//		
//		MyUtils.sendWithJSON(options, response);
//	}
	
}
