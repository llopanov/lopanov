package controllers.admin;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.MyUtils;
import validators.IValidator;
import validators.IdValidator;
import DAOInterfaces.IProjectsDAO;
import bean.Constants;
import bean.Employee;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;


/**
 * Servlet implementation class AdminEmployeeController
 */
public class AdminProjectDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminProjectDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		IProjectsDAO projectDAO = modelFactory.getProjectModel();
		Map<String, String> options = new HashMap<String, String>();
		try {
			IValidator validator = new IdValidator(Employee.DbNames.ID.getColName());
			Integer id = (Integer) validator.validate(request);
			
			projectDAO.deleteProject(id);

			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);
		} catch (ValidateException e) {
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, "wrong format");
		} catch (DaoException e) {
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());
		}

		MyUtils.sendWithJSON(options, response);
	}
	
}
