package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOInterfaces.IActivityDAO;
import DAOInterfaces.ITaskDAO;
import bean.Activity;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Task;
import factories.modelFactory;
import pagination.Paginator;


/**
 * Servlet implementation class DashBoardController
 */
public class DashboardController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashboardController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		
		Employee employee = (Employee) session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE); 
		
		ITaskDAO taskDAO = modelFactory.getTaskModel();
		IActivityDAO activityDAO = modelFactory.getActivityModel();
		
		int pageNumTask = 1; // default number of page 
		int taskCount  = Config.COUNT_TASK_PER_PAGE;
		List <Task> taskList = taskDAO.getTasks(employee.getId(), (pageNumTask-1)*taskCount, taskCount);
		int taskAllCount  = taskDAO.getCountAllRows();

		Paginator paginatorTask = new Paginator(taskAllCount, taskCount, pageNumTask);
		request.setAttribute("paginator", paginatorTask);
		
		int pageNumActivity = 1; // default number of page activity
		int activityCount  = Config.COUNT_ACTIVITIES_PER_PAGE;
		List <Activity> activityList = activityDAO.getAllActivities(activityCount,activityCount*(pageNumActivity-1));
		int activityAllCount  = activityDAO.getCountAllRows();
		
		Paginator paginatorActivity = new Paginator(activityAllCount, activityCount, pageNumActivity);
		
		request.setAttribute("paginatorActivity", paginatorActivity);
		request.setAttribute("activityList", activityList);
		request.setAttribute("taskList", taskList);
		request.setAttribute(Constants.PAR_NAME_PAGENAME, "dashboard.jsp");		
		
		RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
