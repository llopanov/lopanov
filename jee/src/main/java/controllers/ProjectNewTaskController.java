package controllers;

import static logger.LoggerClass.LOGGER;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOInterfaces.IEmployeeDAO;
import DAOInterfaces.IProjectsDAO;
import DAOInterfaces.ITaskDAO;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Project;
import bean.Task;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;
import utils.MyUtils;
import validators.IValidator;
import validators.TaskFormValidator;


/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class ProjectNewTaskController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ProjectNewTaskController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Employee employee = (Employee) (session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE));
		
		IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
		IProjectsDAO projectDAO = modelFactory.getProjectModel();

		LOGGER.info("Get new task form"+employee.getLogin());
		try{
			List <Project> projectsList = projectDAO.getProjectsWhereMemberManager(employee.getId());

			request.setAttribute("listProjects", projectsList);

			int selectedProjectId = projectsList.get(0).getId();
			request.setAttribute("selectedProjectId", selectedProjectId);
			List <Employee> listEmployees = employeeDAO.getEmployeesFromProject(selectedProjectId);

			request.setAttribute("listEmployees", listEmployees);
			
			
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_PATH +"forms/taskForm.jsp");
			rd.forward(request, response);
		} catch (DaoException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} catch (NumberFormatException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, "wrong format");
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} catch (IndexOutOfBoundsException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, "projects where you manager not exist");
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} 
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		ITaskDAO taskDAO = modelFactory.getTaskModel();

		Map<String, String> options = new HashMap<String, String>();
		
		LOGGER.info("Trying add new task");
		try{
			IValidator validateForm = new TaskFormValidator();
			Task task = (Task) validateForm.validate(request);

			int taskId = taskDAO.addTask(task);

			LOGGER.info("Add new task - " +taskId);
			
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);
			options.put(Constants.PAR_NAME_REDIRECT, request.getContextPath() +"/task?taskId="+taskId);

		} catch (ValidateException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		} catch(DaoException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		}
		
		MyUtils.sendWithJSON(options, response);
	}

}
