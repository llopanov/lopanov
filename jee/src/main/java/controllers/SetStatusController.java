package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOInterfaces.ITaskDAO;
import bean.Constants;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;
import utils.MyUtils;
import validators.IValidator;
import validators.IdValidator;

import static logger.LoggerClass.*;
/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class SetStatusController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public SetStatusController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Map<String, String> options = new HashMap<String, String>();
		try{
			IValidator validator = new IdValidator("taskId");
			Integer taskId = (Integer) validator.validate(request);
			
			validator = new IdValidator("statusId");
			Integer statusId = (Integer) validator.validate(request);
			
			ITaskDAO taskDAO = modelFactory.getTaskModel();
			taskDAO.setStatusOnTask(taskId, statusId);
			
			LOGGER.info("Change task status taskId = "+taskId+", statusId = "+ statusId);
			
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);
			//options.put("redirectURI", "dashboard");
		} catch (ValidateException e) {
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, "wrong format");
			
		} catch (DaoException e) {
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());
		}
		MyUtils.sendWithJSON(options, response);
	}

}
