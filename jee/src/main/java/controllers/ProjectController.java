package controllers;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAOInterfaces.IProjectsDAO;
import bean.Config;
import bean.Constants;
import bean.Project;
import exceptions.DaoException;
import factories.modelFactory;
import validators.IValidator;
import validators.IdValidator;

/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class ProjectController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ProjectController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		IProjectsDAO projectDAO = modelFactory.getProjectModel();

		try{
			IValidator validator = new IdValidator("projId");
			int id = (Integer) validator.validate(request);
			
			Project project = projectDAO.getProjectById(id);

			request.setAttribute("project", project);
			
		} catch (DaoException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			
		} 
		request.setAttribute(Constants.PAR_NAME_PAGENAME, "project.jsp");	
		RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
	}

}
