package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOInterfaces.IEmployeeDAO;
import access.FactoryRoles;
import access.IRole;
import bean.Config;
import bean.Constants;
import bean.Employee;
import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;
import validators.IValidator;
import validators.LoginFormValidator;

import static logger.LoggerClass.*;
/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoginController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.setAttribute(Constants.PAR_NAME_PAGENAME, "login.jsp");	
		RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		try{
			IValidator validateForm = new LoginFormValidator();
			Employee employee = (Employee) validateForm.validate(request);
			
			IEmployeeDAO employeeDAO = modelFactory.getEmployeeModel();
		
			employee = employeeDAO.getEmployee(employee.getLogin(), employee.getPassword());
			
			HttpSession session = request.getSession();
			session.setAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE, employee);
			IRole role = FactoryRoles.getRole(employee.getPosition().getId());
			session.setAttribute(Constants.SESSTION_ATTR_NAME_ROLE, role);
			
			LOGGER.info("Logging successful, your login is "+employee.getLogin());
			LOGGER.info("role is "+role.getClass().getName());
			
			response.sendRedirect(request.getContextPath() +role.getHomePage());

		}
		catch (ValidateException e){
			request.setAttribute(Constants.ERROR_MSG, e.getMessage());
			//doGet(request, response);
			request.setAttribute(Constants.PAR_NAME_PAGENAME, "login.jsp");	
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
			rd.forward(request, response);
		}
		catch(DaoException e){
			request.setAttribute(Constants.ERROR_MSG, e.getMessage());
			//doGet(request, response);
			request.setAttribute(Constants.PAR_NAME_PAGENAME, "login.jsp");	
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_MAIN);
			rd.forward(request, response);
		}
	}

}
