package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAOInterfaces.ITaskDAO;
import bean.Attachment;
import bean.Config;

import bean.Task;
import exceptions.ValidateException;
import factories.modelFactory;
import utils.MyUtils;
import validators.IdValidator;


/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class TaskDownloadFileController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TaskDownloadFileController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
		
		IdValidator validateForm = new IdValidator("taskId");
		int taskId = (Integer) validateForm.validate(request);
		validateForm.setIdName("fileId");
		int fileId = (Integer) validateForm.validate(request);
    	ITaskDAO taskDAO = modelFactory.getTaskModel();
    	
    	Task task = taskDAO.getTaskById(taskId);
    	Attachment attachment = null;
    	for(Attachment attach : task.getAttachments()){
    		if (fileId == attach.getId()){
    			attachment = attach;
    			break;
    		}
    	}
		
        if(attachment == null ){//|| fileName.equals("")
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            //throw new ServletException("File Name can't be null or empty");
        }
        
        String fileName = MyUtils.generateFileName(taskId, attachment.getName());
        
        File file = new File(Config.PATH_TO_UPLOAD_FILES+File.separator+fileName);
        if(!file.exists()){
            throw new ServletException("File doesn't exists on server.");
        }

        InputStream fis = new FileInputStream(file);
        String mimeType = attachment.getDescription();//ctx.getMimeType(file.getAbsolutePath());
        response.setContentType(mimeType != null? mimeType:"application/octet-stream");
        response.setContentLength((int)attachment.getSize());//(int) file.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getName() + "\"");
         
        ServletOutputStream os       = response.getOutputStream();
        byte[] bufferData = new byte[1024];
        int read=0;
        while((read = fis.read(bufferData))!= -1){
            os.write(bufferData, 0, read);
        }
        os.flush();
        os.close();
        fis.close();
		} catch (ValidateException e){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
