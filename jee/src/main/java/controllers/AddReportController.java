package controllers;

import static logger.LoggerClass.LOGGER;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOInterfaces.ITaskDAO;
import bean.Activity;
import bean.Config;
import bean.Constants;

import exceptions.DaoException;
import exceptions.ValidateException;
import factories.modelFactory;
import utils.MyUtils;
import validators.ActivityFormValidator;
import validators.IValidator;
import validators.IdValidator;

/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class AddReportController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public AddReportController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		try{
			IValidator validator = new IdValidator("taskId");
			int taskId = (Integer) validator.validate(request);

			request.setAttribute("taskId", taskId);
			
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_PATH +"forms/reportForm.jsp");
			rd.forward(request, response);
		} catch (ValidateException e){
			request.setAttribute("error", "wrong format");
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ITaskDAO taskDAO = modelFactory.getTaskModel();

		Map<String, String> options = new HashMap<String, String>();
		
		LOGGER.info("Trying add report");
		try{
			IValidator validateForm = new ActivityFormValidator();
			Activity activity = (Activity) validateForm.validate(request);
			
			taskDAO.addActivity(activity);
			
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_OK);

		} catch (ValidateException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		} catch(DaoException e){
			options.put(Constants.PAR_NAME_STATUS, Constants.PAR_NAME_STATUS_ERROR);
			options.put(Constants.PAR_NAME_MESSAGE, e.getMessage());

		}
		
		MyUtils.sendWithJSON(options, response);
		
	}

}
