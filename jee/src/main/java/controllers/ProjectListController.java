package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import DAOInterfaces.IProjectsDAO;
import bean.Config;
import bean.Constants;
import bean.Employee;
import bean.Project;
import exceptions.DaoException;
import factories.modelFactory;

/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class ProjectListController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ProjectListController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		Employee employee = (Employee) (session.getAttribute(Constants.SESSTION_ATTR_NAME_EMPLOYEE));

		IProjectsDAO projectDAO = modelFactory.getProjectModel();

		try{
			List <Project> projectList = projectDAO.getProjectsWhereMember(employee.getId());
			
			response.setContentType(Constants.CONTENT_TYPE_JSON);
		    response.setCharacterEncoding(Constants.ENCODING_UTF8);
		    response.getWriter().write(new Gson().toJson(projectList));			
		} catch (DaoException e){
			request.setAttribute(Constants.PAR_NAME_STATUS_ERROR, e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher(Config.TEMPLATE_ERROR_AJAX);
			rd.forward(request, response);
		} 
		
	}

}
