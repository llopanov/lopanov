package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import DAOInterfaces.ITaskDAO;
import bean.Attachment;
import bean.Constants;
import bean.Task;
import exceptions.ValidateException;
import factories.modelFactory;
import utils.MyUtils;
import validators.IdValidator;


/**
 * Servlet implementation class IndexController
 */
//@WebServlet(urlPatterns = {"/index","/"})
public class TaskExportController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TaskExportController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			
			IdValidator validateForm = new IdValidator("taskId");
			int taskId = (Integer) validateForm.validate(request);
			
			ITaskDAO taskDAO = modelFactory.getTaskModel();
	    	Task task = taskDAO.getTaskById(taskId);
			
	    	
	    	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("task");
			doc.appendChild(rootElement);
	    	
			Element id = doc.createElement("id");
			id.appendChild(doc.createTextNode(Integer.toString(task.getId())));
			rootElement.appendChild(id);
			
			Element projectId = doc.createElement("projectId");
			projectId.appendChild(doc.createTextNode(Integer.toString(task.getProjectId())));
			rootElement.appendChild(projectId);
			
			Element description = doc.createElement("description");
			description.appendChild(doc.createTextNode(task.getDescription()));
			rootElement.appendChild(description);
			
			Element psd = doc.createElement("psd");
			psd.appendChild(doc.createTextNode(MyUtils.getDateString(task.getPsd())));
			rootElement.appendChild(psd);
			
			Element pdd = doc.createElement("pdd");
			pdd.appendChild(doc.createTextNode(MyUtils.getDateString(task.getPdd())));
			rootElement.appendChild(pdd);
			
			if (task.getAsd() != null){
				Element asd = doc.createElement("asd");
				asd.appendChild(doc.createTextNode(MyUtils.getDateString(task.getAsd())));
				rootElement.appendChild(asd);
			}
			
			if (task.getAdd() != null){
				Element add = doc.createElement("add");
				add.appendChild(doc.createTextNode(MyUtils.getDateString(task.getAdd())));
				rootElement.appendChild(add);
			}
			
			Element status = doc.createElement("status");
			status.appendChild(doc.createTextNode(task.getStatus().getName()));
			rootElement.appendChild(status);
			
			if (!task.getAttachments().isEmpty()){
				Element attachments = doc.createElement("attachments");
				rootElement.appendChild(attachments);
				
				for (Attachment attach: task.getAttachments()){
					Element attachment = doc.createElement("attachment");
					attachments.appendChild(attachment);
					
					Element name = doc.createElement("name");
					name.appendChild(doc.createTextNode(attach.getName()));
					attachment.appendChild(name);
					
					Element size = doc.createElement("size");
					size.appendChild(doc.createTextNode(Long.toString(attach.getSize())));
					attachment.appendChild(size);
					
					Element desctiption = doc.createElement("desctiption");
					desctiption.appendChild(doc.createTextNode(attach.getDescription()));
					attachment.appendChild(desctiption);
				}
			}
			
			response.setContentType("application/xml");

			response.setCharacterEncoding(Constants.ENCODING_UTF8);
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(response.getOutputStream());
	    	
			transformer.transform(source, result);
	    	
	    	
		} catch (ValidateException e){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
