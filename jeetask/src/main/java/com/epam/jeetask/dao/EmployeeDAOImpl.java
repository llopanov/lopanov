package com.epam.jeetask.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.epam.jeetask.beans.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

	//private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
	 
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
	
	public Employee getEmployeeById(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();     
		Employee p = (Employee) session.get(Employee.class, new Integer(id));
        //logger.info("Person loaded successfully, Person details="+p);
        return p;
	}

	public Employee getEmployeeByLoginPassword(String login, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Employee> listEmployees() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        List<Employee> employeesList = session.createQuery("from Employee").list();
//        for(Employee p : personsList){
//            logger.info("Person List::"+p);
//        }
        return employeesList;
		//return null;
	}

	public void addEmployee(Employee emp) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(emp);
        //logger.info("Person saved successfully, Person Details="+p);
	}

	public void updateEmployee(Employee emp) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        session.update(emp);
        //logger.info("Person loaded successfully, Person details="+p);
    }

	public void removeEmployee(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Employee emp = (Employee) session.load(Employee.class, new Integer(id));
        if(null != emp){
            session.delete(emp);
        }
        //logger.info("Person deleted successfully, person details="+p);
	}

}
