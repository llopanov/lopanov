package com.epam.jeetask.dao;

import java.util.List;

import com.epam.jeetask.beans.Project;

public interface ProjectDAO {
	public List<Project> listProjects();
}
