package com.epam.jeetask.dao;

import java.util.List;

import com.epam.jeetask.beans.Employee;

public interface EmployeeDAO {
	public Employee getEmployeeById(int id);
	public Employee getEmployeeByLoginPassword(String login, String password);
	public List<Employee> listEmployees();


	public void addEmployee(Employee emp);
    public void updateEmployee(Employee emp);
    public void removeEmployee(int id);
}

