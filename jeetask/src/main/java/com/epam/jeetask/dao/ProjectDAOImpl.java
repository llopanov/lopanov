package com.epam.jeetask.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.epam.jeetask.beans.Project;

@Repository
public class ProjectDAOImpl implements ProjectDAO {

private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
	
	public List<Project> listProjects() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
        List<Project> projectList = session.createQuery("from Project").list();
        return projectList;
	}

}
