package com.epam.jeetask.enums;

public enum Status {
 	OPEN,
	IN_PROGRESS,
	RESOLVE,
	CLOSED
}
