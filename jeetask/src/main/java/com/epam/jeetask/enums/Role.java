package com.epam.jeetask.enums;

public enum Role {
	DEVELOPER,
	KEY_DEVELOPER,
	TEAM_LEAD,
	PROJECT_MANAGER,
	QA
}
