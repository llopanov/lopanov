package com.epam.jeetask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.jeetask.beans.Employee;
import com.epam.jeetask.service.EmployeeService;
import com.epam.jeetask.service.ProjectService;

import org.springframework.ui.Model;

@Controller
public class AdminController {
	
	private EmployeeService employeeService;
	private ProjectService projectService;
	
	@Autowired(required=true)
    //@Qualifier(value="employeeService")
    public void setEmployeeService(EmployeeService es){
        this.employeeService = es;
    }
	
	@Autowired(required=true)
    //@Qualifier(value="employeeService")
    public void setProjectService(ProjectService ps){
        this.projectService = ps;
    }
	
	@RequestMapping({"/admin"})
	public String showIndexPage(Model model){
		
		//model.addAttribute("employee", this.employeeService.getEmployeeById(1));  //  new Employee()
		model.addAttribute("employee", new Employee());
		model.addAttribute("listEmployees", this.employeeService.listEmployees());
		model.addAttribute("listProjects", this.projectService.listProjects());
		return "admin";
	}
	
//	@RequestMapping(value= "/admin/employee/add", method = RequestMethod.GET)
//    public String addEmployeeShow(Model model){
//         System.out.println("heelo");
//		model.addAttribute("employee", new Employee());
//		return "admin/employee/add";        //return "admin/employee/add";
//         
//    }
	
//	@RequestMapping(value= "/admin/employee/add")
//	public String addEmployee(
//			@RequestParam(value = "id", required = false) String id,
//			@RequestParam(value = "login", required = false) String login,
//			@RequestParam(value = "password", required = false) String password,
//			@RequestParam(value = "firstName", required = false) String firstName,
//			@RequestParam(value = "lastName", required = false) String lastName,
//			@RequestParam(value = "positionId", required = false) String positionId
//			){
//		
//		System.out.println("id  = "+id+" "+login+" "+password+" "+firstName+" "+lastName+" "+positionId+" ");
//		
//		Employee emp = new Employee();
//		emp.setFirstName(firstName);
//		emp.setLastName(lastName);
//		emp.setPositionId(Integer.parseInt(positionId));
//		emp.setPassword(password);
//		emp.setLogin(login);
//		employeeService.addEmployee(emp);
//		

//         
//        return "redirect:/admin";
//         
//    }
	
	@RequestMapping(value= "/admin/employee/add")
    public String addEmployee(@ModelAttribute("employee") Employee emp){
        if(emp.getId() == 0){
            //new person, add it
            this.employeeService.addEmployee(emp);
        }else{
            //existing person, call update
            this.employeeService.updateEmployee(emp);
        }
         
        return "redirect:/admin";
         
    }
			

}
