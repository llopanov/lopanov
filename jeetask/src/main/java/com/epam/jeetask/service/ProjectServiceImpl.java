package com.epam.jeetask.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jeetask.beans.Project;
import com.epam.jeetask.dao.ProjectDAO;

@Service
public class ProjectServiceImpl implements ProjectService{

	private ProjectDAO projectDAO;
	
	public void setProjectDAO(ProjectDAO projectDAO) {
        this.projectDAO = projectDAO;
    }
	
	@Transactional
	public List<Project> listProjects() {
		// TODO Auto-generated method stub
		return projectDAO.listProjects();
	}

}
