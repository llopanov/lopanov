package com.epam.jeetask.service;

import java.util.List;

import com.epam.jeetask.beans.Project;

public interface ProjectService {
	public List<Project> listProjects() ;
}
