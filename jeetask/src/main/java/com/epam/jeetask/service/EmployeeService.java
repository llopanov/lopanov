package com.epam.jeetask.service;

import java.util.List;

import com.epam.jeetask.beans.Employee;

public interface EmployeeService {

	Employee getEmployeeById(int id);
	public List<Employee> listEmployees() ;
	
	public void addEmployee(Employee emp);
    public void updateEmployee(Employee emp);
    public void removeEmployee(int id);
}
