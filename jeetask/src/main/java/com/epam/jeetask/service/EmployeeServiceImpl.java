package com.epam.jeetask.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jeetask.beans.Employee;
import com.epam.jeetask.dao.EmployeeDAO;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeDAO employeeDAO;
	
	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }
	
	@Transactional
	public Employee getEmployeeById(int id) {
		// TODO Auto-generated method stub
		
		return this.employeeDAO.getEmployeeById(id);
	}

	@Transactional
	public List<Employee> listEmployees() {
        return this.employeeDAO.listEmployees();
    }

	@Transactional
	public void addEmployee(Employee emp) {
		// TODO Auto-generated method stub
		employeeDAO.addEmployee(emp);
	}

	public void updateEmployee(Employee emp) {
		// TODO Auto-generated method stub
		employeeDAO.updateEmployee(emp);
	}

	public void removeEmployee(int id) {
		// TODO Auto-generated method stub
		employeeDAO.removeEmployee(id);
	}
}
