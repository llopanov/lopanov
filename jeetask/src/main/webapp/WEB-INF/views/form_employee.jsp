

1123123 123 12312 312
<!-- 
<form action="<c:url value="/admin/employee/add"/>" method="post">
 <input type="hidden" name="id" value="0">
 <div>
    <label>firstName
        <input type="text" name="firstName">
    </label>
 </div>
  <div>
    <label>lastName
        <input type="text" name="lastName">
    </label>
 </div>
  <div>
    <label>login
        <input type="text" name="login">
    </label>
 </div>
  <div>
    <label>password
        <input type="text" name="password">
    </label>
 </div>
  <div>
    <label>positionId
        <select name="positionId">
            <option value="1">111</option>
            <option value="2">222</option>
        </select>
    </label>
 </div>
 <button>go</button>
 </form>
 
 -->


<form:form action="<c:url value="/admin/employee/add"/>" commandName="employee">


<table>
    <c:if test="${!empty employee.firstName}">
    <tr>
        <td>
            <form:label path="id">
                "ID
            </form:label>
        </td>
        <td>
            <form:input path="id" readonly="true" size="8"  disabled="true" />
            <form:hidden path="id" />
        </td>
    </tr>
    </c:if>
    <tr>
        <td>
            <form:label path="firstName">
                firstName
            </form:label>
        </td>
        <td>
            <form:input path="firstName" />
        </td>
    </tr>
    <tr>
        <td>
            <form:label path="lastName">
                lastName
            </form:label>
        </td>
        <td>
            <form:input path="lastName" />
        </td>
    </tr>
    
    <tr>
        <td>
            <form:label path="login">
                login
            </form:label>
        </td>
        <td>
            <form:input path="login" />
        </td>
    </tr>
        <tr>
        <td>
            <form:label path="password">
                password
            </form:label>
        </td>
        <td>
            <form:password path="password" />
        </td>
    </tr>
    
    </tr>
        <tr>
        <td>
            <form:label path="positionId">
                positionId
            </form:label>
        </td>
        <td>
            <form:input path="positionId" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2">
            <c:if test="${!empty employee.firstName}">
                <input type="submit" value="Edit Person"/> 
            </c:if>
            <c:if test="${empty employee.firstName}">
                <input type="submit" value="Add Person"/>
            </c:if>
        </td>
    </tr>
</table> 
</form:form>
    
    
    <c:url var="addAction" value="/person/add" ></c:url>
    ${addAction}
   
    
    <!-- 
    <form:form method="POST" commandName="employee" action="<c:url value="/admin/employee/add"/>">
        <table>
        <!-- 
        <c:if test="${!empty employee.firstName}">
        <tr>
            <td>
                <form:label path="id">
                    <spring:message text="ID"/>
                </form:label>
            </td>
            <td>
                <form:input path="id" readonly="true" size="8"  disabled="true" />
                <form:hidden path="id" />
            </td>
        </tr>
        </c:if>
        
        
        <form:hidden path="id"  />-->
        
        <!-- 
            <tr>
                <td>First Name:</td>
                <td><form:input path="name" /></td>
                <td><form:errors path="name" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><form:input path="email" /></td>
                <td><form:errors path="email" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Login:</td>
                <td><form:input path="age" /></td>
                <td><form:errors path="age" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><form:select path="gender">
                        <form:option value="" label="Select Gender" />
                        <form:option value="MALE" label="Male" />
                        <form:option value="FEMALE" label="Female" />
                    </form:select></td>
                <td><form:errors path="gender" cssClass="error" /></td>
            </tr>
            <tr>
                <td>Position:</td>
                <td><form:select path="gender">
                        <form:option value="" label="Select Gender" />
                        <form:option value="MALE" label="Male" />
                        <form:option value="FEMALE" label="Female" />
                    </form:select></td>
                <td><form:errors path="gender" cssClass="error" /></td>
    

            </tr>
            
            <tr>
                <td colspan="3"><input type="submit" value="Save Customer"></td>
            </tr>
        </table>
 
    </form:form>    
    
    -->