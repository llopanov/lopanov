
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div> ADMIN PAGE</div>
<div>Control panel for editing users and projects</div>
<div style="margin-bottom:30px">
http://www.journaldev.com/3531/spring-mvc-hibernate-mysql-integration-crud-example-tutorial <br/>
http://docs.jboss.org/hibernate/orm/4.2/devguide/en-US/html_single/<br/>
www.mkyong.com/spring/maven-spring-hibernate-mysql-example/<br/>
</div>

<div class="adminUsersList">
<div>
<a href="<c:url  value='/admin/employee/add'/>">add user</a>
</div>
<c:if test="${empty listEmployees}">
	NO USERS
</c:if>
<c:if test="${!empty listEmployees}">
    <table >
    <tr>
        <th width="80">Person ID</th>
        <th width="120">Person first Name</th>
        <th width="120">Person last name</th>
        <th width="120">login</th>
        
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>
    <c:forEach items="${listEmployees}" var="employee">
        <tr>
            <td>${employee.id}</td>
            <td>${employee.firstName}</td>
            <td>${employee.lastName}</td>
            <td>${employee.login}</td>
            <td><a href="<c:url value='/admin/employee/add/${employee.id}' />" >Edit</a></td>
            <td><a href="<c:url value='/admin/employee/remove/${employee.id}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>





 <div style="margin:30px 0;">
  <form action="<c:url value="/admin/employee/add"/>" method="post">
 <input type="hidden" name="id" value="0">
 <div>
    <label>firstName
        <input type="text" name="firstName">
    </label>
 </div>
  <div>
    <label>lastName
        <input type="text" name="lastName">
    </label>
 </div>
  <div>
    <label>login
        <input type="text" name="login">
    </label>
 </div>
  <div>
    <label>password
        <input type="text" name="password">
    </label>
 </div>
  <div>
    <label>positionId
        <select name="positionId">
            <option value="1">111</option>
            <option value="2">222</option>
        </select>
    </label>
 </div>
 <button>go</button>
 </form>
 </div>
 
 
 
 
</div>

<div class="adminProjectsList">
<div>
<a href="">add project</a>
</div>
<c:if test="${empty listProjects}">
	NO PROJECTS
</c:if>
<c:if test="${!empty listProjects}">
    <table class="tg">
    <tr>
        
        <th width="120">project Name</th>
        <th width="120">project Description</th>
        
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>
    <c:forEach items="${listProjects}" var="project">
        <tr>
            
            <td>${project.name}</td>
            <td>${project.description}</td>
            
            <td><a href="<c:url value='/edit/${project.id}' />" >Edit</a></td>
            <td><a href="<c:url value='/remove/${project.id}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
</c:if>
</div>

<div style="clear:both"></div>
<!-- 
${employee.id}
${employee.login}
${employee.password}
admin
 -->
